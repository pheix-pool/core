# Pheix CMS

## Overview

**Pheix CMS** is a MIT-licence compliant, Perl driven Content Management System. It is extremely lightweight, simple and customizable. 

## Important

[Official Pheix Site](https://pheix.org)

[Changelog](https://pheix.org/changelog.txt)

[MIT License](https://opensource.org/licenses/MIT)

## Contact

Please contact us via [feedback form](https://pheix.org/feedback.html) at pheix.org
