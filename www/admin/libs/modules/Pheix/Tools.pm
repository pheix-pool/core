package Pheix::Tools;

use strict;
use warnings;

##############################################################################
#
# File   :  Tools.pm
#
##############################################################################
#
# Главный пакет дополнительных инструментов в CMS Pheix
#
##############################################################################

our (@ISA, @EXPORT);
BEGIN {

    # getParamsArray    ->    get_params_array
    # fillCommonTags    ->    fillCommontags
    # showHomePage      ->    show_welcome_content
    # showLoginPage     ->    show_login_page
    # getLoginForm      ->    getLoginForm
    # getDateUpdate     ->    format_update
    # getDateYyMmDd     ->    format_year_month_day
    # getDateYear       ->    get_year
    # doFileUpload      ->    psupload

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      getPheixVersion
      getParamsArray
      fillCommonTags
      showHomePage
      showLoginPage
      getLoginForm
      getDateUpdate
      getDateYyMmDd
      getDateYear
      doFileUpload
    );
}

use Readonly;

Readonly my $PHEIX_VERSION => '0.8.120';

#------------------------------------------------------------------------------

my $ADMIN_SKIN_PATH     = 'admin/skins/classic_.txt';
my $FORM_SKIN_PATH      = 'admin/skins/classic_loginform_.txt';

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPheixVersion ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить текущую версию системы управления контентом Pheix.
# Функция возвращает текущую версию системы.
#
#------------------------------------------------------------------------------

sub getPheixVersion {
    return $PHEIX_VERSION;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParamsArray ( $env )
#------------------------------------------------------------------------------
#
# $env - переменная $ENV{QUERY_STRING}.
# Промежуточный функция парсинга $ENV{QUERY_STRING}.
# Функция возвращает массив с разобранными значениями.
#
#------------------------------------------------------------------------------

sub getParamsArray {
    my @params = ();
    if ( defined( $_[0] ) ) {
        my @query = split( /\&/, $_[0] );
        for ( my $i = 0 ; $i <= $#query ; $i++ ) {
            my @elems = split( /\=/, $query[$i] );
            push( @params, $elems[1] );
        }
    }
    return @params;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getQueryStr ( $sesid, @params )
#------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии.
# @params - список параметров.
# Получения значения Query String.
# Функция возвращает значение Query String.
#
#------------------------------------------------------------------------------

sub getQueryStr {
    my $script = $ENV{SCRIPT_NAME};
    my $sessid = $_[0];
    my $action = $_[1];
    my @params = @_[2..$#_];
    my $qurstr = qq~$script?sesid\=$sessid&amp;action\=$action~;
    for ( my $i = 0 ; $i <= $#params ; $i++ ) {
        $params[$i] =~ s/[\t\r\n]//ig;
        if ($params[$i] ne '' && $params[$i] !~ /^[\s]+$/) {
            $qurstr .= "&amp;param".($i+1)."=".$params[$i];
        }
    }
    return $qurstr;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# $rc = fillCommonTags ( $session_id, $input )
#------------------------------------------------------------------------------
#
# $session_id - идентификатор сессии доступа к административной части.
# $input - строка шаблона template.html
# Функция обработки общих cms-тегов шаблона.
# Функция возвращает строку $input, в которую вставлены общие cms-теги шаблона.
#
#------------------------------------------------------------------------------

sub fillCommonTags {
    my $session_id = $_[0];
    if ( !( defined($session_id) ) ) { $session_id = '0'; }

    my $input = $_[1];

    #my $pageaction = $_[2];
    my @access_modes = ( 3, 3, 3 );
    @access_modes = Secure::UserAccess::getAccessMode($session_id);
    my $system_modules_menu_link;
    my $active_modules_menu_link;
    my $system_modules_menu;
    my $active_modules_menu;
    my $cms_navig_block;
    $input =~ s/\%ses_id\%/$session_id/;
    $input =~ s/\%script_name\%/$ENV{SCRIPT_NAME}/;
    if ( $input =~ /\%last_year\%/ ) {
        my $date      = localtime;
        my $last_year = '';
        if ( $date =~ /^(.{1,})([\d]{4})$/ ) { $last_year = $2; }
        $input =~ s/\%last_year\%/$last_year/;
    }
    if ( $input =~ /\%timestamp\%/ ) {
        my $_ts = Pheix::Session::getHexStrFromString(time);
        $input =~ s/\%timestamp\%/$_ts/;
    }
    if ( $input =~ /\%version\%/ ) {
        my $ver = getPheixVersion();
        $input =~ s/\%version\%/$ver/;
    }
    if ( $input =~ /\%jgrowl_message\%/ ) {
        my $crypted_userid = Pheix::Session::getCryptLoginBySid($session_id);
        my $check_passw_hash =
          Pheix::Session::isDefaultPasswd( $crypted_userid, $session_id );
        my $security_warn_block = '';
        if ( $access_modes[1] != 3 && $check_passw_hash == 0) {
            $security_warn_block =
              Secure::UserAccess::isBruteForce( $session_id, $ENV{REQUEST_URI} );
        }
        $input =~ s/\%jgrowl_message\%/$security_warn_block/;
    }

    if ($input =~ /\%apopheoznews\%/) {
        my $newsblock = Config::ModuleMngr::fetchApopheozNews($session_id);
        $input =~ s/\%apopheoznews\%/$newsblock/;
    }

    if ( $input =~ /\%bigbro_code\%/ ) {
        ( my $page = $ENV{REQUEST_URI} ) =~ s/\&/%26/gi;;
        my $uid  = Pheix::Session::getDecryptUidBySesId($session_id) || 0;
        my $code = qq~<script type="text/javascript">doBigBroLook('$page', $uid);</script>~;
        my $show = Config::ModuleMngr::getSingleSetting('Stats', 0, "insertcodetoadmin") || 0;
        if (!$show) {
            $code = '';
        }
        $input =~ s/\%bigbro_code\%/$code/;
    }

    if ( $input =~ /\%cms_navig_block\%/ ) {
        my $crypted_userid = Pheix::Session::getCryptLoginBySid($session_id);
        my $check_passw_hash =
          Pheix::Session::isDefaultPasswd( $crypted_userid, $session_id );
        if ( $check_passw_hash == 0 ) {
            my @installed_modules = Config::ModuleMngr::getInstalledModsFromConf(0);

            for ( my $i = 0 ; $i <= $#installed_modules ; $i++ ) {
                my @parsed_path = Config::ModuleMngr::doConfigPathParse( $installed_modules[$i] );
                my $module_name = Config::ModuleMngr::getConfigValue( $parsed_path[0], $parsed_path[1], "name" );
                my $module_status = Config::ModuleMngr::getModPowerOn($module_name);
                my $module_human_name = Config::ModuleMngr::getConfigValue( $parsed_path[0], $parsed_path[1], "menuitem" );
                my $module_doorway_action = Config::ModuleMngr::getConfigValue( $parsed_path[0], $parsed_path[1], "startaction" );

                if ( $module_status == 0 )    # module is active
                {
                    my $module_system_type = Config::ModuleMngr::getConfigValue( $parsed_path[0], $parsed_path[1], "systemtype" );
                    if ( $module_system_type == 1 ) {
                        my $print_status = 0;

                        # check access mode for module
                        if ( $module_name eq 'Config' && $access_modes[0] != 3 ) {
                            $print_status = 1;
                        }
                        if ( ( $module_name eq 'Secure' || $module_name eq 'Stats') && $access_modes[1] != 3 ) {
                            $print_status = 1;
                        }

                        if ( $print_status > 0 ) {
                            if (!$system_modules_menu_link) {
                                if ($module_doorway_action ne '') {
                                    $system_modules_menu_link = "$ENV{SCRIPT_NAME}?id=$session_id&amp;action=$module_doorway_action";
                                }
                            }
                            $system_modules_menu .=
                            qq~<li class=dropdown-menu-submenu>
                            <a href="$ENV{SCRIPT_NAME}?id=$session_id&amp;action=$module_doorway_action">$module_human_name</a>
                            </li>~;
                        }
                    }
                    else {
                        if ( $access_modes[2] != 3 ) {
                            if (!$active_modules_menu_link) {
                                if ($module_doorway_action ne '') {
                                    $active_modules_menu_link = "$ENV{SCRIPT_NAME}?id=$session_id&amp;action=$module_doorway_action";
                                }
                            }
                            $active_modules_menu .=
                            qq~<li class=dropdown-menu-submenu>
                            <a href="$ENV{SCRIPT_NAME}?id=$session_id&amp;action=$module_doorway_action">$module_human_name</a>
                            </li>~;
                        }
                    }
                }
            }
            if ( $active_modules_menu ne '' ) {
                $active_modules_menu = qq~
                    <li class="dropdown-menu-li">
                    <a href="$active_modules_menu_link">
                    <span class="divmenucell dropdown-menu-width">
                    <span class="topline"></span><span class="item">Установленные модули</span></span></a>
                    <ul class="dropdown-menu-submenu">
                    $active_modules_menu</ul></li>
                ~;
            }
            if ( $system_modules_menu ne '' ) {
                $cms_navig_block .= qq~
                    <li class="dropdown-menu-li">
                    <a name="authmenu_core" href="$system_modules_menu_link">
                    <span class="divmenucell">
                    <span class="topline"></span><span class="item">Настройки системы</span>
                    </span></a><ul class="dropdown-menu-submenu">$system_modules_menu</ul></li>
                    ~;
            }
        }
        $cms_navig_block .=
        qq~
            $active_modules_menu
            <li class="dropdown-menu-li dropdown-menu-exit">
            <a name="authmenuitem_exit"
               href="javascript:doSesChckLoad('$session_id', '$ENV{SCRIPT_NAME}', '&amp;action=secureexitform')">
            <span class="divmenucell-exit">
            <span class="toplineexit"></span>
            <span class="itemexit">Выход</span>
            </span></a></li>~;

        $cms_navig_block = qq~<ul class="dropdown-menu">$cms_navig_block</ul>~;
        $input =~ s/\%cms_navig_block\%/$cms_navig_block/;
    }
    if ( $input =~ /\%uid\%/ ) {
        my $decryptuid = Pheix::Session::getDecryptUidBySesId($session_id) || 0;
        $input =~ s/\%uid\%/$decryptuid/;
    }
    if ( $input =~ /\%userlogin\%/ ) {
        my $decryptlgn = Pheix::Session::getDecryptLgnBySesId($session_id) || 0;
        $input =~ s/\%userlogin\%/$decryptlgn/;
    }
    if ( $input =~ /\%userfio\%/ ) {
        my $decryptfio = Pheix::Session::getDecryptFioBySesId($session_id) || 0;
        $input =~ s/\%userfio\%/$decryptfio/;
    }
    if (   $session_id eq ''
        || $session_id eq 'auth'
        || $session_id eq 'timeout' )
    {
        if ( $input =~ /authmenuitem/ ) { $input = "&nbsp;"; }
        $input =~ s/\%cms_news_block\%//;
    }
    else {
        ;
    }
    return $input;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showHomePage ( $session_id, $login_bysesid, 0  )
#------------------------------------------------------------------------------
#
# $session_id - идентификатор сессии доступа к административной части.
# $login_bysesid - зашифрованный логин, полученный по $login_bysesid из cur_login.tnk
# Вывод (печать) стартовой страницы административной части - "Конфигурация".
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showHomePage {
    open( my $inputFH, "<", $ADMIN_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt    = 0;
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] = &fillCommonTags( $_[0], $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $topics = Config::ModuleMngr::getMMHomePage(@_);
            $scheme[$cnt] =~ s/\%content\%/$topics/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showLoginPage ( $param_flag, $bruteforce )
#------------------------------------------------------------------------------
#
# $param_flag - тип формы
# $bruteforce - время блокировки доступа в секундах
# Вывод (печать) страницы с формой авторизации.
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showLoginPage {
    open( my $inputFH, "<", $FORM_SKIN_PATH );
    my @scheme = <$inputFH>;
    my $cnt    = 0;
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] = &fillCommonTags( $_[0], $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $login = &getLoginForm(@_);
            $scheme[$cnt] =~ s/\%content\%/$login/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($inputFH);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getLoginForm ( $param_flag, $bruteforce )
#------------------------------------------------------------------------------
#
# $param_flag - тип формы
# $bruteforce - время блокировки доступа в секундах
# Вывод формы авторизации при входе в систему.
# Функция возвращает HTML код формы авторизации.
#
#------------------------------------------------------------------------------

sub getLoginForm {
    my $param_flag = $_[0];
    my $bruteforce = $_[1];

    if ( !( defined($param_flag) ) ) { $param_flag = ''; }
    if ( !( defined($bruteforce) ) ) { $bruteforce = ''; }
    my $topic_form = '';

    if ( $param_flag eq '' && $bruteforce eq '' ) {
        $bruteforce =
          Pheix::Session::isBruteforce( $ENV{REMOTE_ADDR}, 'nologging' );
        if ( $bruteforce > 0 ) { $param_flag = 'timeout'; }
    }

    my $activate_login_sec = 600 - $bruteforce;

    if ( $param_flag eq 'timeout' ) {
        $topic_form = qq~
        <div class=errorloginform style="margin-top:40px; margin-bottom:40px;">
            Вы ввели неправильные учетные данные 5 раз подряд!<br>Вход заблокирован на 10 минут (осталось $activate_login_sec секунд).
        </div>
        ~;
    }
    else {
        my $message     = '';
        my $script_name = $ENV{SCRIPT_NAME};
        if ( !( defined( $ENV{SCRIPT_NAME} ) ) ) { $script_name = 'user.pl'; }
        if ($param_flag eq 'auth') {$message = qq~<div class=errorloginform style="margin-top:10px;">Неправильная пара логин-пароль!</div>~;}
        $topic_form = qq~
               $message
               <form id="form" name="form" method="post" action="$script_name?auth">
               <div id="block">
                      <label id="user" for="name"><img src="images/admin/cms/loginform/login.png" alt="" width="16" height="16" /></label>
                      <input type="text" name="login" id="name" placeholder="Логин" required />
                      <label id="pass" for="password"><img src="images/admin/cms/loginform/passw.png" alt="" width="16" height="16" /></label>
                      <input type="password" name="password" id="password" placeholder="Пароль" required />
                      <input type="submit" id="submit" name="submit" value=""/>
               </div>
               </form>
               <div id="option">
                      <p><a href="https://twitter.com/ApopheozRu" target="_blank" class="followlink">follow pheix</a></p>
               </div>
              ~;
    }
    return $topic_form;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDateUpdate ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод даты обновления страницы.
# Функция возвращает строку типа "23, Январь 2015".
#
#------------------------------------------------------------------------------

sub getDateUpdate {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split (/ /,$date);
    @date_array = ($date_array[2],$date_array[1],$date_array[4]);
    if ($date_array[1] eq 'Jan') {$date_array[1] = 'Январь'}
    if ($date_array[1] eq 'Feb') {$date_array[1] = 'Февраль'}
    if ($date_array[1] eq 'Mar') {$date_array[1] = 'Март'}
    if ($date_array[1] eq 'Apr') {$date_array[1] = 'Апрель'}
    if ($date_array[1] eq 'May') {$date_array[1] = 'Май'}
    if ($date_array[1] eq 'Jun') {$date_array[1] = 'Июнь'}
    if ($date_array[1] eq 'Jul') {$date_array[1] = 'Июль'}
    if ($date_array[1] eq 'Aug') {$date_array[1] = 'Август'}
    if ($date_array[1] eq 'Sep') {$date_array[1] = 'Сентябрь'}
    if ($date_array[1] eq 'Oct') {$date_array[1] = 'Октябрь'}
    if ($date_array[1] eq 'Nov') {$date_array[1] = 'Ноябрь'}
    if ($date_array[1] eq 'Dec') {$date_array[1] = 'Декабрь'}
    return "$date_array[0], $date_array[1] $date_array[2]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDateYyMmDd ( $timestamp  )
#------------------------------------------------------------------------------
#
# $timestamp - дата в формате unixtime.
# Вывод даты в формате DD-MM-YYYY.
# Функция возвращает строку типа "30-01-2015".
#
#------------------------------------------------------------------------------

sub getDateYyMmDd {
    my $date = localtime($_[0]);
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split (/ /,$date);
    @date_array = ($date_array[2],$date_array[1],$date_array[4]);
    if ($date_array[1] eq 'Jan') {$date_array[1] = '01'}
    if ($date_array[1] eq 'Feb') {$date_array[1] = '02'}
    if ($date_array[1] eq 'Mar') {$date_array[1] = '03'}
    if ($date_array[1] eq 'Apr') {$date_array[1] = '04'}
    if ($date_array[1] eq 'May') {$date_array[1] = '05'}
    if ($date_array[1] eq 'Jun') {$date_array[1] = '06'}
    if ($date_array[1] eq 'Jul') {$date_array[1] = '07'}
    if ($date_array[1] eq 'Aug') {$date_array[1] = '08'}
    if ($date_array[1] eq 'Sep') {$date_array[1] = '09'}
    if ($date_array[1] eq 'Oct') {$date_array[1] = '10'}
    if ($date_array[1] eq 'Nov') {$date_array[1] = '11'}
    if ($date_array[1] eq 'Dec') {$date_array[1] = '12'}
    return "$date_array[2]-$date_array[1]-$date_array[0]";
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDateYear ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод года в формате YYYY.
# Функция возвращает строку типа "2015".
#
#------------------------------------------------------------------------------

sub getDateYear {
    my $date = localtime;
    $date =~ s/[\.\:\,]//g;
    $date =~ s/[\s]+/ /g;
    my @date_array = split (/ /,$date);
    return $date_array[4];
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doFileUpload ($path, $inputfieldname, $newname, $maxsize, $auto_rename)
#------------------------------------------------------------------------------
#
# $path - путь к каталогу загрузки файлов на сервере
# $inputfieldname - имя поля формы
# $newname - новое имя файла
# $maxsize - максимально допустимый размер файла
# $auto_rename - автоматическое переименование файла
# Универсальная функция загрузки файла на сервер.
# Функция статус загрузки файла на сервер в HTML формате.
#
#------------------------------------------------------------------------------

sub doFileUpload {
    my $path           = shift;
    my $inputfieldname = shift;
    my $newname        = shift;
    my $maxsize        = shift;
    my $auto_rename    = shift;
    if ($path) {
        if ($inputfieldname) {
            my $req = new CGI;
            if ( $req->param($inputfieldname) ) {
                my $file     = $req->param($inputfieldname);
                my $filename = $file;
                $filename =~ s/^.*(\\|\/)//;

                #For IE
                $filename =~ s/ +/\_/g;

                #For Opera
                $filename =~ s/\"//g;
                $filename = $newname if $newname;
                if ( ( -e "$path/$filename" ) && ( $auto_rename == 1 ) ) {
                    my $pick_new_name = 1;
                    my $fore_num      = 1;
                    $filename =~ /^(.+)\.([^\.]+)$/;
                    my $front = $1;
                    my $ext   = $2;
                    while ($pick_new_name) {
                        my $test_name = $front . $fore_num . '.' . $ext;
                        unless ( -e "$path/$test_name" ) {
                            $pick_new_name = 0;
                            $filename      = $test_name;
                        }
                        $fore_num++;
                    }
                }
                if ( open( my $outpuFH, ">", "$path/$filename" ) ) {
                    binmode($outpuFH);
                    while ( my $bytesread = read( $file, my $buffer, 1024 ) ) {
                        print $outpuFH $buffer;
                    }
                    close($outpuFH);
                    if ( $maxsize > 0 ) {
                        if ( ( -s "$path/$filename" ) > ( $maxsize * 1024 ) ) {
                            unlink("$path/$filename");
                            return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="error">file too big! Maxsize=$maxsize</span></div>~;
                        }
                        else {
                            my $filesize = ( -s "$path/$filename" );
                            return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="status">загружен ($filesize)!</span></div>~;
                        }
                    }
                    else {
                        return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="status">загружен!</span></div>~;

                    }
                }
                else {
                    return qq~<div class="standart_text">$path/$filename...&nbsp;<span class="error">file opening is failed!</span></div>~;
                }
            }
            else {
                return qq~<div class="standart_text"><span class="error">upload failed - invalid/unknown fieldname (\$inputfieldname=$inputfieldname)!</span></div>~;
            }
        }
        else {
            return qq~<div class="standart_text"><span class="error">browser error - form sending is failed (invalid \$inputfieldname=$inputfieldname)!</span></div>~;
        }
    }
    else {
        return qq~<div class="standart_text"><span class="error">invalid path - $path</span></div>~;
    }
}

#------------------------------------------------------------------------------

END { }

1;
