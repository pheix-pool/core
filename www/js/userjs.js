var Spin = '<div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>';

function doDataLoad(pagenum, shopid, catid) {
	/*if ($("#loadvariants").contents().length == 0) {*/
		jQuery("#loadvariants")
				.load(
						"/user.pl?action=shopcatloaditems&amp;shopid="
								+ shopid
								+ "&amp;catid="
								+ catid
								+ "&amp;pagenum="
								+ pagenum
								+ "&amp;rand="
								+ Math.random(),
						function(responseTxt, statusTxt, xhr) {
							/*if(statusTxt == "success") {
						    	top.location.href="#variants-anchor";
							}*/
							if (statusTxt == "error")
								alert("Error: " + xhr.status + ": "
										+ xhr.statusText);
						});
	/*} else {
		$("#loadvariants").empty();
	}*/
    top.location.href="#variants-anchor";
}

function doAjaxSearch() {
	var msg   = $('#search-catalog').serialize();
	if (typeof msg === 'undefined' || msg === null) {
		;
	} else {
		jQuery( "#search-container" ).html( Spin );
		var query = $('#search-catalog-query').val();
		console.log( 'doAjaxSearch(): query='+query );
		jQuery.post( 'shopcatsearch.html?'+Math.random(), msg)
			.fail(function( data ) {
				console.log( 'doAjaxSearch(): post failed!' + data  );
			})
			.done(function( data ) {
				console.log( 'doAjaxSearch(): post done!' );
				jQuery( "#search-container" ).html( data );
			});
	}
}

function doAjaxV3SliderLoad( galid ) {
	jQuery( "#v3-slider-load" ).html( Spin );
	jQuery("#v3-slider-load")
	.load(
			"/user.pl?action=sysgalslider&amp;galid="
					+ galid
					+ "&amp;rand="
					+ Math.random(),
			function(responseTxt, statusTxt, xhr) {
				if (statusTxt == "error")
					alert("Error: " + xhr.status + ": "
							+ xhr.statusText);
	});
}


function Currdate(formid, elemnum) {
	all = new Date();
	today = all.getDate();
	month = all.getMonth();
	if (navigator.appName == 'Microsoft Internet Explorer') {
		year = all.getYear()
	}
	;
	if (navigator.appName != 'Microsoft Internet Explorer') {
		year = all.getYear() + 1900
	}
	;
	month = month + 1;
	if (navigator.appName != 'Microsoft Internet Explorer') {
		s = '';
		st = "";
		Str = new String;
		Str = navigator.appVersion;
		s = Str.charAt(0);
		if (s == '4')
			monthname = (month == 1) ? "January"
				: (month == 2) ? "February"
				: (month == 3) ? "March"
				: (month == 4) ? "April"
				: (month == 5) ? "May"
				: (month == 6) ? "June"
				: (month == 7) ? "July"
				: (month == 8) ? "Augest"
				: (month == 9) ? "September"
				: (month == 10) ? "October"
				: (month == 11) ? "November"
				: "December";
		if (s != '4')
			monthname = (month == 1) ? "Январь"
				: (month == 2) ? "Февраль"
				: (month == 3) ? "Март"
				: (month == 4) ? "Апрель"
				: (month == 5) ? "Май"
				: (month == 6) ? "Июнь"
				: (month == 7) ? "Июль"
				: (month == 8) ? "Август"
				: (month == 9) ? "Сентябрь"
				: (month == 10) ? "Октябрь"
				: (month == 11) ? "Ноябрь"
				: "Декабрь";
	}
	if (navigator.appName == 'Microsoft Internet Explorer') {
		monthname = (month == 1) ? "Январь"
				: (month == 2) ? "Февраль"
				: (month == 3) ? "Март"
				: (month == 4) ? "Апрель"
				: (month == 5) ? "Май"
				: (month == 6) ? "Июнь"
				: (month == 7) ? "Июль"
				: (month == 8) ? "Август"
				: (month == 9) ? "Сентябрь"
				: (month == 10) ? "Октябрь"
				: (month == 11) ? "Ноябрь"
				: "Декабрь";
	}
	hours = all.getHours();
	minutes = all.getMinutes();
	seconds = all.getSeconds();
	timevalue = " " + (hours);
	timevalue += ((minutes < 10) ? ":0" : ":") + minutes;
	timevalue += ((seconds < 10) ? ":0" : ":") + seconds;
	time = today + " " + monthname + ", " + year;

	if (!elemnum) {
		elemnum = 1;
	}

	if (document.forms[formid].name && formid != "") {
		document.forms[formid].elements[elemnum].value = time;
	} else {
		document.forms[0].elements[elemnum].value = time;
	}
	// alert(document.forms[0].elements[elemnum].name);
}

function image_dimensions(imagehandler, thinkparam) {
	var i = new Image();
	i.onload = function() {

		var think_width = 380;
		var scale_koeff = 1;
		var scaleheight = 0;
		var scalewidth = 0;
		var globe_x = i.width;
		var globe_y = i.height;

		if (globe_x >= globe_y) {
			if (thinkparam == 1) {
				think_width = 300;
			}
			scale_koeff = (globe_x / think_width);
			if ((scale_koeff < 1)) {
				scale_koeff = 1;
			}
			scaleheight = Math.round((globe_y / scale_koeff));
			scalewidth = Math.round((globe_x / scale_koeff));
			imagehandler.width = scalewidth;
			imagehandler.height = scaleheight;
		}
		if (globe_x < globe_y) {
			think_width = 215;
			if (thinkparam == 1) {
				think_width = 215;
			}
			scale_koeff = (globe_x / think_width);
			if ((scale_koeff < 1)) {
				scale_koeff = 1;
			}
			scaleheight = Math.round((globe_y / scale_koeff));
			scalewidth = Math.round((globe_x / scale_koeff));
			imagehandler.width = scalewidth;
			imagehandler.height = scaleheight;
		}
		// imagehandler = i;
		// alert(imagehandler.width+'\n'+imagehandler.height);
	};
	i.src = imagehandler.src;
	return;
}
