package Pheix::Tools;

use strict;
use warnings;

###############################################################################
#
# File   :  Tools.pm
#
###############################################################################
#
# Главный пакет дополнительных инструментов в CMS Pheix
#
###############################################################################

our ( @ISA, @EXPORT );

BEGIN {
    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      getFilesArray
      getPheixVersion
      getDateUpdate
      getSingleSetting
      getSettingFromGroupByAttr
      doBlowfishDecrypt
      doBlowfishEncrypt
      getHexStrFromString
      getStringFromHexStr
      generateKeyTag
      getDateYear
      getProtoSrvName
      getConfigValue
      getInstAppMods
      getModXml
      getSettingChildNum
    );
}

#------------------------------------------------------------------------------

use utf8;
use XML::LibXML;
use Readonly;
use Digest::MD5 qw(md5_hex);
use Crypt::CBC;
use File::Basename;

Readonly my $PHEIX_VERSION => '0.8.120';

#------------------------------------------------------------------------------

my $MODULES_USRPATH = 'libs/modules';
my $MODULES_PATH    = 'admin/libs/modules';
my $INSTALL_DB_PATH = 'conf/system/install.tnk';

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getPheixVersion ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить текущую версию системы управления контентом Pheix.
# Функция возвращает текущую версию системы.
#
#------------------------------------------------------------------------------

sub getPheixVersion {
    return $PHEIX_VERSION;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getParamsArray ( $env )
#------------------------------------------------------------------------------
#
# $env - переменная $ENV{QUERY_STRING}.
# Промежуточный функция парсинга $ENV{QUERY_STRING}.
# Функция возвращает массив с разобранными значениями.
#
#------------------------------------------------------------------------------

sub getParamsArray {
    my ($query) = @_;
    my @params;
    if ( defined $query ) {
        my @query = split /\&/sx, $query;
        for my $i ( 0 .. $#query ) {
            my @elems = split /\=/sx, $query[$i];
            push @params, $elems[1];
        }
    }
    return @params;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDateUpdate ($unixtime)
#------------------------------------------------------------------------------
#
# $unixtime - значение unix timestamp.
# Вывод даты обновления страницы.
# Функция возвращает строку типа "23, Январь 2015".
#
#------------------------------------------------------------------------------

sub getDateUpdate {
    my ( $timestamp ) = @_;
    my $date;
    my %m = (
        Jan => 'Январь',  Feb => 'Февраль', Mar => 'Март',
        Apr => 'Апрель',  May => 'Май',     Jun => 'Июнь',
        Jul => 'Июль',    Aug => 'Август',  Sep => 'Сентябрь',
        Oct => 'Октябрь', Nov => 'Ноябрь',  Dec => 'Декабрь'
    );
    if ( defined($timestamp) && $timestamp =~ /^\d+$/ ) {
        $date = localtime($timestamp);
    }
    else {
        $date = localtime;
    }
    $date =~ s/[\.\:\,]//sxg;
    $date =~ s/[\s]+/ /sxg;
    my @datarr = split /\s/sx, $date;
    my ( $day, $month, $year ) = ( $datarr[2], $m{$datarr[1]}, $datarr[-1] );
    return $day . ', ' . $month . ' ' . $year;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDateYear ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод года в формате YYYY.
# Функция возвращает строку типа "2015".
#
#------------------------------------------------------------------------------

sub getDateYear {
    my $date = localtime;
    $date =~ s/[\.\:\,]//sxg;
    $date =~ s/[\s]+/ /sxg;
    my @date_array = split /\s/sx, $date;
    return $date_array[-1];
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getFilesArray ( $dir )
#------------------------------------------------------------------------------
#
# $dir - каталог для поиска.
# Найти файлы в каталоге.
# Функция возвращает массив c найденными файлами.
#
#------------------------------------------------------------------------------

sub getFilesArray {
    my ($dir) = @_;
    my $dhndl;
    my @files;
    $dir =~ s/\\/\//g;
    if (-e $dir) {
        opendir $dhndl, $dir or die 'Unable to open directory ' . $dir . ': ' . $!;
        @files = grep { ( !/^\.+$/ ) and !( -d "$dir/$_" ) } readdir $dhndl;
        closedir $dhndl;
    }
    return @files;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModXml ( $tmp_dir )
#------------------------------------------------------------------------------
#
# $tmp_dir - каталог для поиска.
# Поиск файла описателя модуля в формате XML.
# Функция возвращает найденный файл описателя модуля или пустую строку,
# если файл описателя не найден.
#
#------------------------------------------------------------------------------

sub getModXml {
    my ($tmp_dir) = @_;
    my @files_ = getFilesArray($tmp_dir);
    for my $fileinx ( 0 .. $#files_ ) {
        if ( $files_[$fileinx] =~ /\.xml$/sx ) { return $files_[$fileinx]; }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSingleSetting ()
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# Получить настройку из хранилища (одиночная настройка, не входящая в группу).
# Функция возвращает настройку из хранилища.
#
#------------------------------------------------------------------------------

sub getSingleSetting {
    my ( $modulename, $area, $setting_name ) = @_;
    my $modulepath;
    my $setting;

    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }

    my $xmlfile    = getModXml("$modulepath/$modulename/");
    if ( defined $xmlfile ) {
        my $xmlpath    = "$modulepath/$modulename/$xmlfile";
        if ( -e $xmlpath && defined $xmlfile ) {
            my $doc = XML::LibXML->load_xml( location => $xmlpath );
            my @nodes =
                $doc->findnodes("/module/configuration/settings/$setting_name");
            if ( $nodes[0] ) {
                if ( defined $nodes[0]->textContent()
                    && $nodes[0]->textContent() !~ /^[\r\n\s]+$/sx )
                {
                    $setting = $nodes[0]->textContent();
                }
            }
        }
    }
    return $setting;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSettingFromGroupByAttr ( $modulename, $area, $setting_group_name,
#                            $setting_group_attr_n, $setting_group_attr_v,
#                            $setting_name )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (1 - user, 0 - admin).
# $setting_group_name - название группы настроек.
# $setting_group_attr_n - название ключевого атрибута группы.
# $setting_group_attr_v - значение атрибута $setting_group_attr_n.
# $setting_name - название настройки.
# Получить настройку из хранилища (настройка из заданной группы).
# Функция возвращает настройку из хранилища.
#
#------------------------------------------------------------------------------

sub getSettingFromGroupByAttr {
    my ( $modulename, $area, $setting_group_name, $setting_group_attr_n,
        $setting_group_attr_v, $setting_name )
      = @_;
    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    my $setting;
    my $modulepath;
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }
    if ($modulepath) {
        my $xmlfile    = getModXml("$modulepath/$modulename/");
        if ( defined $xmlfile ) {
            my $xmlpath    = "$modulepath/$modulename/$xmlfile";
            if ( -e $xmlpath && defined $xmlfile ) {
                my $doc = XML::LibXML->load_xml( location => $xmlpath );
                foreach my $settings (
                    $doc->findnodes('/module/configuration/settings/*') )
                {
                    if ( $settings->textContent() !~ /^[\r\n\s]+$/sx ) {
                        my $_group = $settings->getAttribute('group') || 'false';
                        if ( $_group eq 'true'
                            && $settings->nodeName() eq $setting_group_name )
                        {
                            my $grouplabl = Encode::encode( "utf8",
                                $settings->getAttribute('label') );
                            my $groupattr = Encode::encode( "utf8",
                                $settings->getAttribute($setting_group_attr_n) );
                            if ( $groupattr eq $setting_group_attr_v ) {
                                foreach my $group_settings (
                                    $settings->nonBlankChildNodes() )
                                {
                                    if (
                                        $group_settings->nodeName() eq $setting_name
                                        && defined $group_settings->textContent()
                                        && $group_settings->textContent() !~
                                        /^[\r\n\s]+$/sx )
                                    {
                                        $setting = $group_settings->textContent();
                                        $setting =~ s/[\r\n]+//sxgi;
                                        $setting =~ s/[\s\t]+/ /sxg;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $setting;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doBlowfishEncrypt ( $strtoencrypt )
#------------------------------------------------------------------------------
#
# $strtoencrypt - строка.
# Функция возвращает зашифрованную строку
#
#------------------------------------------------------------------------------

sub doBlowfishEncrypt {
    my ($strtoencrypt) = @_;
    my $keytag         = generateKeyTag();
    my $ciphertext;
    if ( $strtoencrypt ne '' && $keytag ne '' ) {
        my $cipher = Crypt::CBC->new( -key => $keytag, -cipher => 'Blowfish' );
        $ciphertext = $cipher->encrypt($strtoencrypt);
    }
    return $ciphertext;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doBlowfishDecrypt ( $strtodecrypt )
#------------------------------------------------------------------------------
#
# $strtodecrypt - строка.
# Функция возвращает дешифрованную строку
#
#------------------------------------------------------------------------------

sub doBlowfishDecrypt {
    my ($strtodecrypt) = @_;
    my $keytag         = generateKeyTag();
    my $plaintext;
    if ( $strtodecrypt ne '' && $keytag ne '' ) {
        my $cipher = Crypt::CBC->new( -key => $keytag, -cipher => 'Blowfish' );
        $plaintext = $cipher->decrypt($strtodecrypt);
    }
    return $plaintext;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getHexStrFromString ( $str )
#------------------------------------------------------------------------------
#
# $str - строка.
# Функция возвращает строку в виде hex-кодов символов
#
#------------------------------------------------------------------------------

sub getHexStrFromString {
    my ($str)     = @_;
    my $outstr;
    my @chars_arr = split( //, $str );
    for my $charInx (0..$#chars_arr) {
        if ( $chars_arr[$charInx] ne '' ) {
            my $hex_value = "%" . sprintf( "%x", ord( $chars_arr[$charInx] ) );
            $outstr .= $hex_value;
        }
    }
    return $outstr;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getStringFromHexStr ( $str )
#------------------------------------------------------------------------------
#
# $str - строка в виде hex-кодов символов.
# Представление строки hex-кодов символов в виде обычной строки
#
#------------------------------------------------------------------------------

sub getStringFromHexStr {
    my ($str)     = @_;
    my $outstr    = '';
    my @chars_arr = split /%/, $str;
    for my $charInx (0..$#chars_arr) {
        if ( $chars_arr[$charInx] ne '' && $chars_arr[$charInx] =~ /^[a-zA-Z0-9]+$/ ) {
            $chars_arr[$charInx] = hex( $chars_arr[$charInx] );
            my $char = chr( $chars_arr[$charInx] );
            $outstr .= $char;
        }
    }
    return $outstr;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# generateKeyTag ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Функция возвращает keytag в виде текстовой строки.
#
#------------------------------------------------------------------------------

sub generateKeyTag {
    my $feedin    = '72<84d5e~98#&a25^!(6014f1#0a1!@7695$)612[F)-E]be1a';
    my $feedindex = $feedin;
    $feedindex =~ s/[\D]+//ig;
    my @indexes = split //, $feedindex;
    my @feeinar = split //, $feedin;
    my @feedout = ();

    for my $feedInx (0..$#indexes) {
        push( @feedout, $feeinar[ $indexes[$feedInx] ] );
    }
    return join "", @feedout;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSettingChildNum ( $modulename, $area, $setting_name )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# $setting_name - название настройки.
# Подсчитать количество поднастроек (дочерних элементов)  для настройки.
#
#------------------------------------------------------------------------------

sub getSettingChildNum {
    my ( $modulename, $area, $setting_name ) = @_;
    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    my $setting;
    my $modulepath;
    my $rc = -1;
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }
    my $xmlfile    = getModXml("$modulepath/$modulename/");
    my $xmlpath    = "$modulepath/$modulename/$xmlfile";
    if ( -e $xmlpath && defined $xmlfile ) {
        my $doc = XML::LibXML->load_xml( location => $xmlpath );
        my @nodes =
            $doc->findnodes("/module/configuration/settings/$setting_name/*");
        if ( @nodes ) {
            $rc = $#nodes;
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getProtoSrvName ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить протокол + имя сервера из настроек.
#
#------------------------------------------------------------------------------

sub getProtoSrvName {
    my $_prtcl;
    my $_proto = getSingleSetting('Config', 0, 'workviaproto');
    my $_sname = getSingleSetting('Config', 0, 'servername');
    if (!$_sname || $_sname eq '' || !defined $_sname) {
        $_sname = $ENV{SERVER_NAME};
    }
    if ($_proto == 1) {
        $_prtcl = 'https://';
    } else {
        $_prtcl = 'http://';
    }
    $_sname =~ s/^https?\:\/\///gi;
    $_sname =~ s/\/{1,}$//gi;
    return $_prtcl.$_sname;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getConfigValue ( $module_path, $module_name, $value )
#------------------------------------------------------------------------------
#
# $module_path - путь до каталога модуля.
# $module_xml - xml-описатель файла модуля.
# $value - название значения, которое нужно получить.
# Функции разбора конфигурационного файла модуля в формате XML.
#
#------------------------------------------------------------------------------

sub getConfigValue {
    my ($module_path, $module_name, $val ) = @_;
    my $file_to_parse = $module_path . '/' . $module_name;
    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        for ( my $cfginx = 0 ; $cfginx <= $#filecontent ; $cfginx++ ) {
            if ( $filecontent[$cfginx] =~ /<([a-z\_]+){0,}>/ ) {
                my $tmp_value = $1;
                if (   ( $tmp_value eq $val )
                    && ( $filecontent[$cfginx] =~ />(.{0,})</ ) )
                {
                    return $1;
                }
            }
        }
        return qq~Элемент $val не найден~;
    } else {
        return qq~<div class="error">$file_to_parse не найден!</div>~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getInstAppMods ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Поиск установленных модулей в install.tnk.
# Функция возвращает массив найденных модулей или пустое значение, если
# модули на найдены.
#
#------------------------------------------------------------------------------

sub getInstAppMods {
    my @_appmods;
    if ( -e $INSTALL_DB_PATH ) {
        open( my $fh, "<", $INSTALL_DB_PATH );
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @_cols = split /\|/, $_rows[$i];
            my $_type = getConfigValue(
                dirname($_cols[3]), $_cols[2], 'systemtype'
            );
            if ($_type != 1) {
                push(
                    @_appmods,
                    $MODULES_USRPATH.'/'.$_cols[2].'/'.basename($_cols[3])
                );
            }
        }
        return @_appmods;
    } else {
        return;
    }
}

#------------------------------------------------------------------------------

sub getStaticSiteMapFname {
    my $_mfldr = $MODULES_PATH.'/Config';
    return getConfigValue( $_mfldr, getModXml($_mfldr), 'staticsitemap' );
}

#------------------------------------------------------------------------------

END { }

1;
