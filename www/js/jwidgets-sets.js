   var settings = {
        title: " ",
        description: " ",
        padding: { left: 22, top: 25, right: 50, bottom: 75 },
        legendLayout : { left: 20, top: 355, width: 300, height: 50, flow: 'horizontal' }, 
        showLegend: true,
        source: sampleData,
    	categoryAxis:
			{
                      dataField: 'Day',
                      axisSize: 'auto',
                      textOffset: {x: 0, y: 15}
			},
        colorScheme: 'scheme04',
        seriesGroups:[{
            type: 'line',
            valueAxis:{
				displayValueAxis: true,
                axisSize: 'auto',
                tickMarksColor: '#888888'
            },
            series: [
            	{ dataField: 'host', displayText: 'Хосты', lineColor: '#D2146E', fillColor: '#D2146E'},
            	{ dataField: 'visit', displayText: 'Визиты', lineColor: '#72C32F', fillColor: '#72C32F'},
            	{ dataField: 'hit', displayText: 'Хиты', lineColor: '#EBA902', fillColor: '#EBA902'},
            ]
        }]
    };
