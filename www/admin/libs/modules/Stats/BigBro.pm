package Stats::BigBro;

use strict;
use warnings;

##############################################################################
#
# File   :  BigBro.pm
#
##############################################################################
#
# Главный пакет административной части модуля сбора статистики в CMS Pheix
#
##############################################################################

BEGIN {
    ;
}

use Pheix::Session qw(
    doBlowfishDecrypt
    getStringFromHexStr
    getKeytagByDecryptUid
    getCryptLoginByDecryptUid
);
use Fcntl qw(:DEFAULT :flock :seek);
use Encode qw(encode);

#------------------------------------------------------------------------------
# new ()
#------------------------------------------------------------------------------
#
# Конструктор класса Stats::BigBro.
#
#------------------------------------------------------------------------------

sub new {
    my $MODULE_NAME = "Stats";
    my ($class)     = @_;
    my $self        = {
        name     => 'Stats::BigBro',
        version  => '2.2',
        mod_name => $MODULE_NAME,
        mod_dscr => 'Система сбора статистики Pheix',
        mod_fldr => 'admin/libs/modules/' . $MODULE_NAME,
        mod_tmpl => 'admin/skins/classic_.txt',
        mod_logf => 'conf/system/bigbro.tnk',
        mod_scrt => getStatsJsCss(),
    };
    bless $self, $class;
    return $self;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getStatsJsCss ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Вывод CSS/JS кода модуля.
#
#------------------------------------------------------------------------------

sub getStatsJsCss {
    my $cntnt_ = qq~
    <!-- TABS -->
    <link   type="text/css" rel="stylesheet"
            href="css/easytabs/easy-responsive-tabs.css">
    <script src="js/easytabs/easyResponsiveTabs.js"
            type="text/javascript"></script>
    <!-- JWIDGET -->
    <link rel="stylesheet" href="css/jqwidgets/jqx.base.css" type="text/css">
    <script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript"
            src="js/jqwidgets/jqxchart.core.js"></script>
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showBBHomePage ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# Вывод содержимого стартовой страницы.
#
#------------------------------------------------------------------------------

sub showBBHomePage {
    my ($self, $sid) = @_;
    if (-e $self->{mod_tmpl}) {
        open my $fh, "<", $self->{mod_tmpl}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @tmpl    = <$fh>;
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $cnt (0..$#tmpl) {
            $tmpl[$cnt] =
                Pheix::Tools::fillCommonTags( $sid, $tmpl[$cnt] );
            if ( $tmpl[$cnt] =~ /\%scripts_for_this_page\%/ ) {
                $tmpl[$cnt] =~ s/(\<\!\-\-)|(\-\-\>)//gi;
                my $scrt = $self->{mod_scrt};
                $tmpl[$cnt] =~ s/\%scripts_for_this_page\%/$scrt/;
            }
            if ( $tmpl[$cnt] =~ /\%content\%/ ) {
                my $content = $self->getHomePage($sid);
                $tmpl[$cnt] =~ s/\%content\%/$content/;
            }
            print $tmpl[$cnt];
        }
    } else {
        print '<h4>Pheix: unable to open file '.$self->{mod_tmpl}.'</h4>'
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getHomePage ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# Получение содержимого стартовой страницы.
#
#------------------------------------------------------------------------------

sub getHomePage {
    my ($self, $sid) = @_;
    my $xml_file = Config::ModuleMngr::getModXml($self->{mod_fldr});
    my $mod_descr = Config::ModuleMngr::getConfigValue( $self->{mod_fldr},
        $xml_file, "description" );
    my $cntnt_ = qq~<div class=a_hdr>$mod_descr</div>~;
    $cntnt_ .= $self->getHomePageTmpl($sid);
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getHomePageTmpl ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# Получение шаблона стартовой страницы.
#
#------------------------------------------------------------------------------

sub getHomePageTmpl {
    my ($self, $sid) = @_;
    my $rawlog = $self->getRawLogsTable($sid);
    my $stats  = $self->getWeekTable($sid);
    my $lr     = $self->cropRawLogFile();
    my $cntnt_ .= qq~
    <script type="text/javascript" src="js/easytabs-init.js"></script>
    <div class="pheix-tabs-container">
     <div id="horizontalTab">
      <ul class="resp-tabs-list">
       <li role="tab"
           aria-controls="tab_item-0"
           class="stxt resp-tab-item">Статистика за неделю</li>
       <li role="tab"
           aria-controls="tab_item-1"
           class="stxt resp-tab-item">Лог системы</li>
      </ul>
      <div class="resp-tabs-container">
       <div id="tab_item-0">$stats</div>
       <div id="tab_item-1">
        <div class="stats-padding">
         <div class="srchinputs">
          <input id="srchreq" type="text"
                 class="input-styled srch-inpt"
                 placeholder="Подсказка по поиску: * &ndash; найти всё, !(запрос) &ndash; найти всё кроме">
          <input type="button"
                 value="Искать"
                 class="installbutton srch-btn"
                 onclick="javascript:doInlineDataLoad('statsdat', '$ENV{SCRIPT_NAME}?id=$sid&amp;action=admstatssrch&amp;query='+jQuery('#srchreq').val());">
         </div>
         <div id="statsdat">$rawlog</div></div>
       </div></div>
      </div>
     </div>
    </div>
    ~;
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# doUrlReplacements ( $url )
#------------------------------------------------------------------------------
#
# $url - url страницы.
# Выполнить подстановки в url.
#
#------------------------------------------------------------------------------

sub doUrlReplacements {
    my ($self, $url) = @_;
    if ($url =~ /\/admin.pl/) {
        $url =~ s/\/admin.pl\?id\=[a-z0-9]+\&//i;
        $url = '?'.$url;
    } else {
        if ($url =~ /https?\:\/\//) {
            my $_url = Config::ModuleMngr::getProtoSrvName();
            $url =~ s/$_url//gi
        }
    }
    return $url;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showRawLogsTable ( $sid, $query )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# $query - поисковый запрос.
# Получение таблицы с записями из лога модуля.
#
#------------------------------------------------------------------------------

sub getRawLogsTable {
    my ($self, $sid, $_query) = @_;
    my $cntnt_;
    my $query;
    my $query_stat = 0;
    if ($_query !~ /^\!\((.{1,})\)$/) {
        $query = $_query;
    } else {
        $query = $1;
        $query_stat = 1;
    }
    my $_qquery = quotemeta($query);
    if (-e $self->{mod_logf}) {
        open my $fh, "<", $self->{mod_logf}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @curr_day_stat = <$fh>;
        @curr_day_stat = reverse(@curr_day_stat);
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $i (0..$#curr_day_stat) {
            $curr_day_stat[$i] =~ s/[\r\n+]//g;
            my $date;
            my @tmp = split(/\|/,$curr_day_stat[$i]);
            if ($tmp[1] =~ /^[\d]+$/) {
                $date = localtime($tmp[1]);
                $date =~ s/[\s]+/ /gi;
            } else {
                $date = 'undef';
            }

            if ( $_qquery &&
                 ($curr_day_stat[$i] !~ /$_qquery/i) &&
                 ($date !~ /$_qquery/i) && !$query_stat) { next; }

            if ( $_qquery &&
                 (($curr_day_stat[$i] =~ /$_qquery/i) ||
                 ($date =~ /$_qquery/i)) && $query_stat == 1) { next; }

            if ($tmp[0] ne '' && $tmp[0] !~ /admsets\.html/) {
                my $page = $self->doUrlReplacements($tmp[0]);
                $tmp[0] =~ s/\?id\=([a-z0-9])+/\?id\=$sid/i;
                $tmp[0] = qq~<a href="$tmp[0]"
                       target="_blank"
                       class="statloglink">$page</a>~;
            }

            if ($tmp[2] ne '' ) {
                $tmp[2] = qq~
                    <a href="http://www.checkip.com/ip/$tmp[2]"
                       target="_blank"
                       class=statloglink>$tmp[2]</a>~;
            }
            my $_ref;
            if ($tmp[3] ne '' && $tmp[3] ne 'undef' && $tmp[3] ne 'undefined') {
                my $page = $self->doUrlReplacements($tmp[3]);
                my $_rlen = length $page;
                if ($_rlen > 100) {
                    $page = substr( $page, 0, 100 ) . '...';
                }
                $tmp[3] =~ s/\?id\=([a-z0-9])+/\?id\=$sid/i;
                $tmp[3] = qq~<a href="$tmp[3]"
                       target="_blank"
                       class=statloglink>$page</a>~;
                $_ref = "<p><span>Реферрер: </span>$tmp[3]</p>";
            }
            my $_tborder;
            if (!$cntnt_) {
                $_tborder = 'statslogrec-fullbrdr';
            }
            my $_dt = $date ne 'undef' ? "<p><span>Дата: </span>$date</p>" : '';
            my $_cntry_lc = lc $tmp[-1];
            my $_ulogin   = q{};
            my $_uid      = $tmp[7];
            if ( $_uid > 0 ) {
                my $lcrypt = getCryptLoginByDecryptUid($_uid);
                my $ktag   = getKeytagByDecryptUid($_uid);
                my $login  = q{};
                eval {
                    $login =
                      doBlowfishDecrypt(
                        getStringFromHexStr($lcrypt), $ktag );
                };
                if ( $login ne '' ) {
                    $_ulogin =
                        "<p><span>Пользователь: </span><a href=\"javascript:" .
                        "javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}',".
                        "'&action=admeditsecurityuser&userid=$_uid&page=0'" .
                        ")\">" . $login . "</a></p>";
                }
                else {
                    $_ulogin =
                        "<p><span>Пользователь: </span><span class=error>" .
                        "удален из системы, UID=$_uid</span></p>"
                }
            }
            my $emojiflag = $self->getEmojiFlag($tmp[-1]);
            $cntnt_ .= qq~
            <div class="statslogrec $_tborder">
              <div class="statslogrec-cont">
                <p><span>Цель: </span>$tmp[0]</p>
                $_dt
                <p><span>IP: </span>$tmp[2]</p>
                $_ref
                <p><span>Браузер (user-agent): </span>$tmp[4]</p>
                <p><span>Экран: </span>$tmp[5]</p>
                <p><span>Трек: </span>$tmp[6]</p>
                $_ulogin
              </div>
              <div class="statslogrec-location _phx-fs2x" title="$tmp[-1]">
                $emojiflag
              </div>
            </div>~;
        }
    } else {
        $cntnt_ .= qq~<div class=error>Файл $self->{mod_logf} не найден!</div>~;
    }
    if ($cntnt_ eq '' && $query) {
        $cntnt_ .= qq~<div class=error>По запросу $_query ничего не найдено</div>~;
    }
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWeekTable ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# Получение таблицы с со статистикой за неделю.
#
#------------------------------------------------------------------------------

sub getWeekTable {
    my ($self, $sid) = @_;
    my $h_1 = "Статистика посещаемости главной страницы за прошедшую неделю:";
    my $h_2 = "График посещаемости главной страницы за прошедшую неделю:";
    my $cntnt_;
    if (-e $self->{mod_logf}) {
        open my $fh, "<", $self->{mod_logf}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @curr_day_stat = <$fh>;
        @curr_day_stat = reverse(@curr_day_stat);
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        my @week_times = $self->getWeekTimes();
        my @week_dates = $self->getWeekDates(@week_times);
        $cntnt_ .= qq~
           <div class="stats-padding">
           <p class="topheader stxt"><b>$h_1</b></p>
           <table class="statslogtab"><tr>
             <th width="40%">Дата</th>
             <th width="20%">Хосты</th><th width="20%">Визиты</th>
             <th width="20%">Хиты</th>
            </tr>~;
        for my $i (0..$#week_dates) {
            my $date_class;
            my @daystats= $self->getDayStats($week_dates[$i]);
            my $date = $week_dates[$i];
            if ($date =~ /^Sat/ || $date =~ /^Sun/) {
                $date_class = 'weekend';
            } else {
               $date_class = '';
            }
            $cntnt_ .= qq~<tr>
            <td class="$date_class" width="40%">$week_dates[$i]<!-- / <span class="greytext">$daystats[3] records</span>--></td>
            <td class="$date_class" width="20%">$daystats[0]</td>
            <td class="$date_class" width="20%">$daystats[1]</td>
            <td class="$date_class" width="20%">$daystats[2]</td></tr>~;
        }
        my $plotdata = $self->getPlotData();
        $cntnt_ .= qq~</table>
            <script type="text/javascript">
                var sampleData = [ $plotdata ];
            </script>
            <script type="text/javascript" src="js/jwidgets-sets.js"></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                   if (jQuery("div").is("#chartContainer"))  {
                       jQuery('#chartContainer').jqxChart(settings);
                   }
                });
            </script>
            <p class="plotheader stxt"><b>$h_2</b></p>
            <div id='chartContainer' class="stats-plot"></div>
            </div>
        ~;
    } else {
        $cntnt_ .= qq~<div class=error>Файл $self->{mod_logf} не найден!</div>~;
    }
    return $cntnt_;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWeekTimes ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получение списка unix-time для дней предыдущей недели.
#
#------------------------------------------------------------------------------

sub getWeekTimes {
    my ($self) = @_;
    return (   time,
            time - 86400,
            time - 172800,
            time - 259200,
            time - 345600,
            time - 432000,
            time - 518400,
            time - 604800 );
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getWeekDates ( @time_for_weekdays )
#------------------------------------------------------------------------------
#
# @time_for_weekdays - массив unixtime отпечатков для дней недели.
# Получение списка дат для дней предыдущей недели.
#
#------------------------------------------------------------------------------

sub getWeekDates {
    my ($self, @time_for_weekdays) = @_;
    my @dates_for_weekdays;
    for my $j ( 0..$#time_for_weekdays ) {
        my $weekdate = localtime($time_for_weekdays[$j]);
        $weekdate =~ s/(\d\d\:\d\d\:\d\d)//gi;
        $weekdate =~ s/[\s]+/ /gi;
        my @date_ = split(/\s/,$weekdate);
        push(@dates_for_weekdays, $weekdate);
   }
   return @dates_for_weekdays;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDayStats ( $day_date )
#------------------------------------------------------------------------------
#
# $day_date - unixtime отпечаток для заданного дня недели.
# Получение статистики для заданной даты.
#
#------------------------------------------------------------------------------

sub getDayStats {
    my ($self, $day_date) = @_;
    my @day_stats;
    my @data_stats_for_a_day = ('-','-','-');
    if (-e $self->{mod_logf}) {
        open my $fh, "<", $self->{mod_logf}
            or die "unable to open " . $self->{mod_tmpl} . ": $!";
        my @curr_day_stat = <$fh>;
        close $fh or die "unable to close " . $self->{mod_tmpl} . ": $!";
        for my $i ( 0..$#curr_day_stat ) {
            my @tmp = split(/\|/,$curr_day_stat[$i]);
            my $dlog_ = localtime($tmp[1]);
            $dlog_ =~ s/(\d\d\:\d\d\:\d\d)//gi;
            $dlog_ =~ s/[\s]+/ /gi;
            if ( $dlog_ eq $day_date ) {
                push(@day_stats,$curr_day_stat[$i]);
            }
        }
        @data_stats_for_a_day = $self->parseDayStats(@day_stats);
    }
    return (@data_stats_for_a_day, ($#day_stats+1));
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDayStats ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получение JSON для отображения на графике.
#
#------------------------------------------------------------------------------

sub getPlotData {
    my ($self) = @_;
    my @plot_data;
    my @plot_week_times = $self->getWeekTimes();
    my @plot_week_dates = $self->getWeekDates(@plot_week_times);
    for my $inx (0..$#plot_week_dates) {
        my $date = $plot_week_dates[$inx];
        my @plot_day_stats = $self->getDayStats($plot_week_dates[$inx]);
        if ($date) {
            $date =~ s/(\s[\d]+)$//;
        }
        push @plot_data,
            "{ Day:'$date',".
            "host:$plot_day_stats[0],".
            "visit:$plot_day_stats[1],".
            "hit:$plot_day_stats[2] },";
    }
    return join("\n",reverse(@plot_data));
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getDayStats ( @curr_day_stat )
#------------------------------------------------------------------------------
#
# @curr_day_stat - часть rawlog-файла с данными за заданный диапазон дней.
# Парсинг части лога (хосты, визиты, хиты).
#
#------------------------------------------------------------------------------

sub parseDayStats {
    my ($self, @curr_day_stat) = @_;

    ### Только главная страница ####
    my @main;
    for my $i (0..$#curr_day_stat) {
        # print("***: $curr_day_stat[$i]");
        if ( $curr_day_stat[$i] =~ /^\/index\.html\|/ ||
             $curr_day_stat[$i] =~ /^\/\|/) {
            push @main, $curr_day_stat[$i];
        }
    }

    @curr_day_stat = @main;
    @curr_day_stat  = sort {$a cmp $b} @curr_day_stat;

    #Получили число хитов за текущий день
    my $hits = $#curr_day_stat+1;

    my @hosts= map { (split /[|]/,$_)[2] } @curr_day_stat;
    my @unique_hosts = do { my %seen; grep { !$seen{$_}++ } @hosts };

    #Получили число хостов за текущий день
    my $hosts = $#unique_hosts+1;

    my @tracks = map { (split /[|]/,$_)[6] } @curr_day_stat;
    my @unique_tracks = do { my %seen; grep { !$seen{$_}++ } @tracks };

    #Получили число посетителей за текущий день
    my $visitors = $#unique_tracks+1;

    #print"<p>@unips</p>";
    return ($hosts,$visitors,$hits)
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# cropRawLogFile ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Обрезка лог-файла (удаление статитстики старше 1 недели).
#
#------------------------------------------------------------------------------

sub cropRawLogFile {
    my ($self)     = @_;
    my @lt = localtime();
    my $curr_midnight = time -
        ( ($lt[2] * 3600) + ($lt[1] * 60) + $lt[0] );
    if (-e $self->{mod_logf}) {
        open my $fh, "<", $self->{mod_logf}
            or die "unable to open " . $self->{mod_logf} . ": $!";
        my @recs = <$fh>;
        close $fh or die "unable to close " . $self->{mod_logf} . ": $!";
        if ($recs[0] && $recs[0] !~ /^[\s\r\n\t]+$/) {
            my $crop_stamp = $curr_midnight - 604800;
            my @fr_ = split /[|]/, $recs[0];
            if ($fr_[1] =~ /^[\d]+$/) {
                if ($fr_[1] <= $crop_stamp) {
                    my @uprecs;
                    for my $i (0..$#recs) {
                        my @r_ = split /[|]/, $recs[$i];
                        if ($r_[1] > $crop_stamp) {
                            push @uprecs, $recs[$i];
                        }
                    }
                    if (@uprecs) {
                        open $fh, ">", $self->{mod_logf}
                            or die "unable to open " .
                            $self->{mod_logf} . ": $!";
                        flock( $fh, LOCK_EX )
                            or die "unable to lock mailbox: $!";
                        print $fh @uprecs;
                        truncate $fh, tell($fh);
                        flock( $fh, LOCK_UN )
                            or die "unable to unlock mailbox: $!";
                        close $fh
                            or die "unable to close " .
                            $self->{mod_logf} . ": $!";
                        return 1;
                    }
                }
            }
        }
    }
    return 0
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getEmojiFlag($ca_code)
#------------------------------------------------------------------------------
#
# $ca_code - двусимвольный код страны.
# Получить Emoji флаг страны.
#
#------------------------------------------------------------------------------

sub getEmojiFlag {
    my $self      = shift;
    my ($ca_code) = @_;
    my $flag      = '🏴‍☠️';

    if ($ca_code && $ca_code ne 'undef' && $ca_code =~ m/^[A-Z]{2}$/) {
        my $utf_flag =
            sprintf "%s%s",
                chr(0x1f1e6 - 0x41 + ord(substr(uc($ca_code), 0, 1))),
                    chr(0x1f1e6 - 0x41 + ord(substr(uc($ca_code), 1, 1)));

        $flag = encode('UTF-8', $utf_flag);
    }

    return $flag;
}

#------------------------------------------------------------------------------

END { }

1;
