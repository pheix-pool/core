#!/bin/bash

printf "Do you really want to erase Shopcat? [Y/*]: "
read CONFIRM

if [ "$CONFIRM" == "Y" ]; then
    if [ -d "../www/conf/pages/shopcat2" ]; then
        rm -fr ../www/conf/pages/shopcat2
    fi

    if [ -d "../www/conf/system/pricepool" ]; then
        rm -fr ../www/conf/system/pricepool
    fi

    if [ -d "../www/images/shopcat" ]; then
        rm -fr ../www/images/shopcat
    fi

    rm -f ../www/conf/system/shopcat2*
    rm -f ../www/conf/system/.sc2*
    echo Erased!
else
    echo Skipped
fi
