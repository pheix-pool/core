package Pheix::Pages;

use strict;
use warnings;

##############################################################################
#
# File   :  Pages.pm
#
##############################################################################
#
# Главный пакет управления выводом страниц в пользовательской части CMS Pheix
#
##############################################################################

our ( @ISA, @EXPORT );
BEGIN {

    use lib '../';

    # fill_common_cms_tags      -> fillCommonTags
    # show_main_page            -> showHomePage

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      fillCommonTags
      showHomePage
      showSitemap
    );
}

# Поддержка UTF-8 на уровне переменных, regexp и строковых функций
use POSIX;
use File::Basename;
use Encode;
use utf8;
use open qw(:std :utf8);
use Pheix::Tools qw(
    getSingleSetting
    getSettingFromGroupByAttr
    getPheixVersion
    getDateYear
    getConfigValue
    getInstAppMods
    getProtoSrvName
    getHexStrFromString
);

#------------------------------------------------------------------------------

my $STATIC_PAGES_PATH   = 'conf/pages';
my $INDEX_TEMPLATE_PATH = 'conf/config/index.html';
my $MODULE_NAME         = 'Pheix';
my $_PSET               =  getSingleSetting('Config', 0, 'workviaproto') || 0;
my $PROTO               =  ($_PSET == 1) ? 'https://' : 'http://';

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# $rc = fillCommonTags ( $input, $pageaction, $modulename )
#------------------------------------------------------------------------------
#
# $input - строка шаблона template.html
# $pageaction - название action действия обрабатываемой страницы.
# $modulename - имя вызывающего функцию модуля.
# Функция обработки общих cms-тегов шаблона.
# Функция возвращает строку $input, в которую вставлены общие cms-теги шаблона.
#
#------------------------------------------------------------------------------

sub fillCommonTags {
    my ($input, $pageaction, $callmodulename, $modulespecific) = @_;
    my @seotags_from_db;
    my $_area = 1;
    if ($callmodulename eq $MODULE_NAME) {
        $callmodulename = 'Config';
        $_area = 0;
    }
    my @seo_data_strings = (
        1,
        $pageaction,
        getSettingFromGroupByAttr('Config', 0, 'pageseotags', 'action', 'index', 'title'),
        getSettingFromGroupByAttr('Config', 0, 'pageseotags', 'action', 'index', 'metadescr'),
        getSettingFromGroupByAttr('Config', 0, 'pageseotags', 'action', 'index', 'metakeywords')
    );

    #eval { @seotags_from_db = Seo::Tags::getSeoTags($pageaction); };
    #if ( $#seotags_from_db > -1 ) { @seo_data_strings = @seotags_from_db; }

    if ( $input =~ /\%pageaction\%/ ) {
        $input =~ s/\%pageaction\%/$pageaction/;
    }
    if (   $input =~ /\%page_title\%/
        || $input =~ /\%metadesc\%/
        || $input =~ /\%metakeys\%/ )
    {
        if ( $input =~ /\%page_title\%/ ) {
            if ($callmodulename ne '') {
                my $title = getSettingFromGroupByAttr($callmodulename, $_area, 'pageseotags', 'action', $pageaction, 'title');
                if ($title) { $seo_data_strings[2] = $title; }
            }
            $input =~ s/\%page_title\%/$seo_data_strings[2]/;
        }
        if ( $input =~ /\%metadesc\%/ ) {
            if ($callmodulename ne '') {
                my $meta1 = getSettingFromGroupByAttr($callmodulename, $_area, 'pageseotags', 'action', $pageaction, 'metadescr');
                if ($meta1) { $seo_data_strings[3] = $meta1; }
            }
            $input =~ s/\%metadesc\%/$seo_data_strings[3]/;
        }
        if ( $input =~ /\%metakeys\%/ ) {
            if ($callmodulename ne '') {
                my $meta2 = getSettingFromGroupByAttr($callmodulename, $_area, 'pageseotags', 'action', $pageaction, 'metakeywords');
                if ($meta2) { $seo_data_strings[4] = $meta2; }
            }
            $input =~ s/\%metakeys\%/$seo_data_strings[4]/;
        }
    }
    if ( $input =~ /\%modulename\%/ ) {
        if (defined($callmodulename)) {
            $input =~ s/\%modulename\%/$callmodulename/;
        } else {
            $input =~ s/\%modulename\%/$MODULE_NAME/;
        }
    }
    if ( $input =~ /\%modulecode\%/ ) {
        if (defined($callmodulename)) {
            my $package_prefix;
            if ($callmodulename eq 'Weblog') {
                $package_prefix = "Weblog::Static";
            } elsif ($callmodulename eq 'Feedbck') {
                $package_prefix = "Feedbck::Casual";
            } elsif ($callmodulename eq 'Gallery') {
                $package_prefix = "Gallery::System";
            } elsif ($callmodulename eq 'Lightshop') {
                $package_prefix = "Lightshop::Sales";
            } elsif ($callmodulename eq 'News') {
                $package_prefix = "News::Archive";
            } elsif ($callmodulename eq 'Shopcat') {
                $package_prefix = "Shopcat::V3";
            } else {
                ;
            }
            if (defined($package_prefix)) {
                my $modulecode;
                #eval ( "\$modulecode = $package_prefix\:\:getModJsCssCode();" );
                eval { $modulecode = $package_prefix->getModJsCssCode(); };
                if (!$@ && defined($modulecode)) { $input =~ s/\%modulecode\%/$modulecode/; } else {
                    $input =~ s/\%modulecode\%//;
                }
            } else {
                $input =~ s/\%modulecode\%//;
            }
        } else {
            $input =~ s/\%modulecode\%//;
        }
    }
    if ( $input =~ /\%timestamp\%/ ) {
        my $_ts = getHexStrFromString(time);
        $input =~ s/\%timestamp\%/$_ts/;
    }
    if ( $input =~ /\%version\%/ ) {
        my $ver = getPheixVersion();
        $input =~ s/\%version\%/$ver/;
    }
    if ( $input =~ /\%update\%/ ) {
        my $format_date = getDateYear();
        $input =~ s/\%update\%/$format_date/;
    }
    if ( $input =~ /\%bigbro_code\%/ ) {
        my $code = qq~<script type="text/javascript">doBigBroLook('$ENV{REQUEST_URI}',0);</script>~;
        my $show = getSingleSetting('Stats', 1, "insertcodetouser") || 0;
        if (!$show) {
            $code = q{};
        }
        $input =~ s/\%bigbro_code\%/$code/;
    }
    if ( $input =~ /\%servername\%/ ) {
        my $servername = $PROTO.$ENV{SERVER_NAME};
        $input =~ s/\%servername\%/$servername/;
    }
    if ( $input =~ /\%scriptname\%/ ) {
        my $scriptname = $ENV{SCRIPT_NAME};
        $input =~ s/\%scriptname\%/$scriptname/;
    }
    if ( $input =~ /\%lightshopcart\%/ ) {
        my $cartmenu = q{};
        eval { $cartmenu = Lightshop::Sales::getCartLink(); };
        $input =~ s/\%lightshopcart\%/$cartmenu/g;
    }
    if ( $input =~ /\%lightshopcartquantity\%/ ) {
        my $cartquantity = 0;
        eval { $cartquantity = Lightshop::Sales::get_cart_block_goods(); };
        $input =~ s/\%lightshopcartquantity\%/$cartquantity/g;
    }
    if ( $input =~ /\%lightshopcartcost\%/ ) {
        my $lightshopcartcost = 0;
        eval { $lightshopcartcost = Lightshop::Sales::get_cart_cost(); };
        $input =~ s/\%lightshopcartcost\%/$lightshopcartcost/g;
    }
    if ( $input =~ /\%shopcat_rnd_items\%/ ) {
        $input =~ s/\%shopcat_rnd_items\%//g;
    }
    if ( $input =~ /\%shopcat_([\d]+)_([\d]+)\%/ ) {
        my $inline = q{};
        eval { $inline = Shopcat::V3::getV3HomePage($1, $2, 'shopcatalog', 0); };
        $input =~ s/\%shopcat_([\d]+)_([\d]+)\%/$inline/;
    }
    if ( $input =~ /\%shopcat_itemid\%/ ) {
        $input =~ s/\%shopcat_itemid\%/0/g;
    }
    if ( $input =~ /\%shopcattree\%/ ) {
        my $shopcattree = q{};
        eval { $shopcattree = Shopcat::V3::showLightNaviTree($modulespecific); };
        $input =~ s/\%shopcattree\%/$shopcattree/g;
    }
    if ( $input =~ /\%weblog-page-header\%/ ) {
        my $rc;
        eval {
            $rc = Weblog::Static::get_power_status_for_module();
        };
        if ($@) {
            $input =~ s/\%weblog-page-header\%//;
        }
        else {
            if (defined($rc)) {
                $input =~ s/\%weblog-page-header\%//;
            }
        }
    }
    if ( $input =~ /\%weblog-annonce-list\%/ ) {
        my $rc;
        eval {
            $rc = Weblog::Static::getAnnonceRecsList();
        };
        if ($@) {
            $input =~ s/\%weblog-annonce-list\%//;
        }
        else {
            if (defined($rc)) {
                $input =~ s/\%weblog-annonce-list\%/$rc/;
            }
            else {
                $input =~ s/\%weblog-annonce-list\%//;
            }
        }
    }
    if ( $input =~ /\%metatags\%/ ) {
        eval {
            # meta теги для модуля Weblog обрабатываются внутри модуля
            # функцией  Weblog::Static::_inner_weblog_fill_seotags()!

            if ($callmodulename eq 'Feedbck') {
                # код обработки meta тегов для модуля Feedbck
                $input =~ s/\%metatags\%//;
            } elsif ($callmodulename eq 'Gallery') {
                # код обработки meta тегов для модуля Gallery
                $input =~ s/\%metatags\%//;
            } elsif ($callmodulename eq 'Lightshop') {
                # код обработки meta тегов для модуля Lightshop
                $input =~ s/\%metatags\%//;
            } elsif ($callmodulename eq 'News') {
                # код обработки meta тегов для модуля News
                $input =~ s/\%metatags\%//;
            } elsif ($callmodulename eq 'Shopcat') {
                # код обработки meta тегов для модуля Shopcat
                $input =~ s/\%metatags\%//;
            } else {
                $input =~ s/\%metatags\%//;
            }
        };
        if ($@) { $input =~ s/\%metatags\%//; }
    }
    return $input;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showHomePage ( $pageaction )
#------------------------------------------------------------------------------
#
# $pageaction - название action действия обрабатываемой страницы.
# Промежуточная функция печати главной страницы пользовательской части из CMS.
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showHomePage {
    my ($pageaction) = @_;
    my $template = $INDEX_TEMPLATE_PATH;
    #if ($ENV{SERVER_NAME} !~ /tarqvara\.apopheoz\.ru/) {
    #    $template = 'conf/config/index-2.html';
    #}
    if ( -e $template ) {
        open my $fh, "<", $template;
        my @scheme = <$fh>;
        for my $cnt (0..$#scheme) {
            $scheme[$cnt] =
              fillCommonTags( $scheme[$cnt], $pageaction, $MODULE_NAME );
            if ( $scheme[$cnt] =~ /\%content\%/ ) {
                my $content = _showStaticPage($pageaction.'.txt');
                $scheme[$cnt] =~ s/\%content\%/$content/;
            }
            print $scheme[$cnt];
        }
        close $fh;
    }
    else {
        print "showHomePage(): $template - not found!";
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# _showStaticPage ( $filename )
#------------------------------------------------------------------------------
#
# $filename - имя файла.
# Промежуточная функция вывода некоторой статической страницы из CMS.
# Функция возвращает содержимое файла conf/pages/$filename.
#
#------------------------------------------------------------------------------

sub _showStaticPage {
    my ($filename) = @_;
    my $pagesmodule_content;
    if ( -e "$STATIC_PAGES_PATH/$filename" ) {
        open my $fh, "<", "$STATIC_PAGES_PATH/$filename";
        $pagesmodule_content = join( "", <$fh> );
        close $fh;
    }
    else {
        $pagesmodule_content = qq~
        <p class="error">
        _showStaticPage(): $STATIC_PAGES_PATH/$filename - not found!</p>~;
    }
    return $pagesmodule_content;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showSitemap ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Функция генерирует файл sitemap.xml.
#
#------------------------------------------------------------------------------

sub showSitemap {
    my $_urltmpl = "\t<url><loc>%loc%</loc><lastmod>%lastmod%</lastmod></url>\n";
    my @_smap = (
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">',
        $_urltmpl,'</urlset>'
    );
    my $_root = getProtoSrvName();
    my $_rtup = strftime '%Y-%m-%d', localtime(time);
    $_smap[2] =~ s/%loc%/$_root/gi;
    $_smap[2] =~ s/%lastmod%/$_rtup/gi;
    my @_imods = getInstAppMods();
    if (@_imods) {
        foreach my $mod (@_imods) {
            my $_exec_expr = getConfigValue(
                dirname($mod),
                basename($mod),
                'use_identifier'
            );
            my $_smfunc = getConfigValue(
                dirname($mod),
                basename($mod),
                'sitemapfunction'
            );
            if ($_exec_expr && $_smfunc) {
                my @_sm;
                eval { @_sm = $_exec_expr->$_smfunc(); };
                if (@_sm) {
                    my $_smdata;
                    foreach my $ref (@_sm) {
                        my %_sm = %{$ref};
                        my $_urltmplcp = $_urltmpl;
                        $_urltmplcp =~ s/%loc%/$_sm{loc}/gi;
                        $_urltmplcp =~ s/%lastmod%/$_sm{lastmod}/gi;
                        $_smdata .= $_urltmplcp;
                    }
                    if ($_smdata) {
                        $_smap[2] .= $_smdata;
                    }
                }
            }
        }
    }
    chomp($_smap[2]);
    return join "\n", @_smap;
}

#------------------------------------------------------------------------------

END { }

1;
