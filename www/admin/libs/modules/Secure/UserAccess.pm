package Secure::UserAccess;

use strict;
use warnings;

##############################################################################
#
# File   :  UserAccess.pm
#
##############################################################################
#
# Главный пакет управления безопасностью системы
#
##############################################################################

our ( @ISA, @EXPORT );

BEGIN {

    use lib '../';
    use lib '../../extlibs';

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      showUAHomePage
      showBruteForceLog
      showAccessDenied
      getAccessDenied
      getHomePage
      isBruteForce
      getUserAccessTab
      getBruteForceLog
      getUserAddForm
      getUserEditForm
      doPasswordGen
      setUserDetails
      getModfyPasswdForm
      showExitForm
      doMd5HashesUpdate
      moveRecordUp
      moveRecordDown
      delUserDetails
      getUserDetails
      isAccessDenied
      getAccessMode
      modfyUserDetails
      delBruteForceRecord
    );
}

use Config::ModuleMngr qw(
    showUpdateHint
    getModXml
    getConfigValue
);
use Pheix::Session qw(
    getKeytagByCryptUid
    doBlowfishDecrypt
    getStringFromHexStr
    getCryptLoginByDecryptUid
    getCryptLoginBySid
    getHexStrFromString
    doBlowfishEncrypt
);
use Pheix::Tools qw(getDateUpdate);
use Digest::MD5 qw(md5_hex);
use Crypt::CBC;
use POSIX;

#---------------------------------------------------------------------------------

my $MODULES_PATH    = 'admin/libs/modules';
my $MODULE_DB_FILE  = "admin/system/client_pwd.tnk";
my $BRUTEFORCE_PATH = 'admin/system/bruteforcing.tnk';
my $ADMIN_SKIN_PATH = 'admin/skins/classic_.txt';

my $BASE_VERSION  = 2.2;
my $MODULE_NAME   = "Secure";
my $MODULE_FOLDER = "$MODULES_PATH/$MODULE_NAME/";

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# showUAHomePage ( $sid )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод содержимого стартовой страницы модуля "Безопасность".
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub showUAHomePage {
    my ($sid) = @_;
    open( my $fh, "<", $ADMIN_SKIN_PATH );
    my @_templ= <$fh>;
    for my $i (0..$#_templ) {
        $_templ[$i] =
          Pheix::Tools::fillCommonTags( $sid, $_templ[$i] );
        if ( $_templ[$i] =~ /\%content\%/ ) {
            my $_cntnt = getHomePage(@_);
            $_templ[$i] =~ s/\%content\%/$_cntnt/;
        }
        print $_templ[$i];
    }
    close $fh;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# showBruteForceLog ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы с журналом брутфорса пароля для входа.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub showBruteForceLog {
    my ($sid) = @_;
    open( my $fh, "<", $ADMIN_SKIN_PATH );
    my @_templ= <$fh>;
    for my $i (0..$#_templ) {
        $_templ[$i] =
          Pheix::Tools::fillCommonTags( $sid, $_templ[$i] );
        if ( $_templ[$i] =~ /\%content\%/ ) {
            my $_cntnt = getBruteForceLog(@_);
            $_templ[$i] =~ s/\%content\%/$_cntnt/;
        }
        print $_templ[$i];
    }
    close $fh;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# showAccessDenied ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы c сообщением о блокировки доступа.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub showAccessDenied {
    my ($sid) = @_;
    open( my $fh, "<", $ADMIN_SKIN_PATH );
    my @_templ= <$fh>;
    for my $i (0..$#_templ) {
        $_templ[$i] =
          Pheix::Tools::fillCommonTags( $sid, $_templ[$i] );
        if ( $_templ[$i] =~ /\%content\%/ ) {
            my $_cntnt = getAccessDenied(@_);
            $_templ[$i] =~ s/\%content\%/$_cntnt/;
        }
        print $_templ[$i];
    }
    close $fh;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getAccessDenied ()
#---------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Функция формирования сообщения о блокировки доступа.
# Функция возвращает сообщение о блокировки доступа в HTML формате.
#
#---------------------------------------------------------------------------------

sub getAccessDenied {
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $security_content_variable = qq~
        <div class=a_hdr>$module_descr</div>
        <p class=error>Доступ запрещен!</p>~;
    return $security_content_variable;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getHomePage ( $sesion_id, $crypted_userid, $page, $status, $message )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $page - номер страницы, на которую нужно вернуть пользователя.
# $status - статус выполнения операции.
# $message - сообщение пользователю.
# Функция формирования содержимого главной страницы модуля "Безопасность".
# Функция возвращает содержимое главной страницы в HTML формате.
#
#---------------------------------------------------------------------------------

sub getHomePage {
    my ($sid, $enclgn, $refpage, $prfl, $stat, $mess ) = @_;
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $_cntnt = qq~<div class=a_hdr>$module_descr</div>~.showUpdateHint($prfl, $stat, $mess );
    $_cntnt .= qq~
        <div style="margin: 0px 0px 2px 0px; width:100%" class="container">
        <a  href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=addnewuserform')">
            Добавить нового пользователя в систему</a></div>
    ~;
    my $content_block = getUserAccessTab(@_);
    $_cntnt .= qq~$content_block~;
    return $_cntnt;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# isBruteForce ( $sesion_id, $pageaction )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $pageaction - название action действия обрабатываемой страницы
# Функция вывода предупреждения о зарегистрированных случаях bruteforce.
# Функция возвращает предупреждение в HTML формате.
#
#---------------------------------------------------------------------------------

sub isBruteForce {
    my ($sid, $reqact) = @_;
    my $_cntnt;
    if ( -e $BRUTEFORCE_PATH
         && $reqact !~ /admbruteforcelog/
         && $reqact !~ /admsecuritydelbf/ ) {
        my $_mes = qq~<p>Система безопасности зарегистрировала попытку подбора пароля.<br>\\
        <a href=\\"$ENV{SCRIPT_NAME}?id=$sid&amp;action=admbruteforcelog\\">Посмотреть протокол</a>~;
        $_cntnt = qq~
            (function(jQuery){
                jQuery(document).ready(function(){
                    jQuery.jGrowl.defaults.closer = false;
                    jQuery('\#securitytip').jGrowl("<div class=hintcont>$_mes</div>",
                    {
                        theme: 'updatehint',
                        speed: 'slow',
                        life: 3500,
                        animateOpen: {
                            height: "show",
                        },
                        animateClose: {
                            height: "hide",
                        }
                    });
                });
            })(jQuery);
        ~;
    }
    return $_cntnt;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getUserAccessTab ( $sesion_id, $crypted_userid, $page, $status, $message )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $page - номер страницы, на которую нужно вернуть пользователя.
# $status - статус выполнения операции.
# $message - сообщение пользователю.
# Функция вывода таблицы со списком зарегистрированных пользователей на
# галавную страницу модуля "Безопасность".
# Функция возвращает таблицу в HTML формате.
#
#---------------------------------------------------------------------------------

sub getUserAccessTab {
    my $script = $ENV{SCRIPT_NAME};
    my $security_content_variable;
    my $pix_on_page               = 4;
    my $curr_page                 = $_[2];
    my $login_from_session        = 0;
    my $user_id_crypted           = $_[1];
    my $user_id_from_session      = '';
    my $mykeytag = getKeytagByCryptUid($user_id_crypted);
    eval {
        $user_id_from_session =
          doBlowfishDecrypt(
            getStringFromHexStr($user_id_crypted), $mykeytag );
    };
    my $logincrypted =
      getCryptLoginByDecryptUid($user_id_from_session);
    eval {
        $login_from_session =
          doBlowfishDecrypt(
            getStringFromHexStr($logincrypted), $mykeytag );
    };
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @users_in_db = sort { $b cmp $a } <$inputFH>;
        close $inputFH;

        # Формирование навигации по страницам

        my $pages = floor( $#users_in_db / $pix_on_page );

        if ( ( $pages * $pix_on_page <= $#users_in_db ) || ( $pages < 1 ) ) {
            $pages++;
        }

        if ( $curr_page >= $pages ) { $curr_page = 0; }
        my $page_linx = '&nbsp;-&nbsp;';
        for ( my $i = 1 ; $i <= $pages ; $i++ ) {
            if ( ( $i - 1 ) != $curr_page ) {
                $page_linx .= "<a href=\"$script?id=$_[0]&amp;action=admsecurity&amp;page="
                  . ( $i - 1 )
                  . "\" class=simplelink>"
                  . $i
                  . "</a>&nbsp;-&nbsp;";
            }
            else {
                $page_linx .= qq~
                    <span class=lightadminpagelink>$i</span>&nbsp;-&nbsp;
                ~;
            }
        }
        if ( $curr_page >= $pages ) { $curr_page = $pages - 1; }
        my $end = ( ( $curr_page + 1 ) * $pix_on_page ) - 1;
        if ( $end > $#users_in_db ) { $end = $#users_in_db; }
        my @items_on_this_page =
          @users_in_db[ $curr_page * $pix_on_page .. $end ];

        # Формирование навигации по страницам

        if ( $#items_on_this_page > -1 ) {
            my $security_counter = 0;
            $security_content_variable .=qq~
                <table 
                    cellspacing="0"
                    cellpadding="10"
                    width="100%"
                    class="tabselect">
            ~;
            for (
                my $inxonpage = 0 ;
                $inxonpage <= $#items_on_this_page ;
                $inxonpage++
              )
            {
                my $markup_denied = qq~
                    <span style="color:#FF0000; font-weight:none;">
                        доступ запрещен</span>
                ~;
                my $markup_full = qq~
                    <span style="color:#008000; font-weight:none;">
                        полный доступ</span>
                ~;
                my $markup_preview = qq~
                    <span style="color:#DAA520; font-weight:none;">
                        только просмотр данных</span>
                ~;
                my $rand    = rand();
                my @tmp_arr = split( /\|/, $items_on_this_page[$inxonpage] );
                my $keytag  = $tmp_arr[5];
                my $login;
                my $fio;
                my $access_modules;
                my $access_security;
                my $access_config;

                # Выставление прав #

                if ( $tmp_arr[6] == 1 ) { $access_config = $markup_preview; }
                if ( $tmp_arr[6] == 2 ) { $access_config = $markup_full; }
                if ( $tmp_arr[6] == 3 ) { $access_config = $markup_denied; }

                if ( $tmp_arr[7] == 1 ) { $access_security = $markup_preview; }
                if ( $tmp_arr[7] == 2 ) { $access_security = $markup_full; }
                if ( $tmp_arr[7] == 3 ) { $access_security = $markup_denied; }

                if ( $tmp_arr[8] == 1 ) { $access_modules = $markup_preview; }
                if ( $tmp_arr[8] == 2 ) { $access_modules = $markup_full; }
                if ( $tmp_arr[8] == 3 ) { $access_modules = $markup_denied; }

                # Выставление прав #

                eval {
                    $login =
                      doBlowfishDecrypt(
                        getStringFromHexStr( $tmp_arr[3] ), $keytag );
                };
                eval {
                    $fio =
                      doBlowfishDecrypt(
                        getStringFromHexStr( $tmp_arr[2] ), $keytag );
                };
                my $bgcolor = 'class="tr01"';
                if ( $security_counter % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
                $tmp_arr[$#tmp_arr] =~ s/[\n]+//g;
                my $nextpage      = $curr_page;
                my $prevpage      = $curr_page;
                my $topagewhendel = $curr_page;
                if ( $inxonpage == $#items_on_this_page ) { $nextpage++; }
                if ( $inxonpage == 0 ) {
                    $prevpage--;
                    if ( $prevpage < 0 ) { $prevpage = 0; }
                }
                if ( $inxonpage == $#items_on_this_page ) {
                    $topagewhendel = $prevpage;
                }
                my $delmess = "Вы уверены, что хотите удалить пользователья $login безвозвратно?";
                my $delete_link = qq~
                    <a href="javascript:Confirm('$script?id=$_[0]&amp;action=admsecuritydeluser&amp;uid=$tmp_arr[1]&amp;page=$topagewhendel','$delmess')" class="fa-action-link">
                    <i class="fa fa-trash"
                       title="Удалить пользователья &quot;$login&quot из системы"></i></a>
                ~;
                if ( $login =~ /^rootadmin$/ || $login eq $login_from_session )
                {
                    $delete_link = qq~
                        <span class="fa-nonaction-span" title="Администратора &quot;$login&quot удалить нельзя"><i class="fa fa-trash"></i></span>
                    ~;
                }
                my $updownlinks = '';
                if ( $#items_on_this_page > 0 ) {
                    my $securitysortID = $tmp_arr[0];
                    $securitysortID =~ s/[\D]+//s;
                    $updownlinks = qq~
                        <a href="$script?id=$_[0]&amp;action=admsecuritymoveup&amp;sgid=$securitysortID&amp;page=$prevpage"
                           class="fa-action-link">
                           <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                        <a href="$script?id=$_[0]&amp;action=admsecuritymovedown&amp;sgid=$securitysortID&amp;page=$nextpage"
                           class="fa-action-link">
                           <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>
                    ~;
                    if ( $inxonpage == 0 && $curr_page == 0 ) {
                        $updownlinks = qq~
                            <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                            <a href="$script?id=$_[0]&amp;action=admsecuritymovedown&amp;sgid=$securitysortID&amp;page=$curr_page"
                               class="fa-action-link">
                               <i class="fa fa-angle-down" title="Передвинуть вниз"></i></a>
                        ~;
                    }
                    if (   $inxonpage == $#items_on_this_page
                        && $curr_page == ( $pages - 1 ) )
                    {
                        $updownlinks = qq~<a href="$script?id=$_[0]&amp;action=admsecuritymoveup&amp;sgid=$securitysortID&amp;page=$curr_page" class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                        ~;
                    }
                }
                else {
                    if ( $curr_page > 0 ) {
                        my $securitysortID = $tmp_arr[0];
                        $securitysortID =~ s/[\D]+//s;
                        $updownlinks = qq~
                            <a href="$script?id=$_[0]&amp;action=admsecuritymoveup&amp;sgid=$securitysortID&amp;page=$prevpage"
                               class="fa-action-link">
                            <i class="fa fa-angle-up" title="Передвинуть вверх"></i></a>
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                        ~;
                    }
                    else {
                        $updownlinks = qq~
                            <span class="fa-nonaction-span"><i class="fa fa-angle-up"></i></span>
                            <span class="fa-nonaction-span"><i class="fa fa-angle-down"></i></span>
                        ~;
                    }
                }
                $security_content_variable .= qq~
                    <tr $bgcolor style="height:20;">
                    <td width="10%" class="standart_text" align="center">$updownlinks</td>
                    <td width="80%" class="standart_text" style="font-size:11pt;">
                        <b>$login <span style="color:#C0C0C0">— $fio</span></b>
                        <div class=cmstext style="padding:10; font-size:10pt; color:#A0A0A0; line-height:1.4;">Конфигурирование: $access_config<br>
                        Управление безопасностью системы: $access_security<br>
                        Работа с установленными модулями: $access_modules</div></td>
                    <td width="10%" class="standart_text" align=right>
                        <a href="javascript:doSesChckLoad('$_[0]', '$ENV{SCRIPT_NAME}', '&amp;action=admeditsecurityuser&amp;userid=$tmp_arr[1]&amp;page=$curr_page')"
                           class="fa-action-link">
                           <i class="fa fa-pencil-square-o"
                              title="Редактировать данные пользователя &quot;$login&quot"></i></a>$delete_link</td></tr>
                ~;
                $security_counter++;
            }
            $security_content_variable .=
              qq~</table><p align=right>$page_linx</p>~;
        }
        else {
            $security_content_variable .= qq~
                <p class=error>Список записей пуст - файл $MODULE_DB_FILE пуст (\$#items_on_this_page = $#items_on_this_page)!</p>
            ~;
        }
    }    # if (-e "$MODULE_DB_FILE")
    else {
        $security_content_variable .= qq~
            <p class=error>Список записей пуст - файл $MODULE_DB_FILE не найден!</p>
        ~;
    }
    return $security_content_variable;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getBruteForceLog ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Функция вывода данных из файла bruteforcing.tnk в виде таблицы.
# Функция возвращает таблицу с данными из bruteforcing.tnk в HTML формате.
#
#---------------------------------------------------------------------------------

sub getBruteForceLog {
    my ($sid, $enclgn, $prfl, $stat, $mess ) = @_;
    my $script = $ENV{SCRIPT_NAME};
    my $xml_description_file = getModXml($MODULE_FOLDER);
    my $module_descr = getConfigValue( $MODULE_FOLDER,
        $xml_description_file, "description" );
    my $_hint = showUpdateHint( $prfl, $stat, $mess );
    my $security_content_variable = qq~
        <div class=a_hdr>$module_descr</div>
        $_hint
        <p class=standart_text>
        <a  href="$script?id=$sid&amp;action=admsecurity"
            class=simple_link><b>Безопасность</b></a>&nbsp;/&nbsp;
            Просмотр протокола регистрации подбора пароля
        </p>
    ~;

    if ( -e "$BRUTEFORCE_PATH" ) {
        my $found_ip_inlist = 0;
        open( my $inputFH, "<", $BRUTEFORCE_PATH );
        my @entries = <$inputFH>;
        close($inputFH);
        @entries = sort { $b cmp $a } @entries;
        $security_content_variable .= qq~
            <table  cellspacing="0"
                    cellpadding="10"
                    border="0"
                    width="100%"
                    class="bruteforcetab">
            <tr class="standart_text" style="font-style:italic;">
            <td width="33%" class="bft_bg">Дата запроса</td>
            <td width="33%" class="bft_bg">Удаленный IP</td>
            <td width="24%"
                class="bft_bg"
                style="border-width: 1px 0 0 0;">Всего попыток входа</td>
            <td width="10" class="bft_bg">&nbsp;</td></tr>
        ~;
        for ( my $kinx = 0 ; $kinx <= $#entries ; $kinx++ ) {
            my @tmp_arr = split( /\|/, $entries[$kinx] );
            my $date = localtime( $tmp_arr[0] );
            $security_content_variable .= qq~
                <tr class="standart_text">
                <td width="33%">$date</td>
                <td width="33%">
                <a  href="http://www.checkip.com/ip/$tmp_arr[1]"
                    target="_blank"
                    class=simplelink>$tmp_arr[1]</td>
                <td width="24%" style="border-width: 1px 0 0 0;">$tmp_arr[3]</td>
                <td width="10" align=right>
                <a href="$script?id=$sid&amp;action=admsecuritydelbf&amp;bfid=$tmp_arr[0]">
                <img    src="images/admin/cms/png/delete_16x16.png"
                        border=0 alt="Удалить запись"
                        title="Удалить запись"></td></a></tr>
            ~;
        }
        $security_content_variable .= qq~</table>~;
    }
    else {
        $security_content_variable .= qq~
            <div><p class="error">
                Файл admin/system/bruteforcing.tnk не найден!
            </p></div>
        ~;
    }
    return $security_content_variable;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getUserAddForm ( $sesion_id, $page )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $page - номер страницы, на которую нужно вернуть пользователя.
# Функция вывода главной формы создания нового пользователя.
# Функция возвращает форму в HTML формате.
#
#---------------------------------------------------------------------------------

sub getUserAddForm {
    my $sid                   = $_[0];
    my $page_ref              = $_[2];
    my $password              = doPasswordGen();
    my $security_adduser_date = getDateUpdate();
    my @_amods = getAccessMode( $sid );
    my @_dis1  = ('disabled', 'disabled');
    my @_dis2  = ('disabled', 'disabled');
    my @_dis3  = ('disabled', 'disabled');
    if ($_amods[0] == 1) {
        $_dis1[0] = '';
    } elsif ($_amods[0] == 2) {
        $_dis1[0] = '';
        $_dis1[1] = '';
    }

    if ($_amods[1] == 1) {
        $_dis2[0] = '';
    } elsif ($_amods[1] == 2) {
        $_dis2[0] = '';
        $_dis2[1] = '';
    }

    if ($_amods[2] == 1) {
        $_dis3[0] = '';
    } elsif ($_amods[2] == 2) {
        $_dis3[0] = '';
        $_dis3[1] = '';
    }
    my $security_adduser_form = qq~
    <div class="popup_header">Добавление нового пользователя</div>
        <div class=standart_text><a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsecurity&amp;page=$page_ref" class=simple_link>
           <b>Безопасность</b></a> / Добавление нового пользователя<br><br>
        </div>
        <form id="addweblog" method=post action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=securityadduser&amp;page=$page_ref">
         <div class="stxt frm-div-marg">ФИО: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input placeholder="Иванов Иван Иванович" required type=text name=securityfio class="input modal-input">
         <div class="stxt frm-div-marg">Логин: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input placeholder="ivan.ivanov" required type=text name=securitylogin class="input modal-input"></div>
         <div class="stxt frm-div-marg">Пароль: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input type=text placeholder="придумайте пароль" name=securitypassw required value="$password" class="input modal-input"></div>
         <div class="stxt frm-div-marg modal-tmargin">Права доступа:</font></div>
         <div class="modal-table">
            <div class="mt-row-th">
                <div class="mt-ua-cell-lgnd">Действия в системе</div>
                <div class="mt-ua-cell-data">Просмотр данных</div>
                <div class="mt-ua-cell-data">Полный доступ</div>
                <div class="mt-ua-cell-data">Доступ запрещен</div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Конфигурирование</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf1" hidden="" name="config" $_dis1[0] value="1" type="radio"><label for="ua-conf1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf2" hidden="" name="config" $_dis1[1] value="2" type="radio"><label for="ua-conf2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf3" hidden="" name="config" value="3" checked type="radio"><label for="ua-conf3" class="hoverable"></label>
                  </span>
                </div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Управление безопасностью системы</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec1" hidden="" name="security" $_dis2[0] value="1" type="radio"><label for="ua-sec1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec2" hidden="" name="security" $_dis2[1] value="2" type="radio"><label for="ua-sec2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec3" hidden="" name="security" value="3" checked type="radio"><label for="ua-sec3" class="hoverable"></label>
                  </span>
                </div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Работа с установленными модулями</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods1" hidden="" name="modules" $_dis3[0] value="1" type="radio"><label for="ua-mods1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods2" hidden="" name="modules" $_dis3[1] value="2" type="radio"><label for="ua-mods2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods3" hidden="" name="modules" value="3" checked type="radio"><label for="ua-mods3" class="hoverable"></label>
                  </span>
                </div>
            </div>
         </div>
         <div class="frm-div-marg modal-tmargin"><input type=submit value="Сохранить изменения" class="button modal-butt"></div>
    </form>~;
    return $security_adduser_form;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getUserEditForm ( $sesion_id, $crypted_userid, $page )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_userid - зашифрованный идентификатор пользователя.
# $target_userid - идентификатор пользователя, данные которого будут
# редактироваться (в незашифрованном виде).
# $page - номер страницы, на которую нужно вернуть пользователя.
# Функция вывода главной формы редактирования пользователя.
# Функция возвращает форму в HTML формате.
#
#---------------------------------------------------------------------------------

sub getUserEditForm {
    my ($sid, $user_id_crypted, $user_id, $page_ref) = @_;
    my $password              = doPasswordGen();
    my $security_adduser_date = getDateUpdate();
    my $login_from_session   = 0;
    my $user_id_from_session;
    my $mykeytag = getKeytagByCryptUid($user_id_crypted);
    eval {
        $user_id_from_session =
          doBlowfishDecrypt(
            getStringFromHexStr($user_id_crypted), $mykeytag );
    };
    my $logincrypted =
      getCryptLoginByDecryptUid($user_id_from_session);
    eval {
        $login_from_session =
          doBlowfishDecrypt(
            getStringFromHexStr($logincrypted), $mykeytag );
    };

    my @edituserdata       = getUserDetails($user_id);
    my $edituserkeytag     = $edituserdata[5];
    my $editusercryptlogin = $edituserdata[3];
    my $editusercryptfio   = $edituserdata[2];
    my $edituserlogin;
    my $edituserfio;
    eval {
        $edituserlogin =
          doBlowfishDecrypt(
            getStringFromHexStr($editusercryptlogin),
            $edituserkeytag );
    };
    eval {
        $edituserfio =
          doBlowfishDecrypt(
            getStringFromHexStr($editusercryptfio),
            $edituserkeytag );
    };

    # Выставление прав #

    my @_amods = getAccessMode( $sid );
    my @_dis1  = ('disabled', 'disabled');
    my @_dis2  = ('disabled', 'disabled');
    my @_dis3  = ('disabled', 'disabled');
    if ($_amods[0] == 1) {
        $_dis1[0] = '';
    } elsif ($_amods[0] == 2) {
        $_dis1[0] = '';
        $_dis1[1] = '';
    }

    if ($_amods[1] == 1) {
        $_dis2[0] = '';
    } elsif ($_amods[1] == 2) {
        $_dis2[0] = '';
        $_dis2[1] = '';
    }

    if ($_amods[2] == 1) {
        $_dis3[0] = '';
    } elsif ($_amods[2] == 2) {
        $_dis3[0] = '';
        $_dis3[1] = '';
    }

    my @access_arr = ( '', '', '', '', '', '', '', '' );

    if ( $edituserdata[6] == 1 ) { $access_arr[0] = 'CHECKED'; }
    if ( $edituserdata[6] == 2 ) { $access_arr[1] = 'CHECKED'; }
    if ( $edituserdata[6] == 3 ) { $access_arr[2] = 'CHECKED'; }

    if ( $edituserdata[7] == 1 ) { $access_arr[3] = 'CHECKED'; }
    if ( $edituserdata[7] == 2 ) { $access_arr[4] = 'CHECKED'; }
    if ( $edituserdata[7] == 3 ) { $access_arr[5] = 'CHECKED'; }

    if ( $edituserdata[8] == 1 ) { $access_arr[6] = 'CHECKED'; }
    if ( $edituserdata[8] == 2 ) { $access_arr[7] = 'CHECKED'; }
    if ( $edituserdata[8] == 3 ) { $access_arr[8] = 'CHECKED'; }

    # Выставление прав #

    my $access_mode = qq~
         <div class="stxt frm-div-marg modal-tmargin">Права доступа:</font></div>
         <div class="modal-table">
            <div class="mt-row-th">
                <div class="mt-ua-cell-lgnd">Действия в системе</div>
                <div class="mt-ua-cell-data">Просмотр данных</div>
                <div class="mt-ua-cell-data">Полный доступ</div>
                <div class="mt-ua-cell-data">Доступ запрещен</div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Конфигурирование</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf1" hidden="" name="config" $_dis1[0] $access_arr[0] value="1" type="radio"><label for="ua-conf1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf2" hidden="" name="config" $_dis1[1] $access_arr[1] value="2" type="radio"><label for="ua-conf2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-conf3" hidden="" name="config" $access_arr[2] value="3" type="radio"><label for="ua-conf3" class="hoverable"></label>
                  </span>
                </div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Управление безопасностью системы</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec1" hidden="" name="security" $_dis2[0] $access_arr[3] value="1" type="radio"><label for="ua-sec1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec2" hidden="" name="security" $_dis2[1] $access_arr[4] value="2" type="radio"><label for="ua-sec2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-sec3" hidden="" name="security" $access_arr[5] value="3" type="radio"><label for="ua-sec3" class="hoverable"></label>
                  </span>
                </div>
            </div>
            <div class="mt-row">
                <div class="mt-ua-cell-lgnd">Работа с установленными модулями</div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods1" hidden="" name="modules" $_dis3[0] $access_arr[6] value="1" type="radio"><label for="ua-mods1" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods2" hidden="" name="modules" $_dis3[1] $access_arr[7] value="2" type="radio"><label for="ua-mods2" class="hoverable"></label>
                  </span>
                </div>
                <div class="mt-ua-cell-data">
                  <span class="sysgal-radio-block single-radio">
                   <input id="ua-mods3" hidden="" name="modules" $access_arr[8] value="3" type="radio"><label for="ua-mods3" class="hoverable"></label>
                  </span>
                </div>
            </div>
         </div>
    ~;
    if (
        ( $edituserlogin ne '' && $login_from_session ne '' )
        && (   $edituserlogin eq $login_from_session
            || $edituserlogin eq 'rootadmin' )
      )
    {
        $access_mode = '';
    }
    my $security_adduser_form = qq~
    <div class="popup_header">Редактирование данных пользователя</div>
        <div class=standart_text>
           <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsecurity&amp;page=$page_ref" class=simple_link>
           <b>Безопасность</b></a>&nbsp;/&nbsp;
           Редактирование данных пользователя $edituserlogin<br><br>
        </div>
        <form  id="addweblog"
               method=post 
               action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsecuritymoduser&amp;userid=$user_id&amp;page=$page_ref"
               style="margin:0">
         <div class="stxt frm-div-marg">ФИО: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input placeholder="Иванов Иван Иванович" required type=text name=securityfio value="$edituserfio" class="input modal-input">
         <div class="stxt frm-div-marg">Логин: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input placeholder="ivan.ivanov" required type=text name=securitylogin value="$edituserlogin" class="input modal-input"></div>
         <div class="stxt frm-div-marg">Пароль: <font color="#A0A0A0">[обязательное поле]</font></div>
         <div class="frm-div-marg"><input type=text placeholder="оставьте пустым для сохранения старого пароля" name=securitypassw class="input modal-input"></div>
         $access_mode
         <div class="frm-div-marg modal-tmargin"><input type=submit value="Сохранить изменения" class="button modal-butt"></div>
        </form>
    ~;
    return $security_adduser_form;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# doPasswordGen ()
#---------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Генерация стойкого пароля.
# Функция возвращает сгенерированный пароль.
#
#---------------------------------------------------------------------------------

sub doPasswordGen {
    my @pharses = (
        'Kp1', 'Uiq', 'PzD', '1Ms', 'OU9', 'Nfs', 'IwB', '4X6', 'P1a', 'Jw3',
        '92D', 'Ie5', 'nhY', 'ksO', '7T6', 'mYh', 'OHJ', 'LKq', 'NB6', 'Ofd',
        'Iac', 'PEr', 'Kal', '9U1', '15F', 'KI4', 'nlO', 'Qw3', 'io9', '6Vf'
    );
    my @ost1 = ('0');
    my @ost2 = ('0');
    my @ost3 = ('0');
    my $flag = 1;
    for ( my $whileinx = 0 ; $whileinx <= 200 ; $whileinx++ ) {
        my $rnd1 = rand($#pharses);
        my $rnd2 = rand($#pharses);
        my $rnd3 = rand($#pharses);
        @ost1 = split( /\./, $rnd1 );
        @ost2 = split( /\./, $rnd2 );
        @ost3 = split( /\./, $rnd3 );
        if (   $ost1[0] != $ost2[0]
            && $ost1[0] != $ost3[0]
            && $ost2[0] != $ost3[0] )
        {
            last;
        }
    }
    return $pharses[ $ost1[0] ] . $pharses[ $ost2[0] ] . $pharses[ $ost3[0] ];
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# setUserDetails ( $sort, $id, $fio, $login, $passw, $tag, $conf, $secr, $mods )
#---------------------------------------------------------------------------------
#
# $sort - идентификатор сортировки.
# $id - идентификатор пользователя (в незашифрованном виде).
# $fio - ФИО пользователя.
# $login - логин пользователя.
# $passw - пароль пользователя.
# $tag - keytag.
# $conf - права доступа к конфигурированию.
# $secr - права доступа к управлению безопасностью.
# $mods - права досутпа к установленным модулям.
# Функция добавления данных нового пользователя в БД.
# Функция возвращает массив (<статус_операции>, <сообщение_пользователю>).
#
#---------------------------------------------------------------------------------

sub setUserDetails {
    my $retcode = 0;
    my $retstat;
    if ( -e $MODULE_DB_FILE ) {
        my @input_values = @_;
        my $keytag       = md5_hex( rand() );

        # поиск такого же логина
        my $duplicatedlogin = 0;
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @users_db = <$inputFH>;
        close($inputFH);

        for ( my $jinx = 0 ; $jinx <= $#users_db ; $jinx++ ) {
            my $record2 = $users_db[$jinx];
            $record2 =~ s/[\r\n]+//;
            $record2 =~ s/^[\W]+//;
            my @checkuserdata = split( /\|/, $record2 );
            my $tmpkeytag = $checkuserdata[5];
            my $login_from_db =
              doBlowfishDecrypt(
                getStringFromHexStr( $checkuserdata[3] ), $tmpkeytag );

            if ( $login_from_db eq $_[3] ) { $duplicatedlogin = 1; }
        }
        if (   $input_values[2] ne ''
            && $input_values[3] ne ''
            && $input_values[4] ne '' )
        {
            if ( $input_values[3] =~ /^[a-zA-Z0-9\-\_\.]+$/ ) {
                if ( $input_values[2] !~
                    /[\$\r\n\%\\0\&\>\<\t\*\^\@\{\}\[\]\)\(\+]+/ )
                {
                    if ( $duplicatedlogin == 0 ) {
                        $input_values[2] = getHexStrFromString(
                            doBlowfishEncrypt( $input_values[2], $keytag )
                        );
                        $input_values[3] = getHexStrFromString(
                            doBlowfishEncrypt( $input_values[3], $keytag )
                        );
                        $input_values[4] =
                          md5_hex( $keytag . $input_values[4] );
                        $input_values[5] = $keytag;
                        open( my $outputFH, "+<", $MODULE_DB_FILE );
                        my @indexes = <$outputFH>;
                        push( @indexes, join( "|", @input_values ) . "\n" );
                        seek( $outputFH, 0, 0 );
                        flock( $outputFH, 2 );
                        print $outputFH @indexes;
                        truncate( $outputFH, tell($outputFH) );
                        flock( $outputFH, 8 );
                        close($outputFH);
                        $retcode = 1;
                        $retstat = 'Пользователь добавлен в базу данных!';
                    }
                    else {
                        $retcode = 2;
                        $retstat = "Логин <b>$input_values[3]</b> занят";
                    }
                }
                else {
                    $retcode = 2;
                    $retstat = "Недопустимые символы в поле «ФИО»";
                }
            }
            else {
                $retcode = 2;
                $retstat = "Недопустимые символы в поле «Логин»";
            }
        }
        else {
            $retcode = 2;
            $retstat = "Пустые поля недопустимы - форма должна быть заполнена";
        }
    }
    return ( $retcode, '<p class=hintcont>'.$retstat.'</p>');
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getModfyPasswdForm ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод формы изменения пароля.
# Функция возвращает форму в HTML формате.
#
#---------------------------------------------------------------------------------

sub getModfyPasswdForm {
    my ($sid) = @_;
    my $modfy_passwd_form = qq~
     <div>
      <div class="stxt pheix-resetpassw-head error">
        Для повышения безопасности следует изменить пароль!</div>
      <form  method="post"
        action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=changepasswd"
        enctype="multipart/form-data">
       <div class="stxt frm-div-marg">Старый пароль:</div>
       <div class="frm-div-marg">
        <input  type="password"
            placeholder="Старый пароль"
            required
            class="input pheix-form-shrt-inpt"
            name="oldpasswd"></div>
       <div class="stxt pheix-frm-exit-marg">Новый пароль:</div>
       <div class="frm-div-marg">
        <input  type="password"
            placeholder="Новый пароль"
            required
            class="input pheix-form-long-inpt"
            name="newpasswd"></div>
       <div class="stxt frm-div-marg">Повторите новый пароль:</div>
       <div class="frm-div-marg">
        <input  type="password"
            placeholder="Повторите новый пароль"
            required
            class="input pheix-form-long-inpt"
            name="renewpasswd"></div>
       <div class="pheix-frm-exit-marg">
        <input  type="submit"
            value="Отправить"
            class="button pheix-form-shrt-inpt"></div>
      </form>
     </div>
    ~;
    return $modfy_passwd_form;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# showExitForm ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод формы подтверждения пароля при выходе.
# Функция возвращает форму в HTML формате.
#
#---------------------------------------------------------------------------------

sub showExitForm {
    my ($sid) = @_;
    my $security_exitform = qq~
     <div class="pheix-exit-wndw">
       <div class="pheix-exit-header error">
            Вы действительно хотите выйти из системы?</div>
       <div class="stxt">
        Ввод пароля позволит обновить md5-хэши,&nbsp;
        тем не менее вы можете выйти из системы без ввода пароля,&nbsp;
        при этом md5-хэши не обновятся!</div>
     <form  name="secureexit"
        method="post" action="$ENV{SCRIPT_NAME}?id=$sid&amp;action=secureexit"
        enctype="multipart/form-data"
        style="margin:0px;
        padding:0px;">
     <div class="stxt pheix-frm-exit-marg">Ваш текущий пароль:</div>
     <div class="pheix-frm-exit-marg">
       <input type="password"
            class="input"
            style="width:200px"
            name="password"
            value="">
      &nbsp;&nbsp;&nbsp;&nbsp;
      <a  href="#"
        onclick="javascript:document.forms['secureexit'].elements[0].value='';"
        class="simple_link">Очистить</a>
     </div>
     <div class="pheix-frm-exit-marg">
        <input  type="submit"
            value="Выйти"
            class="button"
            style="width:120px"></div>
     </form></div>
    ~;
    return $security_exitform;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# doMd5HashesUpdate ( $sesion_id, $passwd )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $passwd - пароль пользователя в незашифрованном виде.
# Функция выполняет обновление md5-суммы keytag и перешифрование данных с
# использованием обновленного keytag для пользователя, выполнившего выход.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub doMd5HashesUpdate {
    my $sessionid = $_[0];
    my $passwd    = $_[1];
    if ( -e "$MODULE_DB_FILE" ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @users_db = <$inputFH>;
        close($inputFH);
        my $cryptedid  = getCryptLoginBySid($sessionid);
        my $keytag     = getKeytagByCryptUid($cryptedid);
        my $hash_passw = md5_hex( $keytag . $passwd );
        my $decrypted_id_from_auth =
          doBlowfishDecrypt(
            getStringFromHexStr($cryptedid), $keytag );
        for (
            my $NewKeytagInx = 0 ;
            $NewKeytagInx <= $#users_db ;
            $NewKeytagInx++
          )
        {
            my @tmp = split( /\|/, $users_db[$NewKeytagInx] );
            if ( $tmp[1] eq $decrypted_id_from_auth && $hash_passw eq $tmp[4] )
            {
                my $fio =
                  doBlowfishDecrypt(
                    getStringFromHexStr( $tmp[2] ), $keytag );
                my $login =
                  doBlowfishDecrypt(
                    getStringFromHexStr( $tmp[3] ), $keytag );
                my $keytagnew = md5_hex( rand() );
                $tmp[2] =
                  getHexStrFromString(
                    doBlowfishEncrypt( $fio, $keytagnew ) );
                $tmp[3] =
                  getHexStrFromString(
                    doBlowfishEncrypt( $login, $keytagnew ) );
                $tmp[4] = md5_hex( $keytagnew . $passwd );
                $tmp[5] = $keytagnew;
                $users_db[$NewKeytagInx] = join( "|", @tmp );
                open( my $outputFH, ">", $MODULE_DB_FILE );
                flock( $outputFH, 2 );
                seek( $outputFH, 0, 2 );
                print $outputFH @users_db;
                flock( $outputFH, 8 );
                close($outputFH);
            }
        }
    }
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# moveRecordUp ( $_rid )
#---------------------------------------------------------------------------------
#
# $_rid - идентификатор сортировки.
# Функция выполняет передвижение пользователя вверх по списку в таблице.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub moveRecordUp {
    my ($_rid) = @_;
    if ( -e $MODULE_DB_FILE ) {
        open( my $fh, "+<", $MODULE_DB_FILE );
        my @_cols = sort { $b cmp $a } <$fh>;
        for my $i (0..$#_cols) {
            my @tmp = split( /\|/, $_cols[$i] );
            if ( $tmp[0] == $_rid ) {
                my @tmp2 = split( /\|/, $_cols[ $i - 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $_cols[ $i - 1 ] = join( "|", @tmp2 );
                $_cols[$i] = join( "|", @tmp );
                last;
            }
        }
        seek( $fh, 0, 0 );
        flock( $fh, 2 );
        print $fh @_cols;
        truncate( $fh, tell($fh) );
        flock( $fh, 8 );
        close($fh);
    }
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# moveRecordDown ( $_rid )
#---------------------------------------------------------------------------------
#
# $_rid - идентификатор сортировки.
# Функция выполняет передвижение пользователя вниз по списку в таблице.
# Функция не возвращает какое-либо значение.
#
#---------------------------------------------------------------------------------

sub moveRecordDown {
    my ($_rid) = @_;
    if ( -e $MODULE_DB_FILE ) {
        open( my $fh, "+<", $MODULE_DB_FILE );
        my @_cols = sort { $b cmp $a } <$fh>;
        for my $i (0..$#_cols) {
            my @tmp = split( /\|/, $_cols[$i] );
            if ( $tmp[0] == $_rid ) {
                my @tmp2 = split( /\|/, $_cols[ $i + 1 ] );
                my $buf = $tmp2[0];
                $tmp2[0] = $tmp[0];
                $tmp[0]  = $buf;
                $_cols[ $i + 1 ] = join( "|", @tmp2 );
                $_cols[$i] = join( "|", @tmp );
                last;

            }
        }
        seek( $fh, 0, 0 );
        flock( $fh, 2 );
        print $fh @_cols;
        truncate( $fh, tell($fh) );
        flock( $fh, 8 );
        close($fh);
    }
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# delUserDetails ( $id )
#---------------------------------------------------------------------------------
#
# $id - идентификатор пользователя (в незашифрованном виде).
# Функция удаления данных пользователя из БД.
# Функция возвращает массив (<статус_операции>, <сообщение_пользователю>).
#
#---------------------------------------------------------------------------------

sub delUserDetails {
    my $retcode = 0;
    my $retstat;
    if ( -e "$MODULE_DB_FILE" ) {
        open( my $outputFH, "+<", $MODULE_DB_FILE );
        my @indexes           = <$outputFH>;
        my $modification_flag = 0;
        for ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my $check = $indexes[$kinx];
            $check =~ s/[\n]+//g;
            if ( $check ne '' ) {
                my @temp_arr = split( /\|/, $indexes[$kinx] );
                if ( $_[0] eq $temp_arr[1] ) {
                    my $tmpkeytag = $temp_arr[5];
                    my $login_from_db =
                      doBlowfishDecrypt(
                        getStringFromHexStr( $temp_arr[3] ), $tmpkeytag );
                    if ( $login_from_db ne 'rootadmin' ) {
                        my @ostatok   = @indexes[ $kinx + 1 .. $#indexes ];
                        my @pre_array = @indexes[ 0 .. $kinx - 1 ];
                        @indexes = ( @pre_array, @ostatok );
                        $modification_flag = 1;
                    }
                    else {
                        $retcode = 2;
                        $retstat = "Невозможно удалить пользователя <b>rootadmin</b>";
                    }
                    last;
                }
            }
        }
        if ( $modification_flag == 1 ) {
            seek( $outputFH, 0, 0 );
            flock( $outputFH, 2 );
            print $outputFH @indexes;
            truncate( $outputFH, tell($outputFH) );
            flock( $outputFH, 8 );
            close($outputFH);
            $retcode = 1;
            $retstat = 'Пользователь успешно удален!';
        }
    }
    return ( $retcode, '<p class=hintcont>'.$retstat.'</p>' );
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getUserDetails ( $id )
#---------------------------------------------------------------------------------
#
# $id - идентификатор пользователя (в незашифрованном виде).
# Получение массива данных пользователя из БД.
# Функция возвращает массив данных пользователя.
#
#---------------------------------------------------------------------------------

sub getUserDetails {
    my $userid = $_[0];
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @indexes = <$inputFH>;
        close($inputFH);
        for ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my $record = $indexes[$kinx];
            $record =~ s/[\r\n]+//;
            $record =~ s/^[\W]+//;
            if ( $record ne '' ) {
                my @tmp_arr = split( /\|/, $record );
                if ( $tmp_arr[1] eq $userid ) { return @tmp_arr; }
            }
        }
    }
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# isAccessDenied ( $id )
#---------------------------------------------------------------------------------
#
# $id - идентификатор пользователя (в незашифрованном виде).
# Проверка статуса `доступ запрещен` для заданного пользователя.
# Функция возвращает 0 - доступ разрешен; 1 - доступ запрещен.
#
#---------------------------------------------------------------------------------

sub isAccessDenied {
    my $userid = $_[0];
    if ( !( defined($userid) ) ) { $userid = '' }
    my $accessdenied = 0;
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @indexes = <$inputFH>;
        close($inputFH);
        for ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my $record = $indexes[$kinx];
            $record =~ s/[\r\n]+//;
            $record =~ s/^[\W]+//;
            if ( $record ne '' ) {
                my @tmp_arr = split( /\|/, $record );
                if ( $tmp_arr[1] eq $userid ) {
                    if (   $tmp_arr[ $#tmp_arr - 2 ] == 3
                        && $tmp_arr[ $#tmp_arr - 1 ] == 3
                        && $tmp_arr[$#tmp_arr] == 3 )
                    {
                        $accessdenied = 1;
                    }
                    last;
                }
            }
        }
    }
    return $accessdenied;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# getAccessMode ( $sesion_id )
#---------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Получение прав доступа для пользователя из БД.
# Функция возвращает массив с значениями прав доступа для заданного пользователя.
#
#---------------------------------------------------------------------------------

sub getAccessMode {
    my $session_id           = $_[0];
    my $user_id_crypted      = getCryptLoginBySid($session_id);
    my $user_id_from_session = '';
    my $mykeytag = getKeytagByCryptUid($user_id_crypted);
    eval {
        $user_id_from_session =
          doBlowfishDecrypt(
           getStringFromHexStr($user_id_crypted), $mykeytag );
    };
    my @access_modes = ( 3, 3, 3 );
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @indexes = <$inputFH>;
        close($inputFH);
        for ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my $record = $indexes[$kinx];
            $record =~ s/[\r\n]+//;
            $record =~ s/^[\W]+//;
            if ( $record ne '' ) {
                my @tmp_arr = split( /\|/, $record );
                if ( $tmp_arr[1] eq $user_id_from_session ) {
                    @access_modes = (
                        $tmp_arr[ $#tmp_arr - 2 ],
                        $tmp_arr[ $#tmp_arr - 1 ],
                        $tmp_arr[$#tmp_arr]
                    );
                    last;
                }
            }
        }
    }
    return @access_modes;
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# modfyUserDetails ( $sesid, $id, $fio, $login, $pass, $tag, $conf, $sec, $mod )
#---------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии доступа к административной части.
# $id - идентификатор пользователя (в незашифрованном виде).
# $fio - ФИО пользователя.
# $login - логин пользователя.
# $pass - пароль пользователя.
# $tag - keytag.
# $con - права доступа к конфигурированию.
# $sec - права доступа к управлению безопасностью.
# $mod - права досутпа к установленным модулям.
# Модификация данных пользователя в БД.
# Функция возвращает массив (<статус_операции>, <сообщение_пользователю>).
#
#---------------------------------------------------------------------------------

sub modfyUserDetails {
    my $retcode              = 0;
    my $userid               = $_[1];
    my $retstat;
    my $user_id_from_session;
    my $user_id_crypted      = getCryptLoginBySid( $_[0] );
    my $mykeytag = getKeytagByCryptUid($user_id_crypted);
    eval {
        $user_id_from_session =
          doBlowfishDecrypt(
            getStringFromHexStr($user_id_crypted), $mykeytag );
    };
    if ( -e $MODULE_DB_FILE ) {
        open( my $inputFH, "<", $MODULE_DB_FILE );
        my @users_db = <$inputFH>;
        close($inputFH);
        for ( my $kinx = 0 ; $kinx <= $#users_db ; $kinx++ ) {
            my $record = $users_db[$kinx];
            $record =~ s/[\r\n]+//;
            $record =~ s/^[\W]+//;
            if ( $record ne '' ) {
                my @moduserdata = split( /\|/, $record );
                my $moduserkeytag = $moduserdata[5];
                my $my_prev_login =
                  doBlowfishDecrypt(
                    getStringFromHexStr( $moduserdata[3] ),
                    $moduserkeytag );
                if ( $moduserdata[1] eq $userid ) {
                    if (   $my_prev_login ne 'rootadmin'
                        || $my_prev_login eq $_[3] )
                    {
                        if ( $_[2] ne '' && $_[3] ne '' ) {

                            # поиск такого же логина
                            my $duplicatedlogin = 0;

                            for ( my $jinx = 0 ; $jinx <= $#users_db ; $jinx++ )
                            {
                                my $record2 = $users_db[$jinx];
                                $record2 =~ s/[\r\n]+//;
                                $record2 =~ s/^[\W]+//;
                                my @checkuserdata = split( /\|/, $record2 );
                                my $tmpkeytag = $checkuserdata[5];
                                my $login_from_db =
                                  doBlowfishDecrypt(
                                    getStringFromHexStr(
                                        $checkuserdata[3]
                                    ),
                                    $tmpkeytag
                                  );

                                if ( $login_from_db eq $_[3] ) {
                                    $duplicatedlogin = 1;
                                }
                            }

                            if ( $duplicatedlogin == 0 || $my_prev_login eq $_[3] ) {
                                if ( $_[3] =~ /^[a-zA-Z0-9\-\_\.]+$/ ) {
                                    if ( $_[2] !~ /[\$\r\n\%\\0\&\>\<\t\*\^\@\{\}\[\]\)\(\+]+/)
                                    {
                                        my $moduserkeytag = $moduserdata[5];
                                        if ( $_[4] ne '' ) {
                                            if ( $_[4] =~ /^[a-zA-Z0-9]+$/ ) {
                                                $moduserdata[4] = md5_hex(
                                                    $moduserkeytag . $_[4] );
                                            }
                                            else {
                                                $retcode = 2;
                                                $retstat = "Недопустимые символы в поле «Пароль»";
                                            }
                                        }
                                        if ( $retcode == 0 ) {
                                            if (   $_[5] != 0
                                                && $_[6] != 0
                                                && $_[7] != 0 )
                                            {

                                                if ( $user_id_from_session ne
                                                    $userid )
                                                {
                                                    my $login_from_db = '';
                                                    eval {
                                                        $login_from_db =
                                                          doBlowfishDecrypt(
                                                            getStringFromHexStr(
                                                                $moduserdata[3]
                                                            ),
                                                            $moduserkeytag
                                                          );
                                                    };
                                                    if ( $login_from_db ne
                                                        'rootadmin' )
                                                    {
                                                        $moduserdata[6] = $_[5];
                                                        $moduserdata[7] = $_[6];
                                                        $moduserdata[8] = $_[7];
                                                    }
                                                    else {
                                                        $retcode = 2;
                                                        $retstat = "Невозможно изменить права для пользователя <b>rootadmin</b>";
                                                    }
                                                }
                                                else {
                                                    $retcode = 2;
                                                    $retstat = "Изменять собственные права доступа запрещено";
                                                }
                                            }
                                            if ( $retcode == 0 ) {
                                                $moduserdata[2] =
                                                  getHexStrFromString(
                                                    doBlowfishEncrypt(
                                                        $_[2], $moduserkeytag
                                                    )
                                                  );
                                                $moduserdata[3] =
                                                  getHexStrFromString(
                                                    doBlowfishEncrypt(
                                                        $_[3], $moduserkeytag
                                                    )
                                                  );
                                                $users_db[$kinx] =
                                                  join( "|", @moduserdata )
                                                  . "\n";
                                                $retcode = 1;
                                                $retstat = 'Данные успешно изменены!';
                                            }
                                        }
                                    }
                                    else {
                                        $retcode = 2;
                                        $retstat = "Форма заполнена некорректно - недопустимые символы в поле «ФИО»";
                                    }
                                }
                                else {
                                    $retcode = 2;
                                    $retstat = "Форма заполнена некорректно - недопустимые символы в поле «Логин»";
                                }
                            }
                            else {
                                $retcode = 2;
                                $retstat = "Логин <b>$_[3]</b> уже используется в системе";
                            }
                        }
                        else {
                            $retcode = 2;
                            $retstat = "Форма заполнена некорректно - пустые поля недопустимы";
                        }
                    }
                    else {
                        $retcode = 2;
                        $retstat = "Невозможно изменить логин пользователя <b>rootadmin</b>";
                    }
                }
            }
        }
        if ( $retcode == 1 ) {
            open( my $outputFH, ">", $MODULE_DB_FILE );
            flock( $outputFH, 2 );
            seek( $outputFH, 0, 2 );
            print $outputFH @users_db;
            flock( $outputFH, 8 );
            close($outputFH);
        }
    }
    else {
        $retcode = 2;
        $retstat = "<b>$MODULE_DB_FILE</b> - файл не найден!";
    }
    return ( $retcode, '<p class=hintcont>'.$retstat.'</p>' );
}

#---------------------------------------------------------------------------------

#---------------------------------------------------------------------------------
# delBruteForceRecord ( $record_id )
#---------------------------------------------------------------------------------
#
# $record_id - идентификатор записи в журнале.
# Удаление записи из журнала брутфорса пароля для входа.
# Функция возвращает результат операции удаления (0: ошибка, 1: успех )
#
#---------------------------------------------------------------------------------

sub delBruteForceRecord {
    my ($record_id) = @_;
    my $retcode = 0;
    my $retstat;
    if ( -e $BRUTEFORCE_PATH ) {
        open( my $outputFH, "+<", $BRUTEFORCE_PATH );
        my @indexes           = <$outputFH>;
        my $modification_flag = 0;
        for ( my $kinx = 0 ; $kinx <= $#indexes ; $kinx++ ) {
            my $check = $indexes[$kinx];
            $check =~ s/[\n]+//g;
            if ( $check ne '' ) {
                my @temp_arr = split( /\|/, $indexes[$kinx] );
                if ( $record_id eq $temp_arr[0] ) {
                    my @ostatok   = @indexes[ $kinx + 1 .. $#indexes ];
                    my @pre_array = @indexes[ 0 .. $kinx - 1 ];
                    @indexes = ( @pre_array, @ostatok );
                    $modification_flag = 1;
                    $retstat = 'Попытка доступа к системе с адреса <b>'.
                                   $temp_arr[1] . '</b> удалена из протокола.';
                    last;
                }
            }
        }
        if ( $modification_flag == 1 ) {
            if ( $#indexes > -1 ) {
                seek( $outputFH, 0, 0 );
                flock( $outputFH, 2 );
                print $outputFH @indexes;
                truncate( $outputFH, tell($outputFH) );
                flock( $outputFH, 8 );
                close($outputFH);
                $retcode = 1;
            } else {
                close($outputFH);
                if (-e $BRUTEFORCE_PATH) {
                    $retcode = 2;
                    unlink($BRUTEFORCE_PATH);
                    $retstat .= '<br>Файл <b>'.$BRUTEFORCE_PATH.'</b> удален!';
                }
            }
        }
    }
    return ( $retcode, '<p class=hintcont>'.$retstat.'</p>')
}

#---------------------------------------------------------------------------------

END { }

1;
