#!/usr/bin/env perl

use strict;
use warnings;

########################################################################
#
# File   :  admin.pl
#
########################################################################
#
# Главный модуль управления административной частью CMS Pheix
#
########################################################################

BEGIN {

    #for all servers
    use lib 'admin/libs/extlibs/';
    use lib 'admin/libs/modules/';

}

# COMMON INSTALLED PACKAGES

# use Devel::NYTProf;

use POSIX;
use CGI::Carp qw(fatalsToBrowser);
use CGI;
use Time::Local;

my $co = new CGI;

# DO NOT EDIT REQUIRE SECTION

use Secure::UserAccess; # (%INSTALLED_MODULE%,r--,Secure,int,%INSTALLED_SECURITYMODULE_PROCEDURES%,0) #
use Config::ModuleMngr;  # (%INSTALLED_MODULE%,r--,Config,int,%INSTALLED_CONFIGMODULE_PROCEDURES%,0) #
use Stats::BigBro; # (%INSTALLED_MODULE%,r--,Stats,int,%INSTALLED_STATS_PROCEDURES%,0) #


# EMBEDDED INTERNAL PACKAGES

use Pheix::Session;
use Pheix::Tools;

#-------------------------------------------------------------------------------

my $SHOW_MESS   = 1;
my $DO_QUIET    = 0;
my $CLIENT_PWD_PATH = 'admin/system/client_pwd.tnk';

#-------------------------------------------------------------------------------

my $proto = $ENV{HTTP_REFERER} =~ /^https:\/\// ? 'https://' : 'http://';

#-------------------------------------------------------------------------------

if (defined($ENV{QUERY_STRING})) {
    if ($ENV{QUERY_STRING} eq 'auth') {
        my $referer = $ENV{HTTP_REFERER};
        $referer =~ s/https?\:\/\///g;
        my @referer_arr = split( "\/", $referer );

        if ($referer_arr[0] eq $ENV{HTTP_HOST}) {
            my $login    = $co->param('login');
            my $password = $co->param('password');
            my $userid   = 0;

            if (-e $CLIENT_PWD_PATH) {
                open(my $FHANDLEAUTH, "<", $CLIENT_PWD_PATH);
                my @authdb = <$FHANDLEAUTH>;
                close($FHANDLEAUTH);

                foreach my $auth (@authdb) {
                    $userid = Pheix::Session::getDecryptUid($auth, $login, $password);

                    if ($userid > 0) {
                        print $co->header( -type => 'text/html; charset=UTF-8' );

                        Pheix::Session::delSessionByTimeOut();
                        Pheix::Session::createUserSession($userid);

                        my $sesid = Pheix::Session::getSessionId($userid);

                        Pheix::Tools::showHomePage($sesid, Pheix::Session::getCryptLoginBySid($sesid), $DO_QUIET);

                        exit;
                    }
                }

                my $bruteforce = Pheix::Session::isBruteforce( $ENV{REMOTE_ADDR}, 'logging' );

                print $co->header( -type => 'text/html; charset=UTF-8' );

                if ($bruteforce == 0) {
                    Pheix::Tools::showLoginPage('auth');
                }
                else {
                    Pheix::Tools::showLoginPage( 'timeout', $bruteforce );
                }

                exit;
            }
            else {
                my $timestamp = Pheix::Session::defaultCredentials($login, $password);

                if ($timestamp) {
                    Pheix::Session::delSessionByTimeOut();

                    my $userid = Pheix::Session::addDefaultAccount($timestamp, $login, $password);

                    if ($userid) {
                        Pheix::Session::createUserSession($userid);

                        my $sesid = Pheix::Session::getSessionId($userid);

                        print $co->header( -type => 'text/html; charset=UTF-8' );

                        Pheix::Tools::showHomePage($sesid, $login, $DO_QUIET);

                        exit;
                    }
                }
            }
        } else {
            print $co->redirect(-uri => sprintf("%s%s", $proto, $ENV{SERVER_NAME}));

            exit;
        }
    }
}

my @env_params = Pheix::Tools::getParamsArray( $ENV{QUERY_STRING} );

# ACCESS MODE FOR USER

my @access_modes = ( 3, 3, 3 );
my $check_auth = Pheix::Session::isValidUser( $env_params[0] );
if ( $check_auth == 1 ) {
    @access_modes = Secure::UserAccess::getAccessMode( $env_params[0] );
}

# ACCESS MODE FOR USER

#-------------------------------------------------------------------------------

# DO NOT EDIT
# (%INSTALLED_STATS_PROCEDURES%) #

my $bbro = Stats::BigBro->new;

if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admstats' ) ) {
    Pheix::Session::delSessionByTimeOut();
    if ( $check_auth == 1 ) {
        print $co->header( -type => 'text/html; charset=UTF-8' );
        $bbro->showBBHomePage($env_params[0]);
        exit;
    }
    print $co->header( -type => 'text/html; charset=UTF-8' );
    Pheix::Tools::showLoginPage();
    exit;
}

if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admstatssrch' ) ) {
    Pheix::Session::delSessionByTimeOut();
    if ( $check_auth == 1 ) {
        print $co->header( -type => 'text/html; charset=UTF-8' );
        my $query = $co->param("query");
        if ( $query eq q{*} ) {
            print $bbro->getRawLogsTable($env_params[0])
        } else {
            print $bbro->getRawLogsTable($env_params[0], $query)
        }
        exit;
    }
    print $co->header( -type => 'text/html; charset=UTF-8' );
    Pheix::Tools::showLoginPage();
    exit;
}

# (%INSTALLED_STATS_PROCEDURES%) #

# (%INSTALLED_SECURITYMODULE_PROCEDURES%) #
if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'checksession' ) ) {
    my $crypted_userid = Pheix::Session::getCryptLoginBySid( $env_params[0] );
    my $check_passw_hash =
        Pheix::Session::isDefaultPasswd( $crypted_userid, $env_params[0] );
    if ($check_passw_hash == 1) {
        print $co->header(
            -type => 'text/html; charset=UTF-8',
            -status=> '200 Password Renewal',
        );
    }
    else {
        if ( $access_modes[1] == 3 ) {
            print $co->header(
                -type => 'text/html; charset=UTF-8',
                -status=> '200 Access Denied',
            );
        }
        else {
            print $co->header(
                -type => 'text/html; charset=UTF-8',
                -status=> '200 OK',
            );
        }

    }
    print $check_auth;
    exit;
}

if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'secureexitform' ) ) {
    Pheix::Session::delSessionByTimeOut();
    if ( $check_auth == 1 ) {
        print $co->header( -type => 'text/html; charset=UTF-8' );
        print Secure::UserAccess::showExitForm(
            $env_params[0],
            Pheix::Session::getCryptLoginBySid( $env_params[0] )
            );
        exit;
    }
    print $co->header( -type => 'text/html; charset=UTF-8' );
    Pheix::Tools::showLoginPage();
    exit;
}

if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'secureexit' ) ) {
    my $outer_ses_id = $env_params[0];
    Pheix::Session::delSessionByTimeOut();
    if ( $check_auth == 1 ) {
        my $password = $co->param('password');
        if ( $password ne '' ) {
            Secure::UserAccess::doMd5HashesUpdate( $env_params[0], $password );
        }
        Pheix::Session::doUserExit($outer_ses_id);
    }
    print $co->redirect( -uri => $proto.$ENV{SERVER_NAME}.'/admin.pl' );
    exit;
}

if ( $access_modes[1] == 1 || $access_modes[1] == 2 )    # Access modes: r+rw
{

     # action from read-write block - allow to change default password

     if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'changepasswd' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            my $changepasswreport = '';
            my $oldpassw          = $co->param('oldpasswd');
            my $newpassw          = $co->param('newpasswd');
            my $renewpassw        = $co->param('renewpasswd');
            if ( $oldpassw ne '' && $newpassw ne '' && $renewpassw ne '' ) {
                $changepasswreport = Config::ModuleMngr::setPassword( $env_params[0], $oldpassw, $newpassw, $renewpassw );
            }
            Pheix::Tools::showHomePage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $SHOW_MESS,
                $changepasswreport
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # Force password check for login with default password

    {
        if ( defined( $env_params[0] )) {
            my $session_id       = $env_params[0];
            my $crypted_userid   = Pheix::Session::getCryptLoginBySid( $session_id );
            my $check_passw_hash =
                Pheix::Session::isDefaultPasswd( $crypted_userid, $session_id );
            if ( $check_passw_hash == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                Pheix::Tools::showHomePage( $session_id, $crypted_userid, $DO_QUIET );
                exit;
            }
        }
    }

    # read-only actions

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecurity' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Secure::UserAccess::showUAHomePage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page,
                $DO_QUIET
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admbruteforcelog' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Secure::UserAccess::showBruteForceLog(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $DO_QUIET
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'addnewuserform' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[2];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Secure::UserAccess::getUserAddForm(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $got_page
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admeditsecurityuser' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Secure::UserAccess::getUserEditForm(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2],
                $got_page
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # read-only actions

    if ( $access_modes[1] == 2 )    # Access modes: rw
    {

        # read-write actions

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'securityadduser' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my @updatestatus     = ( 0, '' );
                my $got_page         = 0;
                my $flag             = 0;
                my $uplmess          = '';
                my $adduser_sort     = time;
                my $adduser_id       = $adduser_sort;
                my $adduser_fio      = $co->param('securityfio');
                my $adduser_login    = $co->param('securitylogin');
                my $adduser_passw    = $co->param('securitypassw');
                my $adduser_config   = $co->param('config');
                my $adduser_security = $co->param('security');
                my $adduser_modules  = $co->param('modules');
                my $keytag_seed      = 0;

                if ( $adduser_config !~ /^[123]$/ || $access_modes[0] == 3)   {
                    $adduser_config   = 3;
                } else {
                    if ($access_modes[0] == 1 && $adduser_config == 2) {
                        $adduser_config = 1;
                    }
                }
                if ( $adduser_security !~ /^[123]$/ || $access_modes[1] == 3) {
                    $adduser_security = 3;
                } else {
                    if ($access_modes[1] == 1 && $adduser_security == 2) {
                        $adduser_security = 1;
                    }
                }
                if ( $adduser_modules !~ /^[123]$/ || $access_modes[2] == 3)  {
                    $adduser_modules  = 3;
                } else {
                    if ($access_modes[2] == 1 && $adduser_modules == 2) {
                        $adduser_modules = 1;
                    }
                }

                @updatestatus = Secure::UserAccess::setUserDetails(
                    $adduser_sort,   $adduser_id,       $adduser_fio,
                    $adduser_login,  $adduser_passw,    $keytag_seed,
                    $adduser_config, $adduser_security, $adduser_modules
                );

                print $co->header( -type => 'text/html; charset=UTF-8' );
                Secure::UserAccess::showUAHomePage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page,
                    $SHOW_MESS,
                    $updatestatus[0],
                    $updatestatus[1]
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecuritydeluser' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my @updatestatus = ( 0, '' );
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                @updatestatus = Secure::UserAccess::delUserDetails( $env_params[2] );
                Secure::UserAccess::showUAHomePage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page,
                    $SHOW_MESS,
                    $updatestatus[0],
                    $updatestatus[1]
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecuritymoduser' ) ) {
            my @updatestatus = ( 0, '' );
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                my $session_id = $env_params[0];
                my $got_page   = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                print $co->header( -type => 'text/html; charset=UTF-8' );
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    my $adduser_fio      = $co->param('securityfio');
                    my $adduser_login    = $co->param('securitylogin');
                    my $adduser_passw    = $co->param('securitypassw');
                    my $adduser_config   = $co->param('config');
                    my $adduser_security = $co->param('security');
                    my $adduser_modules  = $co->param('modules');
                    if ( $adduser_config !~ /^[123]$/)   {
                        $adduser_config   = 0;
                    } else {
                        if ($access_modes[0] == 1 && $adduser_config == 2) {
                            $adduser_config = 1;
                        }
                    }
                    if ( $adduser_security !~ /^[123]$/) {
                        $adduser_security = 0;
                    } else {
                        if ($access_modes[1] == 1 && $adduser_security == 2) {
                            $adduser_security = 1;
                        }
                    }
                    if ( $adduser_modules !~ /^[123]$/)  {
                        $adduser_modules  = 0;
                    } else {
                        if ($access_modes[2] == 1 && $adduser_modules == 2) {
                            $adduser_modules = 1;
                        }
                    }
                    @updatestatus = Secure::UserAccess::modfyUserDetails(
                        $session_id,    $env_params[2],
                        $adduser_fio,      $adduser_login,
                        $adduser_passw,    $adduser_config,
                        $adduser_security, $adduser_modules
                    );

                }

                my $crypted_userid   = Pheix::Session::getCryptLoginBySid( $session_id );
                my $check_passw_hash =
                    Pheix::Session::isDefaultPasswd(
                        $crypted_userid,
                        $session_id
                    );
                if ( $check_passw_hash == 1 ) {
                    Pheix::Tools::showHomePage( $session_id, $crypted_userid, $DO_QUIET );
                } else {
                    Secure::UserAccess::showUAHomePage(
                        $session_id,
                        $crypted_userid,
                        $got_page,
                        $SHOW_MESS,
                        $updatestatus[0],
                        $updatestatus[1]
                        );
                }
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecuritydelbf' ) )
        {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @updatestatus = Secure::UserAccess::delBruteForceRecord(
                    $env_params[2] );
                if ($updatestatus[0] == 1) {
                    Secure::UserAccess::showBruteForceLog(
                        $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        $SHOW_MESS,
                        $updatestatus[0],
                        $updatestatus[1]
                        );
                } else {
                    if ($updatestatus[0] > 1) { $updatestatus[0] = 1; }
                    Secure::UserAccess::showUAHomePage(
                        $env_params[0],
                        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                        0,
                        $SHOW_MESS,
                        $updatestatus[0],
                        $updatestatus[1]
                    );
                }
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecuritymovedown' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Secure::UserAccess::moveRecordDown( $env_params[2] );
                }
                Secure::UserAccess::showUAHomePage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page,
                    $DO_QUIET
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsecuritymoveup' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $got_page = $env_params[3];
                if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
                if ( $env_params[2] =~ /^[\d]+$/ ) {
                    Secure::UserAccess::moveRecordUp( $env_params[2] );
                }
                Secure::UserAccess::showUAHomePage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $got_page,
                    $DO_QUIET
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        # read-write actions

    }   # Access modes: rw
}       # Access modes: r+rw

# (%INSTALLED_SECURITYMODULE_PROCEDURES%) #

# (%INSTALLED_CONFIGMODULE_PROCEDURES%) #
if ( $access_modes[0] == 1 || $access_modes[0] == 2 )    # Access modes: r+rw
{

    # read-only actions

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admmain' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showHomePage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $DO_QUIET
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsetup' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            my $got_page = $env_params[3];
            if ( $got_page !~ /^[\d]+$/ ) { $got_page = 0; }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Config::ModuleMngr::showSetupPage(
                $env_params[0],
                Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                $env_params[2],
                $DO_QUIET
                );
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admgeneratesm' ) ) {
        Pheix::Session::delSessionByTimeOut();
        if ( $check_auth == 1 ) {
            print $co->header( -type => 'text/html; charset=UTF-8' );
            print Config::ModuleMngr::showSmSummary($env_params[0]);
            exit;
        }
        print $co->header( -type => 'text/html; charset=UTF-8' );
        Pheix::Tools::showLoginPage();
        exit;
    }

    # read-only actions

    if ( $access_modes[0] == 2 )    # Access modes: rw
    {

        # read-write actions

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'installmodule' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @upload_result = Config::ModuleMngr::doModUpload( $co->param('uploadfile') );
                my @installres = Config::ModuleMngr::doModInstall( @upload_result, getCryptLoginBySid( $env_params[0] ) );
                Config::ModuleMngr::showUploadRes(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $installres[0],
                    $installres[1],
                    $DO_QUIET
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'uninstallmodule' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my @uninstallres = Config::ModuleMngr::doModUninstall( $env_params[2] );
                Config::ModuleMngr::showUninstallRes(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $uninstallres[0],
                    $uninstallres[1],
                    $DO_QUIET
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'powermodule' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $modulename = $env_params[2];
                my $powerreport = Config::ModuleMngr::setModPowerOn($modulename);
                Pheix::Tools::showHomePage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $SHOW_MESS,
                    $powerreport
                    );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsetupsave' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                my $modulename = $env_params[2];
                my $area = $env_params[3];
                my @rc_vars = Config::ModuleMngr::saveModSettings($env_params[0], $modulename, $area, $co);
                Config::ModuleMngr::showSetupPage(
                    $env_params[0],
                    Pheix::Session::getCryptLoginBySid( $env_params[0] ),
                    $modulename,
                    $SHOW_MESS,
                    $rc_vars[0],
                    $rc_vars[1]
                );
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsmdetails' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                Config::ModuleMngr::saveStaticSitemap();
                print Config::ModuleMngr::getSitemapDetails($env_params[0]);
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }

        if ( ( defined( $env_params[1] ) ) && ( $env_params[1] eq 'admsitemapdel' ) ) {
            Pheix::Session::delSessionByTimeOut();
            if ( $check_auth == 1 ) {
                print $co->header( -type => 'text/html; charset=UTF-8' );
                Config::ModuleMngr::delStaticSitemap();
                print Config::ModuleMngr::getSitemapDetails($env_params[0]);
                exit;
            }
            print $co->header( -type => 'text/html; charset=UTF-8' );
            Pheix::Tools::showLoginPage();
            exit;
        }
        # read-write actions

    }   # Access modes: rw
}       # Access modes: r+rw
# (%INSTALLED_CONFIGMODULE_PROCEDURES%) #
# DO NOT EDIT

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Встроенные программные действия дополнительных модулей CMS Pheix
#-------------------------------------------------------------------------------
#
# В процессе установки новых модулей в эту секцию добавляются новые программные
# действия. После деинсталляции модуля программные действия удаляются.
# Изменять содержимое секции вручную запрещено!
#
#-------------------------------------------------------------------------------

if ( $access_modes[2] == 1 || $access_modes[2] == 2 )    # Access modes: r+rw
{

# DO NOT EDIT PROCEDURE SECTION

}

#----------------------------------  DEBUG MODULES PASTE -----------------------

#----------------------------------  DEBUG MODULES PASTE -----------------------

print $co->header(
    -type => 'text/html; charset=UTF-8',
    -status=> '200 Access Denied',
);
if ( $check_auth == 1 ) {
    Secure::UserAccess::showAccessDenied(
        $env_params[0],
        Pheix::Session::getCryptLoginBySid( $env_params[0] ),
        $DO_QUIET
        );
} else {
    Pheix::Tools::showLoginPage();
}
