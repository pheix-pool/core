#!/bin/bash

PHEIX_VERSION=`cat ../www/admin/libs/modules/Pheix/Tools.pm | grep "Readonly my \$PHEIX_VERSION" | sed "s/.*'\(.\{1,\}\)';/\1/"`

echo "Build mediapack for Pheix v ${PHEIX_VERSION}..."

MEDIAPACK=./MEDIA_PACK

rm -rf ${MEDIAPACK}

mkdir -p ${MEDIAPACK}/css/fonts

cp -rf ../www/css/fonts ${MEDIAPACK}/css
if [ $? -ne 0 ]
then
	echo "Error while copying ../www/css/fonts, exiting"
	rm -rf ${MEDIAPACK}
	exit -1;
fi

cp -rf ../www/ckeditor ${MEDIAPACK}
if [ $? -ne 0 ]
then
	echo "Error while copying ../www/ckeditor, exiting"
	rm -rf ${MEDIAPACK}
	exit -1;
fi

cp -rf ../www/codemirror ${MEDIAPACK}
if [ $? -ne 0 ]
then
	echo "Error while copying ../www/codemirror, exiting"
	rm -rf ${MEDIAPACK}
	exit -1;
fi

cp -rf ../www/images ${MEDIAPACK}
if [ $? -ne 0 ]
then
	echo "Error while copying ../www/images, exiting"
	rm -rf ${MEDIAPACK}
	exit -1;
fi

rm -rf ${MEDIAPACK}/images/levels

rm -f ${MEDIAPACK}/images/systemgallery/upload/*

rm -f ${MEDIAPACK}/images/postdata/upload/*

rm -rf ${MEDIAPACK}/images/lightnews

rm -rf ${MEDIAPACK}/images/shopcat/*

cd ${MEDIAPACK}

tar -czf ../mediapack.pheix.${PHEIX_VERSION}.tgz --mode='g-w' .
if [ $? -ne 0 ]
then
	echo "Error while gzipping ../mediapack.pheix.${PHEIX_VERSION}.tgz, exiting"
	rm -rf ${MEDIAPACK}
	exit -1;
fi

cd ..

rm -rf ../www/.mediapack/*

mv ./mediapack.pheix.${PHEIX_VERSION}.tgz ../www/.mediapack

rm -rf ${MEDIAPACK}

echo "Done!"

