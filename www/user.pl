#!/usr/bin/env perl

use strict;
use warnings;

########################################################################
#
# File   :  user.pl
#
########################################################################
#
# Главный модуль управления пользовательской частью CMS Pheix
#
########################################################################

BEGIN {

    #for all servers
    use lib 'admin/libs/extlibs/';
    use lib 'libs/modules';

}

# COMMON INSTALLED PACKAGES

use CGI::Carp qw(fatalsToBrowser);
use CGI;
use POSIX;

# DO NOT EDIT REQUIRE SECTION

use Stats::BigBro; # (%INSTALLED_MODULE%,r--,Stats,int,%INSTALLED_STATS_PROCEDURES%,0) #

# EMBEDDED INTERNAL PACKAGES

use Pheix::Pages;
use Pheix::Tools;
use Stats::BigBro;

# EMBEDDED EXTERNAL PACKAGES

use Geo::IP::XPurePerl;

my $co    = new CGI;
my $proto = $ENV{HTTP_REFERER} =~ /^https:\/\// ? 'https://' : 'http://';

my @env_params = ();
if ( ( defined( $ENV{QUERY_STRING} ) ) ) {
    @env_params = Pheix::Tools::getParamsArray( $ENV{QUERY_STRING} );
}

my $global_time_stamp    = time - 86400;
my $global_last_modified = localtime($global_time_stamp);

if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq '404error' ) ) {
    print $co->header(
        -status => 404,
        -type   => 'text/html; charset=UTF-8'
    );
    Pheix::Pages::showHomePage("404");
    exit;
}

if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'redirectto' ) ) {
    print $co->redirect( -uri => "$env_params[1]" );
    exit;
}

if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'pheix-sitemap-xml' ) ) {
    my $_dynasm = getSingleSetting('Config', 0, 'dynasitemap');
    if ($_dynasm == 1) {
        print $co->header( -type   => 'text/xml; charset=UTF-8' );
        print Pheix::Pages::showSitemap();
    } else {
        my $_statism = Pheix::Tools::getStaticSiteMapFname();
        if (-e $_statism) {
            my $_mdtime = (stat($_statism))[9];
            my $_fstats = strftime '%a, %d %h %G %T GMT', localtime($_mdtime);
            print $co->header(
                -type   => 'text/xml; charset=UTF-8',
                -Last_Modified   => $_fstats,
                '-Expires'       => $_fstats,
                '-Cache-Control' => 'max-age=0, must-revalidate'
            );
            open my $fh, '<', $_statism;
            print join '', <$fh>;
            close $fh;
        } else {
            print $co->header(
                -status => 404,
                -type   => 'text/html; charset=UTF-8'
            );
            Pheix::Pages::showHomePage("404");
        }
    }
    exit;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Встроенные программные действия дополнительных модулей CMS Pheix
#-------------------------------------------------------------------------------
#
# В процессе установки новых модулей в эту секцию добавляются новые программные
# действия. После деинсталляции модуля программные действия удаляются.
# Изменять содержимое секции вручную запрещено!
#
#-------------------------------------------------------------------------------

# DO NOT EDIT PROCEDURE SECTION

# (%INSTALLED_PHEIX_PROCEDURES%) #
if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'index' ) ) {
    print $co->header(
        -type            => 'text/html; charset=UTF-8',
        -Last_Modified   => $global_last_modified,
        '-Expires'       => $global_last_modified,
        '-Cache-Control' => 'max-age=0, must-revalidate'
    );
    Pheix::Pages::showHomePage("index");
    exit;
}

if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'showcaptcha' ) ) {
    use Image::Magick;
    my $random;
    my $pointsize = 25;
    my $font      = 'css/fonts/codefont.ttf';
    my $decrypted_captcha_value;
    my $captchavalue = $env_params[1];

    eval {
        $decrypted_captcha_value =
          Pheix::Tools::doBlowfishDecrypt(
            Pheix::Tools::getStringFromHexStr($captchavalue) );
    };
    if ( $decrypted_captcha_value !~ /^[\d]+$/ ) {
        $random = rand();
    } else {
        $random = "0.$decrypted_captcha_value";
    }

    $random = sprintf( "%.6f", $random );
    my @tmp = split /\./, $random;

    my $image = new Image::Magick;
    $image->Set( size => '88x31' );
    $image->ReadImage('xc:white');
    $image->Set(
        type      => 'TrueColor',
        antialias => 'True',
        fill      => 'silver',
        font      => $font,
        pointsize => $pointsize,
    );
    $image->Draw(
        rotate    => -15,
        primitive => 'text',
        points    => '2,36',
        text      => $tmp[1],
    );
    $image->Swirl( degrees => int( rand(14) ) + 20, );
    print $co->header( -type => 'image/png' );
    binmode STDOUT;
    $image->Write('png:-');
    exit;
}

if ( ( defined( $env_params[0] ) ) && ( $env_params[0] eq 'bigbrother' ) ) {
    print $co->header(
        -type            => 'text/html; charset=UTF-8',
        -Last_Modified   => $global_last_modified,
        '-Expires'       => $global_last_modified,
        '-Cache-Control' => 'max-age=0, must-revalidate'
    );
    my $logObj = Stats::BigBro->new;
    my $rc = $logObj->doPheixLog($co);
    exit;
}
# (%INSTALLED_PHEIX_PROCEDURES%) #

print $co->header(
    -status          => 404,
    -type            => 'text/html; charset=UTF-8',
);
Pheix::Pages::showHomePage("404");
