#!/bin/bash

MODE=$1
PREFIX=..
ADMIN_MODS_PATH=$PREFIX/www/admin/libs/modules
USER_MODS_PATH=$PREFIX/www/libs/modules
DOPFILEZ="$PREFIX/www/.htaccess $PREFIX/www/robots.txt $PREFIX/www/favicon.ico $PREFIX/www/sitemap_static.xml $PREFIX/www/conf/config/index.html $PREFIX/www/conf/config/template.html"
DATE=`date '+%Y-%m-%d_%H-%M-%S'`
TAR=$PREFIX/config_${DATE}.tar

if [ ! \( "$MODE" = "pheix" -o "$MODE" = "user" \) ]
then
	echo -e "usage: ./save-configuration.sh MODE\nMODE defines saving policy:\n\tpheix - save configuration for pheix destribution (light)\n\tuser  - save configuration for user deployment (full)"
	exit
else
	if [ "$MODE" = "pheix" ]
	then
		DOPFILEZ=
	fi
fi

if [ -f $TAR ]; then
	rm -f $TAR
fi

tar --create --file=$TAR --files-from /dev/null

for path in $ADMIN_MODS_PATH $USER_MODS_PATH
do
	find $path -type f -name "*.xml" -printf '%h/%f\n' | while read file; do
		tar --append --file=$TAR $file
	done
done

if [ -f $TAR ]; then
#	echo $DOPFILEZ;
	for file in $DOPFILEZ
	do
		if [ -f $file ]; then
			tar --append --file=$TAR $file
		fi
	done
	gzip $TAR
	rm -f $TAR
fi
