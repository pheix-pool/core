var Spin = '<div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>';

function doSesChckLoad ( session_id, script, query ) {
	var secactions = ["changepasswd", "admsecurity", "admbruteforcelog", "addnewuserform", "admeditsecurityuser", "securityadduser", "admsecuritydeluser", "admsecuritymoduser", "admsecuritydelbf", "admsecuritymovedown", "admsecuritymoveup"];
	var issec = false;
	var loadUrl = new String;
	var chckUrl = new String;
	loadUrl = script+"?id="+session_id+query;
	chckUrl = script+"?id="+session_id+"&action=checksession";
	for (var i = 0; i < secactions.length; i++) {
		var re = new RegExp(secactions[i], 'i');
		if ( query.match( re ) ) {
			issec = true;
			console.log("security action detected: " + secactions[i]);
		}
	}
	jQuery.get(chckUrl, function( responseTxt, statusTxt, xhr ) {
	if(statusTxt == "error") {
		alert("Ajax request error: " + xhr.status + ": " + xhr.statusText);
		top.location.href=script;
	} else {
		//console.log(xhr.status + ": " + xhr.statusText);
		if (responseTxt.localeCompare("1") == 0) {
			if ( !issec || xhr.statusText.match(/OK/i) ) {
				jQuery.fancybox({
					href: loadUrl,
					type: 'ajax',
					closeBtn: false,
					autoCenter: true,
				});
			} else {
				console.log( "skip fancybox dialog for: " + xhr.status + "(" + xhr.statusText + ")" );
				jQuery.jGrowl.defaults.closer = false;
				jQuery('#securitytip').jGrowl("<div class=hintcont><p>Недостаточно прав для доступа к странице!</p></div>",
					{
						theme: 'updatehint',
						speed: 'slow',
						life: 3500,
						animateOpen: {
							cheight: "show",
						},
						animateClose: {
							height: "hide",
						}
					}
				);
			}
		} else {
			top.location.href=script;
		}
	}
	}, 'text')
	.fail(function( jqXHR, textStatus, errorThrown ) {
		console.log( jqXHR.status + ' ' + errorThrown );
	});
}

function GalPathCkeditorDialogAdd ( galid, images, currnum, path ) {
	 var formname = "slickgalleryform";
	 var elmtname = "twitterimage";
	 /*if (top.document.forms[formname]) {
		 if (top.document.forms[formname].elements[elmtname]) {
			 top.document.forms[formname].elements[elmtname].value = path;
		 }
	 }*/
	 if (jQuery('.cke_dialog')) {
		 var input = jQuery('.cke_dialog').find('input:text');
		 input.val(path);
	 }
	 if (galid && images > 0 && currnum > -1) {
		 var i;
		 for (i = 0; i <= images; i++) {
			 jQuery("#"+galid+"_"+i).css('border', "solid 2px #D5D5D5");
		 }
		 jQuery("#"+galid+"_"+currnum).css('border', "solid 2px red");
	 }
}

function GalPathAdd ( galid, images, currnum, path ) {
	 var formname = "slickgalleryform";
	 var elmtname = "twitterimage";
	 if (top.document.forms[formname]) {
		 if (top.document.forms[formname].elements[elmtname]) {
			 top.document.forms[formname].elements[elmtname].value = path;
		 }
	 }
	 if (galid && images > 0 && currnum > -1) {
		 var i;
		 for (i = 0; i <= images; i++) {
			 jQuery("#"+galid+"_"+i).css('border', "solid 2px #D5D5D5");
		 }
		 jQuery("#"+galid+"_"+currnum).css('border', "solid 2px red");
	 }
 }

function doInlineDataLoad( into, query ) {
	jQuery( "#"+into ).html( Spin );
	var modquery = query.replace(new RegExp(" ","g"),"%20");
	jQuery("#"+into).load(modquery+"&amp;rand="+Math.random(), function(responseTxt, statusTxt, xhr){
	if(statusTxt == "error")
		 alert("Error: " + xhr.status + ": " + xhr.statusText);
	});
}

function _Confirm(url, text) {
	flag = 0;
	text2 = new String;
	text2 = "Удалить???";
	if (text) {
		text2 = text;
	}
	if (confirm(text2)) {
		flag = 1;
	}
	if (flag == 1) {
		top.location.href = url;
	}
}

function Confirm(url, text) {
	flag = 0;
	text2 = new String;
	text2 = "Удалить???";
	if (text) {
		  text2 = text;
	}
	$.confirm({
	    title: 'Подтверждение',
	    content: text2,
			width :'auto',
			useBootstrap: false,
	    buttons: {
	        confirm: function () {
	            top.location.href = url;
	        },
	        cancel: function () {
	            ;
	        }
	    }
	}).css("font-family", "'PT Sans', sans-serif");;
}


function Currdate(elemnum, name) {
	all = new Date();
	today = all.getDate();
	month = all.getMonth();
	if (navigator.appName == 'Microsoft Internet Explorer') {
		year = all.getYear()
	}
	;
	if (navigator.appName != 'Microsoft Internet Explorer') {
		year = all.getYear() + 1900
	}
	;
	month = month + 1;
	if (navigator.appName != 'Microsoft Internet Explorer') {
		s = '';
		st = "";
		Str = new String;
		Str = navigator.appVersion;
		s = Str.charAt(0);
		if (s == '4')
			monthname = (month == 1) ? "January"
					: (month == 2) ? "February"
							: (month == 3) ? "March"
									: (month == 4) ? "April"
											: (month == 5) ? "May"
													: (month == 6) ? "June"
															: (month == 7) ? "July"
																	: (month == 8) ? "Augest"
																			: (month == 9) ? "September"
																					: (month == 10) ? "October"
																							: (month == 11) ? "November"
																									: "December";
		if (s != '4')
			monthname = (month == 1) ? "Январь"
					: (month == 2) ? "Февраль"
							: (month == 3) ? "Март"
									: (month == 4) ? "Апрель"
											: (month == 5) ? "Май"
													: (month == 6) ? "Июнь"
															: (month == 7) ? "Июль"
																	: (month == 8) ? "Август"
																			: (month == 9) ? "Сентябрь"
																					: (month == 10) ? "Октябрь"
																							: (month == 11) ? "Ноябрь"
																									: "Декабрь";
	}
	if (navigator.appName == 'Microsoft Internet Explorer') {
		monthname = (month == 1) ? "Январь"
				: (month == 2) ? "Февраль"
						: (month == 3) ? "Март"
								: (month == 4) ? "Апрель"
										: (month == 5) ? "Май"
												: (month == 6) ? "Июнь"
														: (month == 7) ? "Июль"
																: (month == 8) ? "Август"
																		: (month == 9) ? "Сентябрь"
																				: (month == 10) ? "Октябрь"
																						: (month == 11) ? "Ноябрь"
																								: "Декабрь";
	}
	hours = all.getHours();
	minutes = all.getMinutes();
	seconds = all.getSeconds();
	timevalue = " " + (hours);
	timevalue += ((minutes < 10) ? ":0" : ":") + minutes;
	timevalue += ((seconds < 10) ? ":0" : ":") + seconds;
	time = today + " " + monthname + ", " + year;
	if (!elemnum) {
		elemnum = 1;
	}
	if (!name) {
		name = 0;
	}
	document.forms[name].elements[elemnum].value = time;
}

function spoiler_js(obj) {
	var obj_content = obj.parentNode.parentNode.getElementsByTagName('div')[1];
	var obj_text_show = obj.getElementsByTagName('span')[1];
	var obj_text_hide = obj.getElementsByTagName('span')[0];

	if (obj_content.style.display != '') {
		obj_content.style.display = '';
		obj_text_show.style.display = '';
		obj_text_hide.style.display = 'none';
	} else {
		obj_content.style.display = 'none';
		obj_text_show.style.display = 'none';
		obj_text_hide.style.display = '';
	}
	return false;
}

function addLinkWizard(formindex, elemindex) {
	linkname = prompt("Введите название ссылки:", defaultText = "");
	if (linkname) {
		linkurl = prompt("Введите адрес ссылки (URL):", defaultText = "http://");
		if (linkurl) {
			top.document.forms[formindex].elements[elemindex].value = top.document.forms[formindex].elements[elemindex].value
					+ '<a href="'
					+ linkurl
					+ '" class=simple_link>'
					+ linkname
					+ '</a>\n';
		}
	}
}

function image_dimensions(imagehandler, thinkparam) {
	var i = new Image();
	i.onload = function() {
		var think_width = 100;
		var scale_koeff = 1;
		var scaleheight = 0;
		var scalewidth = 0;
		var globe_x = i.width;
		var globe_y = i.height;
		if (globe_x >= globe_y) {
			if (thinkparam == 1) {
				think_width = 122;
			}
			scale_koeff = (globe_x / think_width);
			if ((scale_koeff < 1)) {
				scale_koeff = 1;
			}
			scaleheight = Math.round((globe_y / scale_koeff));
			scalewidth = Math.round((globe_x / scale_koeff));
			imagehandler.width = scalewidth;
			imagehandler.height = scaleheight;
		}
		if (globe_x < globe_y) {
			think_width = 80;
			if (thinkparam == 1) {
				think_width = 87;
			}
			scale_koeff = (globe_x / think_width);
			if ((scale_koeff < 1)) {
				scale_koeff = 1;
			}
			scaleheight = Math.round((globe_y / scale_koeff));
			scalewidth = Math.round((globe_x / scale_koeff));
			imagehandler.width = scalewidth;
			imagehandler.height = scaleheight;
		}
	};
	i.src = imagehandler.src;
	return;
}
