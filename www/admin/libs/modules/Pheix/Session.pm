package Pheix::Session;

use strict;
use warnings;

##############################################################################
#
# File   :  Session.pm
#
##############################################################################
#
# Главный пакет управления сессиями авторизации в CMS Pheix
#
##############################################################################

our (@ISA, @EXPORT);
BEGIN {
    # get_keytag_by_crypted_id      -> getKeytagByCryptUid
    # session_id                    -> getSessionId
    # GetAccount                    -> getDecryptUid
    # add_currlogin                 -> createUserSession
    # clear_currlogin               -> delSessionByTimeOut
    # user_exit                     -> doUserExit
    # valid_user                    -> isValidUser
    # get_login_bysesid             -> getCryptLoginBySid
    # check_login                   -> isLoginExists
    # get_keytag_hash_by_userid     -> getKeytagByDecryptUid
    # get_cryptedlogin_by_userid    -> getCryptLoginByDecryptUid
    # get_keytag_hash_by_login      -> getKeytagByDecryptLogin
    # check_passwd_hash             -> isDefaultPasswd
    # get_passwd_hash               -> getCryptPasswdByCryptUid
    # renew_passwd_hash_forlogin    -> doPasswdChangeByCryptUid
    # bruteforce_check              -> isBruteforce
    # blowfish_encrypt_proc         -> doBlowfishEncrypt
    # blowfish_decrypt_proc         -> doBlowfishDecrypt
    # string_to_hexstring           -> getHexStrFromString
    # hexstring_to_string           -> getStringFromHexStr

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      getKeytagByCryptUid
      getSessionId
      getDecryptUid
      createUserSession
      delSessionByTimeOut
      doUserExit
      isValidUser
      getCryptLoginBySid
      isLoginExists
      getKeytagByDecryptUid
      getCryptLoginByDecryptUid
      getKeytagByDecryptLogin
      isDefaultPasswd
      getCryptPasswdByCryptUid
      doPasswdChangeByCryptUid
      isBruteforce
      doBlowfishEncrypt
      doBlowfishDecrypt
      getHexStrFromString
      getStringFromHexStr
      getDecryptFioBySesId
    );
}

use Digest::MD5 qw(md5_hex);
use Crypt::CBC;

#-------------------------------------------------------------------------------

my $DEFAULT_LOGIN       = 'rootadmin';
my $DEFAULT_PASSWORD    = 'rootpasswd';
my $CLIENT_PWD_PATH     = 'admin/system/client_pwd.tnk';
my $CURR_LOGIN_PATH     = 'admin/system/cur_login.tnk';
my $BRUTEFORCE_PATH     = 'admin/system/bruteforcing.tnk';

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = &getKeytagByCryptUid ( $id_from_auth )
#-------------------------------------------------------------------------------
#
# $id_from_auth - зашифрованный идентификатор пользователя.
# Получение keytag по зашифрованному идентификатору пользователя.
# Функция возвращает keytag.
#
#-------------------------------------------------------------------------------

sub getKeytagByCryptUid {
    my ($id_from_auth) = @_;
    my $decrypted_id_from_auth;
    my $param;
    #open my $fh, '<', $CLIENT_PWD_PATH or die "unable to open $CLIENT_PWD_PATH: ".$!;

    if (-e $CLIENT_PWD_PATH) {
        open my $fh, '<', $CLIENT_PWD_PATH;
        my @entries = <$fh>;
        close $fh;

        for my $i (0..$#entries) {
            $entries[$i] =~ s/[\r\n]+//g;
            if ( $entries[$i] ne '' ) {
                my @tmp = split(/\|/, $entries[$i]);
                my $keytag = $tmp[5];
                eval {
                    $decrypted_id_from_auth =
                      doBlowfishDecrypt(
                        getStringFromHexStr($id_from_auth), $keytag );
                };
                if ( $decrypted_id_from_auth eq $tmp[1] ) { $param = $keytag; }
            }
        }
    }

    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getSessionId ( $userid )
#-------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Получение идентификатору сессии по идентификатору пользователя.
# Функция возвращает идентификатор сессии.
#
#-------------------------------------------------------------------------------

sub getSessionId {
    my ($userid) = @_;
    my $previous = 0;
    my $sess_id  = 0;
    my $userid_logged;
    my $login;
    my $keytag        = getKeytagByDecryptUid($userid);
    open my $fh, '<', $CURR_LOGIN_PATH or die "unable to open $CURR_LOGIN_PATH: ".$!;
    my @currlogins = <$fh>;
    close($fh);
    for my $i (0..$#currlogins) {
        $currlogins[$i] =~ s/[\r\n]+//g;
        my @tmp = split( /\|/, $currlogins[$i] );
        eval {
            $userid_logged =
              doBlowfishDecrypt(
                getStringFromHexStr( $tmp[0] ), $keytag );
        };
        if ( $userid_logged eq $userid ) {
            if ( $previous < $tmp[3] ) {
                $sess_id  = $tmp[2];
                $previous = $tmp[3];
            }
        }
    }
    return $sess_id;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getDecryptUid ( $data_from_file )
#-------------------------------------------------------------------------------
#
# $data_from_file - содержимое файла client_pwd.tnk.
# $login - незашифрованный логин пользователя.
# $passw - незашифрованный пароль пользователя.
# Поиск авторизации в файле client_pwd.tnk.
# Функция возвращает незашифрованный идентификатор пользователя (timestamp)
#
#-------------------------------------------------------------------------------

sub getDecryptUid {
    my ($data_from_file, $login, $passw) = @_;
    my $return_value = 0;
    my $_login;
    $data_from_file =~ s/[\r\n]+//g;
    my @array      = split( /\|/, $data_from_file );
    my $keytag     = $array[5];
    my $hash_passw = md5_hex( $keytag . $passw );
    eval {
        $_login =
          doBlowfishDecrypt(
            getStringFromHexStr( $array[3] ), $keytag );
    };

    if ( ( $_login eq $login ) && ( $array[4] eq $hash_passw ) ) {
        if ( $array[ $#array - 2 ] != 3 || $array[ $#array - 1 ] != 3 || $array[$#array] != 3 ) {
            $return_value = $array[1];
        }
    }
    return $return_value;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = createUserSession ( $userid )
#-------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Добавление записи в файл cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#-------------------------------------------------------------------------------

sub createUserSession {
    my $userid    = $_[0];
    my $keytag    = &getKeytagByDecryptUid($userid);
    my $hash_ssid = '';
    my $rnd_num   = 0;
    my $user_ip   = $ENV{REMOTE_ADDR};
    my $found     = 1;

    while ( $found == 1 ) {
        $found   = 0;
        $rnd_num = 100000000;
        while ( $rnd_num <= 100000000 ) {
            my $r_temp = rand 1000000000;
            $rnd_num = 100000000;
            $rnd_num = sprintf "%u", $r_temp;
        }
        $hash_ssid = md5_hex( $rnd_num . $keytag );
        if ( -e "$CURR_LOGIN_PATH" ) {
            open( my $inputFH, "<", $CURR_LOGIN_PATH );
            my @loggedusers = <$inputFH>;
            for ( my $finx = 0 ; $finx <= $#loggedusers ; $finx++ ) {
                if ( $loggedusers[$finx] =~ /$hash_ssid/ ) {
                    $found = 1;
                    last;
                }
            }
            close $inputFH;
        }
    }
    my $hash_login = &getHexStrFromString( &doBlowfishEncrypt( $userid, $keytag ) );
    my $fh;
    if (-e $CURR_LOGIN_PATH) {
        open $fh, "+<", $CURR_LOGIN_PATH or die "open $CURR_LOGIN_PATH error: ".$!;
    } else {
        open $fh, ">", $CURR_LOGIN_PATH or die "open $CURR_LOGIN_PATH error: ".$!;
    }
    flock $fh, 2;
    seek $fh, 0, 2;
    print $fh join( "|", $hash_login, $user_ip, $hash_ssid, time ) . "\n";
    flock $fh, 8;
    close $fh;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# delSessionByTimeOut ()
#-------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Удаление устаревших записей из файла cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#-------------------------------------------------------------------------------

sub delSessionByTimeOut {
    my @alive_entries = ();
    open( my $inputFH, "<", $CURR_LOGIN_PATH );
    my @f_strings = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#f_strings ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $f_strings[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( time - $tmp[3] < 900 ) {
            push( @alive_entries, $f_strings[$clearLogInx] );
        }
    }

    open( my $outputFH, ">", $CURR_LOGIN_PATH );
    flock $outputFH, 2;
    print $outputFH @alive_entries;
    truncate( $outputFH, tell($outputFH) );
    flock $outputFH, 8;
    close($outputFH);
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# doUserExit ( $sesion_id )
#-------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Выход из административной панели, удаление сессии из cur_login.tnk.
# Функция не возвращает какое-либо значение.
#
#-------------------------------------------------------------------------------

sub doUserExit {
    if ( -e "$CURR_LOGIN_PATH" ) {
        my @alive_entries = ();
        open( my $inputFH, "<", $CURR_LOGIN_PATH );
        my @f_strings = <$inputFH>;
        close($inputFH);

        for ( my $clrLogInx = 0 ; $clrLogInx <= $#f_strings ; $clrLogInx++ )
        {
            my @tmp = split( /\|/, $f_strings[$clrLogInx] );
            $tmp[$#tmp] =~ s/[\r\n]+//g;
            if ( ( $tmp[2] ne $_[0] ) && ( $tmp[2] != $_[0] ) ) {
                push( @alive_entries, $f_strings[$clrLogInx] );
            } else {
                ;
            }
        }
        open( my $outputFH, ">", $CURR_LOGIN_PATH );
        flock( $outputFH, 2 );
        print $outputFH @alive_entries;
        truncate( $outputFH, tell($outputFH) );
        flock( $outputFH, 8 );
        close($outputFH);
    }
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = isValidUser ( $sesion_id )
#-------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Проверка $sesion_id на корректность с данными из cur_login.tnk.
# Функция возвращает 1, если данные корректны и -1 в противном случае.
#
#-------------------------------------------------------------------------------

sub isValidUser {
    my $sid = $_[0];
    if ( !( defined($sid) ) ) { $sid = 0; }
    my $valid   = 0;
    my $user_ip = $ENV{REMOTE_ADDR};

    open( my $inputFH, "<", $CURR_LOGIN_PATH );
    my @f_strings = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#f_strings ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $f_strings[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( $tmp[2] eq $sid && $tmp[1] eq $user_ip && ( ( time - $tmp[3] ) < 900 ) )
        {
            $valid = 1;
            $tmp[3] = time;
            $f_strings[$clearLogInx] = join( "\|", @tmp ) . "\n";
        }
    }
    open( my $outputFH, ">", $CURR_LOGIN_PATH );
    flock( $outputFH, 2 );
    print $outputFH @f_strings;
    truncate( $outputFH, tell($outputFH) );
    flock( $outputFH, 8 );
    close($outputFH);
    return $valid;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getCryptLoginBySid ( $sesion_id )
#-------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Получение зашифрованного идентификатора пользователя по идентификатору сессии
# из файла активных пользовательских сессий cur_login.tnk.
# Функция возвращает зашифрованный идентификатор пользователя.
#
#-------------------------------------------------------------------------------

sub getCryptLoginBySid {
    my $param = -1;
    my $found = 0;
    open( my $inputFH, "<", $CURR_LOGIN_PATH );
    my @entries = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $entries[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( $tmp[2] eq $_[0] && $found == 0 ) {
            $param = $tmp[0];
            last;
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = isLoginExists ( $crypted_login )
#-------------------------------------------------------------------------------
#
# $crypted_login - зашифрованный логин пользователя.
# Проверка логина на присутствие в файле cur_login.tnk.
# Функция возвращает 0, если логин найден и 1 в противном случае.
#
#-------------------------------------------------------------------------------

sub isLoginExists {
    open( my $inputFH, "<", $CURR_LOGIN_PATH );
    my @f_strings = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#f_strings ; $clearLogInx++ ) {
        my @tmp = split( /\|/, $f_strings[$clearLogInx] );
        $tmp[$#tmp] =~ s/[\r\n]+//g;
        if ( $tmp[0] eq $_[0] ) {
            return 0;
        }
    }
    return 1;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getKeytagByDecryptUid ( $crypted_login )
#-------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Получение keytag из client_pwd.tnk по незашифрованному
# идентификатору пользователя.
# Функция возвращает keytag.
#
#-------------------------------------------------------------------------------

sub getKeytagByDecryptUid {
    my $param  = -1;
    my $userid = $_[0];
    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);
    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        $entries[$clearLogInx] =~ s/[\r\n]+//g;
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            if ( $tmp[1] eq $userid ) { $param = $tmp[5]; }
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getCryptLoginByDecryptUid ( $crypted_login )
#-------------------------------------------------------------------------------
#
# $userid - незашифрованный идентификатор пользователя.
# Получение зашифрованного логина из client_pwd.tnk по незашифрованному
# идентификатору пользователя.
# Функция возвращает зашифрованный логин.
#
#-------------------------------------------------------------------------------

sub getCryptLoginByDecryptUid {
    my $param  = -1;
    my $userid = $_[0];

    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);
    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        $entries[$clearLogInx] =~ s/[\r\n]+//g;
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            if ( $tmp[1] eq $userid ) { $param = $tmp[3]; }
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getKeytagByDecryptLogin ( $crypted_login )
#-------------------------------------------------------------------------------
#
# $login_from_auth - незашифрованный логин пользователя.
# Получение keytag из client_pwd.tnk по незашифрованному логину пользователя.
# Функция возвращает keytag.
#
#-------------------------------------------------------------------------------

sub getKeytagByDecryptLogin {
    my $param           = -1;
    my $login_from_db   = '';
    my $login_from_auth = $_[0];

    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);
    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        $entries[$clearLogInx] =~ s/[\r\n]+//g;
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            my $keytag = $tmp[5];
            eval {
                $login_from_db =
                  &doBlowfishDecrypt(
                    &getStringFromHexStr( $tmp[3] ), $keytag );
            };
            if ( $login_from_db eq $login_from_auth ) { $param = $keytag; }
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = isDefaultPasswd ( $id_from_auth )
#-------------------------------------------------------------------------------
#
# $id_from_auth - зашифрованный идентификатор пользователя.
# Проверка текущего пароля на пароль по умолчанию в client_pwd.tnk.
# Функция возвращает 1, если найден пароль по умолчанию и 0 в противном случае.
#
#-------------------------------------------------------------------------------

sub isDefaultPasswd {
    my $param                = 0;
    my $session_id           = 0;
    my $user_id_from_session = '';
    my $login_from_session   = '';
    my $keytag               = &getKeytagByCryptUid( $_[0] );
    eval {
        $user_id_from_session =
          &doBlowfishDecrypt(
            &getStringFromHexStr( $_[0] ), $keytag );
    };
    my $logincrypted =
      &getCryptLoginByDecryptUid($user_id_from_session);
    eval {
        $login_from_session =
          &doBlowfishDecrypt(
            &getStringFromHexStr($logincrypted), $keytag );
    };

    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        $entries[$clearLogInx] =~ s/[\r\n]+//g;
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            if ( ( $tmp[1] eq $user_id_from_session ) ) {
                my $passwd_hash = md5_hex( $keytag . $DEFAULT_PASSWORD );
                if (   $passwd_hash eq $tmp[4]
                    && $login_from_session eq $DEFAULT_LOGIN )
                {
                    $param = 1;
                }
            }
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getCryptPasswdByCryptUid ( $id_from_auth )
#-------------------------------------------------------------------------------
#
# $id_from_auth - зашифрованный идентификатор пользователя.
# Получение хэша пароля из client_pwd.tnk по зашифрованному
# идентификатору пользователя.
# Функция возвращает хэш пароля.
#
#-------------------------------------------------------------------------------

sub getCryptPasswdByCryptUid {
    my $param               = -1;
    my $session_id          = 0;
    my $userid_from_session = '';
    my $userid_from_db      = '';
    my $keytag              = &getKeytagByCryptUid( $_[0] );
    eval {
        $userid_from_session =
          &doBlowfishDecrypt(
            &getStringFromHexStr( $_[0] ), $keytag );
    };
    #print;
    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);

    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        $entries[$clearLogInx] =~ s/[\r\n]+//g;
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            if ( $tmp[1] eq $userid_from_session ) {
                $param = $tmp[4];
            }
        }
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = renew_passwd_hash_forlogin ( $useridcrypted, $keytag, $new_passw_hash )
#-------------------------------------------------------------------------------
#
# $useridcrypted - зашифрованный идентификатор пользователя.
# $keytag - keytag.
# $new_passw_hash - новый хэш пароля.
# Смена пароля для заданного пользователя (зашифрованный идентификатор)
# в client_pwd.tnk.
# Функция возвращает хэш пароля.
#
#-------------------------------------------------------------------------------

sub doPasswdChangeByCryptUid {
    my $param          = -1;
    my $useridcrypted  = $_[0];
    my $keytag         = $_[1];
    my $new_passw_hash = $_[2];

    #print "@_";

    my $login_from_session   = '';
    my $login_from_db        = '';
    my $user_id_from_session = '';

    eval {
        $user_id_from_session =
          &doBlowfishDecrypt(
            &getStringFromHexStr($useridcrypted), $keytag );
    };
    my $logincrypted =
      &getCryptLoginByDecryptUid($user_id_from_session);
    eval {
        $login_from_session =
          &doBlowfishDecrypt(
            &getStringFromHexStr($logincrypted), $keytag );
    };

    open( my $inputFH, "<", $CLIENT_PWD_PATH );
    my @entries = <$inputFH>;
    close($inputFH);
    for ( my $clearLogInx = 0 ; $clearLogInx <= $#entries ; $clearLogInx++ ) {
        if ( $entries[$clearLogInx] ne '' ) {
            my @tmp = split( /\|/, $entries[$clearLogInx] );
            eval {
                $login_from_db =
                  &doBlowfishDecrypt(
                    &getStringFromHexStr( $tmp[3] ),
                    $tmp[5] );
            };
            if ( $login_from_session eq $login_from_db ) {
                $tmp[4] = $new_passw_hash;
                $entries[$clearLogInx] = join( "|", @tmp );
                $param = 0;
                last;
            }
        }
    }
    if ( $param > -1 ) {
        open( my $outputFH, ">", $CLIENT_PWD_PATH );
        flock( $outputFH, 2 );
        print $outputFH @entries;
        truncate( $outputFH, tell($outputFH) );
        flock( $outputFH, 8 );
        close($outputFH);
    }
    return $param;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = isBruteforce ( $remote_ip, $soft )
#-------------------------------------------------------------------------------
#
# $remote_ip - удаленный IP-адрес.
# $soft - флаг протоколирования.
# Функция проверки брутфорса пароля для входа.
# Функция возвращает 0, если брутфорс не обнаружен или количество
# секунд до разблокировки доступа
#
#-------------------------------------------------------------------------------

sub isBruteforce {
    my $bruteforce_value = 5;
    my $remote_ip        = $_[0];
    if ( !( defined($remote_ip) ) ) { $remote_ip = ''; }
    my $soft            = $_[1];
    my $bruteforcing    = 0;
    my $found_ip_inlist = 0;
    my $lock_form       = 0;
    my @entries         = ();

    if ( -e "$BRUTEFORCE_PATH" ) {
        open( my $inputFH, "<", $BRUTEFORCE_PATH );
        @entries = <$inputFH>;
        close($inputFH);
    }

    for ( my $kinx = 0 ; $kinx <= $#entries ; $kinx++ ) {
        $entries[$kinx] =~ s/[\r\n]+//g;
        my @entry_arr = split( /\|/, $entries[$kinx] );
        $entry_arr[0] =~ s/[\W]//g;
        my $time_delta = time - $entry_arr[0];
        if ( $remote_ip eq $entry_arr[1] ) {
            $found_ip_inlist = 1;
            if ( $time_delta <= 30 ) {
                if ( $entry_arr[2] == $bruteforce_value ) {
                    $bruteforcing = $time_delta;
                } else {
                    $entry_arr[2]++;
                    $entry_arr[3]++;
                    $entry_arr[0] = time;
                    if ( $entry_arr[2] == $bruteforce_value ) {
                        $lock_form = 1;
                    }
                }
            } else {
                if ( $time_delta > 600 ) {
                    $bruteforcing = 0;
                    $entry_arr[0] = time;
                    $entry_arr[2] = 1;
                    $entry_arr[3]++;
                } else {
                    if ( $entry_arr[2] < $bruteforce_value ) {
                        $entry_arr[0] = time;
                        $entry_arr[2] = 1;
                        $entry_arr[3]++;
                    } else { $bruteforcing = $time_delta; }
                }
            }
        }
        $entries[$kinx] = join( "|", @entry_arr ) . "\n";
    }
    if ( $found_ip_inlist == 0 ) {
        my $value = 0;
        if ( ( $soft eq 'logging' ) ) { $value = 1; }
        push( @entries, join( "|", time, $remote_ip, $value, $value ) . "\n" );
    }
    if ( ( $soft eq 'logging' ) && ( $bruteforcing == 0 ) ) {
        open( my $outputFH, ">", $BRUTEFORCE_PATH );
        flock( $outputFH, 2 );
        print $outputFH @entries;
        flock( $outputFH, 8 );
        close($outputFH);
    }
    return $lock_form > 0 ? $lock_form : $bruteforcing;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = doBlowfishEncrypt ( $strtoencrypt, $keytag )
#-------------------------------------------------------------------------------
#
# $strtoencrypt - строка.
# $keytag - keytag.
# Шифрование некоторой строки.
# Функция возвращает зашифрованную строку
#
#-------------------------------------------------------------------------------

sub doBlowfishEncrypt {
    my $strtoencrypt = $_[0];
    my $keytag       = $_[1];
    my $ciphertext   = '';
    if ( $strtoencrypt ne '' && $keytag ne '' ) {
        my $cipher = Crypt::CBC->new( -key => $keytag, -cipher => 'Blowfish' );
        $ciphertext = $cipher->encrypt($strtoencrypt);
    }
    return $ciphertext;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = &doBlowfishDecrypt ( $strtodecrypt, $keytag )
#-------------------------------------------------------------------------------
#
# $strtodecrypt - строка.
# $keytag - keytag.
# Дешифрование некоторой строки.
# Функция возвращает дешифрованную строку
#
#-------------------------------------------------------------------------------

sub doBlowfishDecrypt {
    my $strtodecrypt = $_[0];
    my $keytag       = $_[1];
    my $plaintext    = '';
    if ( $strtodecrypt ne '' && $keytag ne '' ) {
        my $cipher = Crypt::CBC->new( -key => $keytag, -cipher => 'Blowfish' );
        $plaintext = $cipher->decrypt($strtodecrypt);
    }
    return $plaintext;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getHexStrFromString ( $str )
#-------------------------------------------------------------------------------
#
# $str - строка.
# Представление строки в виде hex-кодов символов.
# Функция возвращает строку в виде hex-кодов символов
#
#-------------------------------------------------------------------------------

sub getHexStrFromString {
    my $str       = $_[0];
    my $outstr    = '';
    my @chars_arr = split( //, $str );
    for ( my $charInx = 0 ; $charInx <= $#chars_arr ; $charInx++ ) {
        if ( $chars_arr[$charInx] ne '' ) {
            my $hex_value = "%" . sprintf( "%x", ord( $chars_arr[$charInx] ) );
            $outstr .= $hex_value;
        }
    }
    return $outstr;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getStringFromHexStr ( $str )
#-------------------------------------------------------------------------------
#
# $str - строка в виде hex-кодов символов.
# Представление строки hex-кодов символов в виде обычной строки
# Функция возвращает обычную текстовую строку
#
#-------------------------------------------------------------------------------

sub getStringFromHexStr {
    my $str       = $_[0];
    my $outstr    = '';
    my @chars_arr = split( /%/, $str );
    for ( my $charInx = 0 ; $charInx <= $#chars_arr ; $charInx++ ) {
        if ( $chars_arr[$charInx] ne '' && $chars_arr[$charInx] =~ /^[a-zA-Z0-9]+$/ ) {
            $chars_arr[$charInx] = hex( $chars_arr[$charInx] );
            my $char = chr( $chars_arr[$charInx] );
            $outstr .= $char;
        }
    }
    return $outstr;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getDecryptUidBySesId ( $str )
#-------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии доступа к административной части.
# Функция возвращает дешифрованный идентификатор пользователя.
#
#-------------------------------------------------------------------------------

sub getDecryptUidBySesId {
    my ( $sesid ) = @_;
    my $cryptuid = getCryptLoginBySid($sesid);
    my $keytag     = getKeytagByCryptUid( $cryptuid );
    my $decryptuid;
    eval {
        $decryptuid =
          doBlowfishDecrypt(
            getStringFromHexStr($cryptuid), $keytag );
    };
    return $decryptuid;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getDecryptLgnBySesId ( $str )
#-------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии доступа к административной части.
# Функция возвращает дешифрованный логин пользователя.
#
#-------------------------------------------------------------------------------

sub getDecryptLgnBySesId {
    my ( $sesid ) = @_;
    my $cryptuid = getCryptLoginBySid($sesid);
    my $keytag     = getKeytagByCryptUid( $cryptuid );
    my $decryptuid;
    my $decryptlgn;
    eval {
        $decryptuid =
          doBlowfishDecrypt(
            getStringFromHexStr($cryptuid), $keytag );
    };
    if ($decryptuid) {
        my $ctyptlgn = getCryptLoginByDecryptUid($decryptuid);
        $decryptlgn =
          doBlowfishDecrypt(
            getStringFromHexStr($ctyptlgn), $keytag );
    }
    if (!defined $decryptlgn) {
        $decryptlgn = 'Логин неопределен';
    }
    return $decryptlgn;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = getDecryptFioBySesId ( $str )
#-------------------------------------------------------------------------------
#
# $sesid - идентификатор сессии доступа к административной части.
# Функция возвращает ФИО пользователя.
#
#-------------------------------------------------------------------------------

sub getDecryptFioBySesId {
    my ( $sesid ) = @_;
    my $cryptuid = getCryptLoginBySid($sesid);
    my $keytag     = getKeytagByCryptUid( $cryptuid );
    my $decryptuid;
    my $decryptfio;
    eval {
        $decryptuid =
          &doBlowfishDecrypt(
            &getStringFromHexStr($cryptuid), $keytag );
    };
    if ($decryptuid) {
        open my $infh, "<", $CLIENT_PWD_PATH or die "unable to open $!";
        my @usrs = <$infh>;
        close $infh or die "unable to close $!";
        for my $i (0..$#usrs) {
        $usrs[$i] =~ s/[\r\n]+//g;
        if ( $usrs[$i] ne '' ) {
            my @tmp = split( /\|/, $usrs[$i] );
            if ( $tmp[1] eq $decryptuid ) {
                $decryptfio =
                    doBlowfishDecrypt(
                        getStringFromHexStr($tmp[2]), $keytag );
                last;
                }
            }
        }
    }
    if (!defined $decryptfio) {
        $decryptfio = 'Пользователь неопределен';
    }
    return $decryptfio;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = defaultCredentials ($login, $password)
#-------------------------------------------------------------------------------
#
# $login - логин.
# $password - пароль.
# Функция возвращает 0 если логин и пароль не являются дефолтными, иначе time()
#
#-------------------------------------------------------------------------------

sub defaultCredentials {
    my ($login, $password) = @_;

    return time if $login eq $DEFAULT_LOGIN && $password eq $DEFAULT_PASSWORD;

    return 0;
}

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# $rc = addDefaultAccount ($login, $password)
#-------------------------------------------------------------------------------
#
# $login - логин.
# $password - пароль.
# Функция возвращает 1 если дефолтный аккаунт добавлен в систему
#
#-------------------------------------------------------------------------------

sub addDefaultAccount {
    my ($create_time, $login, $password) = @_;

    if (-e $CLIENT_PWD_PATH) {
        return 0;
    }
    else {
        my $keytag = md5_hex(rand());
        my $userid = int(rand($create_time - 1524138900)) + 1524138900;

        my @account = (
            $create_time,
            $userid,
            getHexStrFromString(doBlowfishEncrypt($DEFAULT_LOGIN, $keytag)),
            getHexStrFromString(doBlowfishEncrypt($DEFAULT_LOGIN, $keytag)),
            md5_hex(sprintf("%s%s", $keytag, $password)),
            $keytag,
            2,
            2,
            2,
        );

        my $outputFH;

        open($outputFH, ">", $CLIENT_PWD_PATH) && flock($outputFH, 2) && seek($outputFH, 0, 2);

        print $outputFH join(q{|}, @account);

        flock($outputFH, 8) && close($outputFH);

        return $userid;
    }

    return 0;
}


#-------------------------------------------------------------------------------

END {}

1;
