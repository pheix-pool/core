package Config::ModuleMngr;

use strict;
use warnings;

###############################################################################
#
# File   :  ModuleMngr.pm
# History:  20150129 - включен в CMS вместо config.admin.lib
#           20150203 - подробное описание в комментариях к функциям модуля
#           20150227 - новые имена функций в соответствии с правилами
#                      именования CMS, export функций
#
###############################################################################
#
# Главный пакет конфигурирования административной части CMS Pheix
#
###############################################################################

our ( @ISA, @EXPORT );

BEGIN {

    use lib '../../extlibs';
    # show_upload_result            -> showUploadRes
    # show_uninstall_result         -> showUninstallRes
    # module_pwd                    -> getModWorkDir
    # powermodule_procedure         -> setModPowerOn
    # get_power_status_for_module   -> getModPowerOn
    # changepassw_procedure         -> setPassword
    # get_config_value              -> getConfigValue
    # search_for_installed_modules  -> getInstalledModsFromPl
    # get_installed_modules         -> getInstalledModsFromConf
    # get_module_actions            -> getModActions
    # upload_module_to_server       -> doModUpload
    # get_subdirs                   -> getDirsArray
    # get_files_array               -> getFilesArray
    # lookup_for_module_xml         -> getModXml
    # move_files_fromdir_todir      -> moveFilesFromTo
    # update_install_file           -> doInstallFileUpdate
    # make_backup_copy              -> doBackupCopy
    # check_if_installed            -> isModInstalled
    # check_min_version             -> isVersionCompatible
    # parse_config_path             -> doConfigPathParse
    # exec_syscall_and_getstrings   -> doSyscall
    # delete_files_fromdir          -> delFiles
    # delete_dir_with_files         -> delDir
    # get_installed_modules_table   -> getInstalledModsTable
    # main_install_procedure        -> doModInstall
    # update_source_code            -> doCodeUpdateAftrInstall
    # main_uninstall_procedure      -> doModUninstall
    # update_source_code_uninstall  -> doCodeUpdateAftrUninstall
    # CFG_uninstallresult           -> getUninstallRes
    # CFG_uploadresult              -> getUploadRes
    # install_new_module_form       -> getInstallModForm
    # CFG_startpage                 -> getMMHomePage

    require Exporter;
    @ISA = qw(Exporter);
    our @EXPORT = qw(
      showUpdateHint
      showSetupPage
      showUploadRes
      showUninstallRes
      getModWorkDir
      setModPowerOn
      getModPowerOn
      setPassword
      getConfigValue
      getInstalledModsFromPl
      getInstalledModsFromConf
      getModActions
      doModUpload
      getDirsArray
      getModXml
      moveFilesFromTo
      doInstallFileUpdate
      doBackupCopy
      isModInstalled
      isVersionCompatible
      doConfigPathParse
      doSyscall
      delDir
      delFiles
      getFilesArray
      getInstalledModsTable
      doModInstall
      doCodeUpdateAftrInstall
      doModUninstall
      doCodeUpdateAftrUninstall
      getUninstallRes
      getUploadRes
      getInstallModForm
      getMMHomePage
      checkIfSetupable
      getPageSetupCntnt
      showSetupTab
      saveModSettings
      saveXMLData
      getSingleSetting
      getSettingFromGroupByAttr
      getSettingChildNum
      getProtoSrvName
    );
}

use POSIX;
use Archive::Extract;
use File::Copy;
use File::Path;
use File::Basename;
use IPC::Open3;
use Symbol qw(gensym);
use IO::File;
use Digest::MD5 qw(md5_hex);
use XML::LibXML;
use LWP::UserAgent;
use JSON;
use Encode;

#------------------------------------------------------------------------------

my $MODULES_USRPATH   = 'libs/modules';
my $MODULES_PATH      = 'admin/libs/modules';
my $USER_MODULES_PATH = 'libs/modules';
my $ADMIN_SKIN_PATH   = 'admin/skins/classic_.txt';
my $INSTALL_DB_PATH   = 'conf/system/install.tnk';
my $BACKUP_PATH       = 'conf/backup';

my $BASE_VERSION    = 2.11;
my $MODULE_NAME     = "Config";
my $MODULE_FOLDER   = "$MODULES_PATH/$MODULE_NAME/";
my $STATIC_SM_PATH  = getConfigValue( $MODULE_FOLDER, getModXml($MODULE_FOLDER), 'staticsitemap' );

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showUpdateHint ( $printflag, $procstat, $procmess)
#------------------------------------------------------------------------------
#
# $printflag - флаг управления печатью.
# $procstat - статус завершения операции.
# $procmess - сообщение.
# Вывод jgrowl сообщения.
# Функция выводит jgrowl сообщение.
#
#------------------------------------------------------------------------------

sub showUpdateHint {
    my ( $printflag, $procstat, $procmess ) = @_;
    $procmess =~ s/[\r\n]//g;
    $procmess =~ s/[\s]+/ /g;
    my $ipdatetime;
    my $hint;
    my $jgrowlhint = qq~
        <script type="text/javascript">
          (function(\$){
          \$(document).ready(function(){
          \$.jGrowl.defaults.closer = false;
          \$('\#updatehint').jGrowl("<div class=update_hint>\\
          %hint_content%
          </div>",
              {
                life: 10000,
                theme: 'updatehint',
                speed: 'slow',
                animateOpen: {
                    height: "show",
                },
                animateClose: {
                    height: "hide",
                }
              });
          });
          })(jQuery);
        </script>
        <div id="updatehint" class="top-right" style="display:inline; margin-top:15px;"></div>
      ~;

    if (localtime =~ /([\d]+:[\d]+:[\d]+)/) {
        $ipdatetime = "$1";
    }
    my $message = qq~<p class=hinthead>$ipdatetime - Операция выполнена успешно!</p>~;
    if ($printflag == 1) {
        if ($procstat != 1) {
            # Операция завершилась ошибкой
            $message = qq~<p class=hinthead>$ipdatetime - Операция завершилась ошибкой</p>~;
        }
        $hint = "$message\\\n$procmess\\";
    } else {
        #$hint = $message;
        $jgrowlhint = '';
    }
    $jgrowlhint =~ s/%hint_content%/$hint/g;
    return $jgrowlhint;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showSetupPage ( $sesion_id )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы редактирования настроек модуля.
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showSetupPage {
    open( my $TempFileHandl, "<", $ADMIN_SKIN_PATH );
    my @scheme    = <$TempFileHandl>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $content = getPageSetupCntnt(@_);
            $scheme[$cnt] =~ s/\%content\%/$content/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($TempFileHandl);
}


#-----------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showUploadRes ( $sesion_id )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы с результатами инсталляции модуля.
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showUploadRes {
    open( my $TempFileHandl, "<", $ADMIN_SKIN_PATH );
    my @scheme    = <$TempFileHandl>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $topics = getUploadRes(@_);
            $scheme[$cnt] =~ s/\%content\%/$topics/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($TempFileHandl);
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showUninstallRes ( $sesion_id )
#------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод страницы с результатами деинсталляции модуля.
# Функция не возвращает какое-либо значение.
#
#------------------------------------------------------------------------------

sub showUninstallRes {
    open( my $TempFileHandl, "<", $ADMIN_SKIN_PATH );
    my @scheme    = <$TempFileHandl>;
    my $cnt       = 0;
    my $sesion_id = $_[0];
    while ( $cnt <= $#scheme ) {
        $scheme[$cnt] =
          Pheix::Tools::fillCommonTags( $sesion_id, $scheme[$cnt] );
        if ( $scheme[$cnt] =~ /\%content\%/ ) {
            my $topics = getUninstallRes(@_);
            $scheme[$cnt] =~ s/\%content\%/$topics/;
        }
        print $scheme[$cnt];
        $cnt++;
    }
    close($TempFileHandl);
}
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModWorkDir ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Полный путь до каталога модуля (административная часть).
# Функция возвращает полный путь до каталога модуля (административная часть).
#
#------------------------------------------------------------------------------

sub getModWorkDir {
    return $MODULE_FOLDER;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# setModPowerOn ( $modulename )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# Включение-выключение заданного модуля.
# Функция возвращает статус операции в HTML формате.
#
#------------------------------------------------------------------------------

sub setModPowerOn {
    my $modulename    = $_[0];
    my $message_ok_1  = "Модуль «$modulename» выключен!";
    my $message_ok_2  = "Модуль «$modulename» включен!";
    my $message_fail  = "Не удалось выключить модуль «$modulename»!";
    my $file_to_parse = "$INSTALL_DB_PATH";
    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        my $status = -1;
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $modulename ) {
                $tmp[4] = $tmp[4] ^ 1;
                $status = $tmp[4];
                $filecontent[$admplinx] = join( "|", @tmp );
                last;
            }
        }
        open( my $outputFH, ">", $file_to_parse );
        print $outputFH @filecontent;
        close($outputFH);
        if ( $status == 1 ) {
            return qq~<p class=hintcont id=status>$message_ok_1</p>~;
        } else {
            return qq~<p class=hintcont id=status>$message_ok_2</p>~;
        }
    } else {
        return qq~<p class=hintcont id=error>$message_fail</p>~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModPowerOn ( $modulename )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# Получить статус модуля (включен или выключен).
# Функция возвращает статус модуля (1 - выключен, 0 - включен) или пустую строку
# в случае, если модуль с заданным именем не найден
#
#------------------------------------------------------------------------------

sub getModPowerOn {
    my $modulename    = $_[0];
    my $file_to_parse = "$INSTALL_DB_PATH";
    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            if ( $tmp[2] eq $modulename ) { return $tmp[4]; }
        }
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# setPassword ( $sessionid, $oldpassw, $newpassw, $renewpassw)
#------------------------------------------------------------------------------
#
# $sessionid - идентификатор сессии.
# $oldpassw - старый пароль.
# $newpassw - новый пароль.
# $renewpassw - повторный ввод нового пароля.
# Смена пароля (вызов из reminder).
# Функция возвращает статус операции в HTML формате.
#
#------------------------------------------------------------------------------

sub setPassword {
    my ($sid, $oldpassw, $newpassw, $renewpassw) = @_;
    my $rt_mess;

    my @resources = (
        "Пароль успешно изменен.",
        "Пароль не изменен: login не найден!",
        "Введен неверный старый пароль!",
        "Новые пароли не совпадают!"
        );
    if ( $newpassw eq $renewpassw ) {
        my $cryptedid = Pheix::Session::getCryptLoginBySid($sid);
        my $keytag    = Pheix::Session::getKeytagByCryptUid($cryptedid);
        my $oldpassw_hash_1 =
          Pheix::Session::getCryptPasswdByCryptUid($cryptedid);
        my $oldpassw_hash_2 = md5_hex( $keytag . $oldpassw );

        if ( $oldpassw_hash_1 eq $oldpassw_hash_2 ) {
            my $newpassw_hash = md5_hex( $keytag . $newpassw );
            my $ret =
              Pheix::Session::doPasswdChangeByCryptUid( $cryptedid, $keytag,
                $newpassw_hash );
            if ( $ret > -1 ) {
                my $_sermess;
                my $xmlfile  = getModXml($MODULE_FOLDER);
                my $_setting = {
                    setting => '/module/configuration/settings/servername',
                    value => $ENV{SERVER_NAME},
                };
                my $_rc = saveXMLDataSetting ( $sid, $MODULE_FOLDER, $xmlfile, $_setting);
                if ($_rc > 0) {
                    $_sermess = '&nbsp;Обновлено имя сервера!';
                }
                $rt_mess = qq~<p class=hintcont id=status>$resources[0]$_sermess</span>~;
            } else {
                $rt_mess = qq~<p class=hintcont id=error>$resources[1]</span>~;
            }
        } else {
            $rt_mess = qq~<p class=hintcont id=error>$resources[2]</span>~;
        }
    } else {
        $rt_mess = qq~<p class=hintcont id=error>$resources[3]</span>~;
    }
    return $rt_mess;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getConfigValue ( $module_path, $module_name, $value )
#------------------------------------------------------------------------------
#
# $module_path - путь до каталога модуля (административная часть).
# $module_name - название модуля.
# $value - название значения, которое нужно получить.
# Функции разбора конфигурационного файла модуля в формате XML
# Функция возвращает значение тега $value из конфигурационного файла модуля
# в формате XML или статус операции в HTML формате, если тег не найден.
#
# Краткий комментарий к полям конфигурационного файла (перечисление названий):
#
#	module
#	name
#	menuitem
#	description
#	version
#	module_library
#	module_include
#	startaction
#	startfunction
#	configuration
#	tankfile
#	indexfile
#	datafile
#
#------------------------------------------------------------------------------

sub getConfigValue {
    my $module_path   = $_[0];
    my $file_to_parse = $module_path . "/" . $_[1];
    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        for ( my $cfginx = 0 ; $cfginx <= $#filecontent ; $cfginx++ ) {
            if ( $filecontent[$cfginx] =~ /<([a-z\_]+){0,}>/ ) {
                my $tmp_value = $1;
                if (   ( $tmp_value eq $_[2] )
                    && ( $filecontent[$cfginx] =~ />(.{0,})</ ) )
                {
                    return $1;
                }
            }
        }
        return qq~Элемент $_[2] не найден~;
    } else {
        return qq~<div class="error">$file_to_parse не найден!</div>~;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getInstalledModsFromPl ( $module_type )
#------------------------------------------------------------------------------
#
# $module_type - тип модуля (0 - admin, 1 - user).
# Поиск установленных модулей в admin.pl.
# Функция возвращает массив найденных модулей или пустое значение, если
# модули на найдены
#
#------------------------------------------------------------------------------

sub getInstalledModsFromPl {
    my @found_modules = ();
    my $module_type   = $_[0];    # 0 - admin, 1 - user;
    my $file_to_parse = "";

    if   ( $module_type == 0 ) {
        $file_to_parse = "admin.pl";
    } else {
        $file_to_parse = "user.pl";
    }

    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            if ( $filecontent[$admplinx] =~ /\# \(\%INSTALLED\_MODULE\%/ ) {
                if ( $filecontent[$admplinx] =~ /\# \((.{0,})\) #/ ) {
                    my $module_sys_descr = $1;
                    my @tmp = split( /\,/, $module_sys_descr );
                    if ( $#tmp == 5 ) {
                        push( @found_modules, $module_sys_descr );
                    } else {
                        return;
                    }
                }
            }
        }
        return @found_modules;
    } else {
        return;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getInstalledModsFromConf ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Поиск установленных модулей в install.tnk.
# Функция возвращает массив найденных модулей или пустое значение, если
# модули на найдены
#
#------------------------------------------------------------------------------

sub getInstalledModsFromConf {
    my @found_modules = ();
    my $file_to_parse = $INSTALL_DB_PATH;

    if ( -e $file_to_parse ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my @tmp = split( /\|/, $filecontent[$admplinx] );
            push( @found_modules, $tmp[3] );
        }
        return @found_modules;
    } else {
        return;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getInstAppMods ()
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Поиск установленных модулей в install.tnk.
# Функция возвращает массив найденных модулей или пустое значение, если
# модули на найдены.
#
#------------------------------------------------------------------------------

sub getInstAppMods {
    my @_appmods;
    if ( -e $INSTALL_DB_PATH ) {
        open( my $fh, "<", $INSTALL_DB_PATH );
        my @_rows = <$fh>;
        close $fh;
        for my $i (0..$#_rows) {
            my @_cols = split /\|/, $_rows[$i];
            my $_type = getConfigValue(
                dirname($_cols[3]), $_cols[2], 'systemtype'
            );
            if ($_type != 1) {
                push(
                    @_appmods,
                    $MODULES_PATH.'/'.$_cols[2].'/'.basename($_cols[3])
                );
            }
        }
        return @_appmods;
    } else {
        return;
    }
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getModActions ( $module_identifer, $file_to_parse )
#------------------------------------------------------------------------------
#
# $module_identifer - идентификатор типа (%INSTALLED_CONFIGMODULE_PROCEDURES%).
# $file_to_parse - файл, в котором выполняется поиск (admin.pl или user.pl).
# Поиск actions для модуля в admin.pl.
# Функция возвращает массив найденных actions или пустое значение, если
# модули на найдены
#
#------------------------------------------------------------------------------

sub getModActions {
    my $module_identifer = $_[0];
    my $file_to_parse    = $_[1];
    my $action_index     = 0;
    if ( $file_to_parse eq '' ) {
        $file_to_parse = "admin.pl";
        $action_index  = 1;
    }
    my @found_actions = ();

    if ( ( -e $file_to_parse ) && ( $module_identifer ne '' ) ) {
        open( my $inputFH, "<", $file_to_parse );
        my @filecontent = <$inputFH>;
        close($inputFH);
        my $start_index = -1;
        my $end_index   = -1;
        for ( my $admplinx = 0 ; $admplinx <= $#filecontent ; $admplinx++ ) {
            my $tst_src_string = $filecontent[$admplinx];
            $tst_src_string =~ s/[\r\n]+//g;
            if ( $tst_src_string =~ /^\# \($_[0]\) \#$/ ) {
                if ( ( $tst_src_string =~ /\# \((.{0,})\) #/ )
                    && $start_index == -1 )
                {
                    $start_index = $admplinx;
                }
                if ( ( $tst_src_string =~ /\# \((.{0,})\) #/ )
                    && $start_index > -1 )
                {
                    $end_index = $admplinx;
                }
            }
        }
        my @src_tmp_fragment = @filecontent[ $start_index ... $end_index ];

        for (
            my $srcfraginx = 0 ;
            $srcfraginx <= $#src_tmp_fragment ;
            $srcfraginx++
          )
        {
            if ( $src_tmp_fragment[$srcfraginx] =~
                /\$env\_params\[$action_index\] eq \'(.{1,})\'/ )
            {
                push( @found_actions, $1 );
            }
        }
        return @found_actions;
    } else {
        return;
    }
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# doModUpload ( $module_identifer, $file_to_parse )
#-----------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Загрузка модуля на сервер.
# Функция возвращает массив (<статус_загрузки>, <имя_загруженного_файла>) или
# сообщение об ошибке в HTML формате.
#
#-----------------------------------------------------------------------------------

sub doModUpload {
    my $res            = '';
    my $path_to_upload = $MODULES_PATH;
    my $tmp_file_name  = time . "_.zip";
    my $file_to_upload = $_[0];
    if ( $file_to_upload ne '' ) {
        $res = Pheix::Tools::doFileUpload( $path_to_upload, "uploadfile",
            $tmp_file_name, 0, 0 );
        return ( $res, $tmp_file_name );
    } else {
        return ( qq~<div class="error">Файл не найден!</div>~, 0 );
    }
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# getDirsArray ( $currdir )
#-----------------------------------------------------------------------------------
#
# $currdir - каталог для поиска.
# Найти подкаталоги в каталоге.
# Функция возвращает массив c найденными подкаталогами.
#
#-----------------------------------------------------------------------------------

sub getDirsArray {
    my ($dir) = @_;
    my $dhndl;
    my @dirs;
    $dir =~ s/\\/\//g;
    if (-e $dir) {
        opendir $dhndl, $dir or die 'Unable to open directory ' . $dir . ': ' . $!;
        @dirs = grep { !/^\.+$/ and -d "$dir/$_" } readdir $dhndl;
        closedir $dhndl;
    }
    return @dirs;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# getFilesArray ( $dir )
#-----------------------------------------------------------------------------------
#
# $dir - каталог для поиска.
# Найти файлы в каталоге.
# Функция возвращает массив c найденными файлами.
#
#-----------------------------------------------------------------------------------

sub getFilesArray {
    my ($dir) = @_;
    my $dhndl;
    my @files;
    $dir =~ s/\\/\//g;
    if (-e $dir) {
        opendir $dhndl, $dir or die 'Unable to open directory ' . $dir . ': ' . $!;
        @files = grep { ( !/^\.+$/ ) and !( -d "$dir/$_" ) } readdir $dhndl;
        closedir $dhndl;
    }
    return @files;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# getModXml ( $tmp_dir )
#-----------------------------------------------------------------------------------
#
# $tmp_dir - каталог для поиска.
# Поиск файла описателя модуля в формате XML.
# Функция возвращает найденный файл описателя модуля или пустую строку,
# если файл описателя не найден.
#
#-----------------------------------------------------------------------------------

sub getModXml {
    my $tmp_dir = $_[0];
    my @files_  = &getFilesArray($tmp_dir);
    for my $fileinx (0..$#files_) {
        if ( $files_[$fileinx] =~ /\.xml$/ ) { return $files_[$fileinx]; }
    }
    return;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# getModXmlRecursion ( $tmp_dir )
#-----------------------------------------------------------------------------------
#
# $tmp_dir - каталог для поиска.
# Поиск файла описателя модуля в формате XML.
# Функция возвращает найденный файл описателя модуля или пустую строку,
# если файл описателя не найден. Выполняется рекурсивный обход каталога
#
#-----------------------------------------------------------------------------------

sub getModXmlRecursion {
    my ($dir) = @_;
    my $dhndl;
    my $xmlfile;
    if ( -e $dir ) {
        opendir $dhndl, $dir or die 'Unable to open directory ' . $dir . ': ' . $!;
        my @contents = map "$dir/$_", sort grep !/^\.\.?$/, readdir $dhndl;
        closedir $dhndl;
        foreach (@contents) {
            if ( !-l && -d ) {
                $xmlfile = getModXmlRecursion($_);
            }
            else {
                if ( $_ =~ /(\.xml)$/i ) {
                    $xmlfile = $_;
                    last;
                }
            }
        }
    }
    return $xmlfile;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# moveFilesFromTo ( $from_dir, $to_dir)
#-----------------------------------------------------------------------------------
#
# $from_dir - каталог, из которого выполняется перенос.
# $to_dir - каталог, в который выполняется перенос.
# Перенос всех файлов из каталога в каталог.
# Функция возвращает статус операции (1 - успех, 0 - отказ).
#
#-----------------------------------------------------------------------------------

sub moveFilesFromTo {
    my $mainreturn = 1;
    my $from_dir   = $_[0];
    my $to_dir     = $_[1];
    my @files_     = &getFilesArray($from_dir);
    if (@files_) {
        if ( !( -e $to_dir ) ) { mkpath($to_dir); }
        for ( my $fileinx = 0 ; $fileinx <= $#files_ ; $fileinx++ ) {
            #my $cmd = `mv -v $from_dir/$files_[$fileinx] $to_dir/$files_[$fileinx]`;
            #print "<h1>$cmd</h1>"
            my $res =
              move( "$from_dir/$files_[$fileinx]",
                "$to_dir/$files_[$fileinx]" );
            if ( !$res ) { $mainreturn = 0; }
        }
    }
    return $mainreturn;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# doInstallFileUpdate ( $action, $module_dir, $module_xml, $timestamp)
#-----------------------------------------------------------------------------------
#
# $action - действие (install или uninstall).
# $module_dir - каталог модуля.
# $module_xml - имя файла описателя модуля в формате XML.
# Добавление/удаление записи об установленном модуле в install.tnk.
# Функция возвращает статус операции (1 - успех, 0 - отказ).
#
#-----------------------------------------------------------------------------------

sub doInstallFileUpdate {
    my $action         = $_[0];
    my $install_file   = $INSTALL_DB_PATH;
    my $module_dir     = $_[1];
    my $module_xml     = $_[2];
    my $install_time   = $_[3];
    my $xml_descr_path = "$module_dir/$module_xml";

    if ( -e $install_file ) {
        open( my $inputFH, "<", $install_file );
        my @install_file = <$inputFH>;
        close($inputFH);

        if ( $action eq 'install' ) {
            my $install_date = Pheix::Tools::getDateUpdate();
            my $namemodule =
              &getConfigValue( $module_dir, $module_xml, "name" );

            my $already_installed = -1;

            for ( my $srcinx = 0 ; $srcinx <= $#install_file ; $srcinx++ ) {
                my @tmp = split( /\|/, $install_file[$srcinx] );
                if ( $tmp[2] eq $namemodule ) { $already_installed = 1 }
            }

            if ( $already_installed == -1 ) {
                push(
                    @install_file,
                    join( "|",
                        $install_time,   $install_date, $namemodule,
                        $xml_descr_path, 0, 0 )."\n"
                );
                open( my $outputFH, ">", $install_file );
                print $outputFH @install_file;
                close($outputFH);
                return 1;
            }
        }    #install
        if ( $action eq 'uninstall' ) {
            my $backup_id = 1;
            my @new_install_list = ();
            my $namemodule       = $module_xml;
            for ( my $srcinx = 0 ; $srcinx <= $#install_file ; $srcinx++ ) {
                my @tmp = split( /\|/, $install_file[$srcinx] );
                if ( $tmp[2] ne $namemodule ) {
                    push( @new_install_list, $install_file[$srcinx] );
                } else {
                    $backup_id = $tmp[0];
                }

                #print "<p>$tmp[2] ne $namemodule</p>";
            }
            open( my $outputFH, ">", $install_file );
            print $outputFH @new_install_list;
            close($outputFH);
            return $backup_id;
        }    #uninstall
    }
    return;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# doBackupCopy ( $action, $backup_workdir, $butime_stamp)
#-----------------------------------------------------------------------------------
#
# $action - указание части CMS для бэкапа (admin или client).
# $backup_workdir - каталог бэкапа.
# $butime_stamp - timestamp бэкапа.
# Создание резервной копии перед установкой.
# Функция возвращает статус операции (1 - успех) или пустую строку (отказ).
#
#-----------------------------------------------------------------------------------

sub doBackupCopy {
    my $action         = $_[0];
    my $backup_workdir = $_[1];
    my $butime_stamp   = $_[2];
    my $log_date       = localtime($butime_stamp);
    my $module_name    = $_[3];
    my $user_installed = $_[4];
    my @client_bu_list = ('user.pl');
    my @admin_bu_list  = ('admin.pl');
    my @work_bu        = ();

    if   ( $action eq 'client' ) {
        @work_bu = @client_bu_list;
    } else {
        @work_bu = @admin_bu_list;
    }

    if ( !-e "$backup_workdir" ) { mkdir($backup_workdir); }
    if ( !-e "$backup_workdir/$butime_stamp" ) {
        mkdir("$backup_workdir/$butime_stamp");
    }
    my $bures =
      copy( $work_bu[0], "$backup_workdir/$butime_stamp/$work_bu[0]" );
    if ($bures) {
        open( my $outputFH,
            ">", "$backup_workdir/$butime_stamp/" . $action . "backup.tnk" );
        print $outputFH "$butime_stamp|$log_date|$module_name|$user_installed\n";
        close($outputFH);
        return 1;
    }

    return;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# isModInstalled ( $module_name, $module_type )
#-----------------------------------------------------------------------------------
#
# $module_name - название модуля.
# $module_type - тип модуля (0 - admin, 1 - user).
# Проверка, что модуль уже установлен.
# Функция возвращает статус операции (1 - не устновлен) или пустую строку (установлен).
#
#-----------------------------------------------------------------------------------

sub isModInstalled {
    my $module_name = $_[0];
    my $module_type = $_[1];    # 0 - admin, 1 - user;
    if ( !$module_type ) { $module_type = 0; }
    my @installed_modules = &getInstalledModsFromPl($module_type);
    for (
        my $instmodinx = 0 ;
        $instmodinx <= $#installed_modules ;
        $instmodinx++
      )
    {
        my @tmp_install_array = split( /\,/, $installed_modules[$instmodinx] );
        if ( $tmp_install_array[2] eq $module_name ) { return; }
    }
    return 1;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# isVersionCompatible ( $module_version )
#-----------------------------------------------------------------------------------
#
# $module_version - версия модуля.
# $module_type - тип модуля (0 - admin, 1 - user).
# Проверка совместимости версий.
# Функция возвращает статус операции в виде строки (несовместимые версии модуля и CMS)
# или пустую строку (если версии модуля и CMS совместимы).
#
#-----------------------------------------------------------------------------------

sub isVersionCompatible {
    my $module_min_version = $BASE_VERSION;
    my $module_version     = $_[0];
    my @installed_modules  = &getInstalledModsFromConf();
    my $configmodule       = 'configmodule';
    for ( my $i = 0 ; $i <= $#installed_modules ; $i++ ) {
        if ( $installed_modules[$i] =~ /$configmodule$/ ) {
            my @parsed_path = &doConfigPathParse( $installed_modules[$i] );
            $module_min_version =
              getConfigValue( $parsed_path[0], $parsed_path[1], "version" );
            last;
        }
    }
    if ( $module_version == $module_min_version ) {
        return "cms=$module_min_version, module=$module_version";
    } else {
        return;
    }
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# doConfigPathParse ( $module_config_path )
#-----------------------------------------------------------------------------------
#
# $module_config_path - путь к файлу описателя модуля.
# Функция разбивает путь к файлу описателя модуля на <путь> и <файл.xml>.
# Функция возвращает массив ( <путь>, <файл.xml> ).
#
#-----------------------------------------------------------------------------------

sub doConfigPathParse {
    my $module_config_path = $_[0];
    my @path_parse         = split( /\//, $module_config_path );
    my $module_path        = join( "/", @path_parse[ 0 .. $#path_parse - 1 ] );
    my $module_file        = $path_parse[$#path_parse];
    return ( $module_path, $module_file );
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# doSyscall ( $command )
#-----------------------------------------------------------------------------------
#
# $command - команда ОС (системный вызов).
# Запуск системного вызова и возврат строк консоли (stdout или stderr).
# Функция возвращает стандартный вывод выолненной команды.
#
#-----------------------------------------------------------------------------------

sub doSyscall {
    my $command = $_[0];
    local *CATCHOUT = IO::File->new_tmpfile;
    local *CATCHERR = IO::File->new_tmpfile;
    my $pid = open3( gensym, ">&CATCHOUT", ">&CATCHERR", "$command" );
    waitpid( $pid, 0 );
    seek $_, 0, 0 for \*CATCHOUT, \*CATCHERR;
    my @command_stdout = <CATCHOUT>;
    my @command_stderr = <CATCHERR>;

    if   ( $_[1] == 'stderr' ) {
        return @command_stderr;
    } else {
        return @command_stdout;
    }
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# delFiles ( $from_dir )
#-----------------------------------------------------------------------------------
#
# $from_dir - путь к каталогу.
# Удаление всех файлов в каталоге.
# Функция возвращает статус операции (1 - успех, 0 - отказ).
#
#-----------------------------------------------------------------------------------

sub delFiles {
    my $mainreturn = 1;
    my $from_dir   = $_[0];
    my @files_     = &getFilesArray($from_dir);
    for ( my $fileinx = 0 ; $fileinx <= $#files_ ; $fileinx++ ) {
        my $res = unlink("$from_dir/$files_[$fileinx]");
        if ( !$res ) { $mainreturn = 0; }
    }
    return $mainreturn;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# delDir ( $from_dir )
#-----------------------------------------------------------------------------------
#
# $from_dir - путь к каталогу.
# Удаление каталога с файлами.
# Функция возвращает статус операции (1 - успех) или пустую строку (отказ).
#
#-----------------------------------------------------------------------------------

sub delDir {
    my $from_dir = $_[0];
    my $res      = &delFiles($from_dir);
    if ($res) {
        $res = rmdir($from_dir);
        if ($res) { return 1; }
    }
    return;
}

#-----------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# getInstalledModsTable ( $session_id, $crypted_login, $SHOW_MESS, $message )
#-----------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_login - зашифрованный логин пользователя.
# $SHOW_MESS - флаг показа сообщения (1 - показ, 0 - пусто).
# $message - сообщение
# Вывод таблицы с установленными модулями для главной страницы модуля "Конфигурация".
# Функция возвращает таблицу с установленными модулями в HTML формате.
#
#-----------------------------------------------------------------------------------

sub getInstalledModsTable {
    my ($sid, $cryptlgn, $printflag, $procmess) = @_;
    my @installed_modules = getInstalledModsFromPl();
    my $image_type;
    my $rc;
    if ( $procmess !~ /error/ ) {
        $rc = 1;
    } else {
        $rc = 0;
    }
    my $add_module_form = getInstallModForm( $sid );
    my $MODULES_content .= showUpdateHint($printflag, $rc, $procmess).qq~
        <div class="spoiler-wrapper" style="width:98%">
            <div class="spoiler folded">
                <a href="javascript:void(0);" class="spoilerlink">
                    Установить новый модуль в систему</a></div>
            <div class="spoiler-text">$add_module_form</div>
        </div>
        <script type="text/javascript">
               jQuery(document).ready(function(){
                   jQuery('.spoiler-text').hide()
                   jQuery('.spoiler').click(function(){
                       jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
                   })
               })
        </script>
        <table cellspacing="0" cellpadding="10" width="100%" class="tabselect">
    ~;
    for ( my $modinx = 0 ; $modinx <= $#installed_modules ; $modinx++ ) {
        my $counter          = $modinx + 1;
        my @module_sys_array = split( /\,/, $installed_modules[$modinx] );
        my $bgcolor          = 'class="tr01"';
        if ( $modinx % 2 == 0 ) { $bgcolor = 'class="tr02"'; }
        my $module_folder        = "$MODULES_PATH/$module_sys_array[2]";
        my $xml_description_file = &getModXml($module_folder);
        my $module_descr =
          &getConfigValue( $module_folder, $xml_description_file,
            "description" );
        my $module_version =
          &getConfigValue( $module_folder, $xml_description_file, "version" );
        my $module_path1 =
          &getConfigValue( $module_folder, $xml_description_file,
            "module_package" );
        my $module_path2 =
          &getConfigValue( $module_folder, $xml_description_file,
            "module_include" );
        my $module_path3 =
          &getConfigValue( $module_folder, $xml_description_file,
            "tankfile" );
        my @path_statuses = ('', '', '');

        my $settings_link = '';
        if (&checkIfSetupable($module_sys_array[2])) {
            $settings_link = qq~
            <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsetup&amp;name=$module_sys_array[2]"
               class="fa-action-link">
                    <i class="fa fa-cog"
                       title="Дополнительные настройки модуля"></i></a>~;
        }

        my $delete_link = qq~
        $settings_link
        <a href="javascript:Confirm('$ENV{SCRIPT_NAME}?id=$sid&amp;action=uninstallmodule&amp;name=$module_sys_array[2]','Вы уверены, что хотите удалить модуль &quot;$module_descr&quot; из системы?')"
           class="fa-action-link">
        <i class="fa fa-trash"
           title="Удалить модуль &quot;$module_descr&quot из системы" ></i></a>
        <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=powermodule&amp;name=$module_sys_array[2]"
           class="fa-action-link">
        <i class="fa fa-power-off"
           title="Включить/выключить модуль"></i></a>

        ~;
        my $image_type = qq~
            <img
                src="images/admin/cms/png/connect.png"
                title="Модуль подключен к системе"
                alt="Модуль подключен к системе">
        ~;
        if ( &getModPowerOn( $module_sys_array[2] ) ) {
            $image_type = qq~
                <img
                    src="images/admin/cms/png/disconnect.png"
                    title="Модуль выключен"
                    alt="Модуль выключен">
            ~;
            $module_descr = qq~
                <span class="poweroffmodule">$module_descr</span>
            ~;
        }
        if ( $module_sys_array[3] eq 'int' ) {
            $delete_link = qq~
                $settings_link
                <span class="fa-nonaction-span" title="Деинсталлировать встроенный модуль нельзя"><i class="fa fa-trash"></i></span>
            ~;
            if ($module_sys_array[2] eq $MODULE_NAME) {
                my $_dynasm = getSingleSetting($MODULE_NAME, 0, 'dynasitemap');
                if ($_dynasm != 1) {
                    $delete_link = qq~
                    <a href="javascript:doSesChckLoad('$sid', '$ENV{SCRIPT_NAME}', '&amp;action=admgeneratesm')"
                       class="fa-action-link">
                    <i class="fa fa-sitemap"
                       title="Сгенерировать статическую карту сайта"></i></a>
                    ~ . $delete_link;
                }
            }
        }
        if ( -e $module_path1 ) {
            $path_statuses[0] = "<div>$module_path1 (<span class='bluetext'>ok</span>)</div>";
        }
        if ( -e $module_path2 ) {
            $path_statuses[1] = "<div>$module_path2 (<span class='bluetext'>ok</span>)</div>";
        }
        if ( -e $module_path3 ) {
            $path_statuses[2] = "<div>$module_path3 (<span class='bluetext'>ok</span>)</div>";
        }
        my $actions_in_module =
          join( ", ", &getModActions( $module_sys_array[4] ) );
        if ( $actions_in_module eq '' ) {
            $actions_in_module = qq~
                <span class="error">действий не найдено!</span>
            ~;
        }
        my $EXTENDED_BLOCK = qq~
            <div style="border:1px dashed #DDDDDD; background-color:#F5F5F5;">
                <div style="background-color:#FFFFFF; padding:6px;">
                <a href="#"
                    style="text-decoration: none"
                    onclick="return spoiler_js(this);"
                    class="silverlink">
                <span style="">
                <img
                    style="margin-left:5px;"
                    border="0"
                    src="images/admin/cms/icon_plus.gif"
                    width="9"
                    height="9"
                    alt="">&nbsp;
                    Дополнительная информация об установленном модуле
                </span>
                <span style="display:none;">
                <img
                    style="margin-left:5px"
                    border="0"
                    src="images/admin/cms/icon_minus.gif"
                    width="9"
                    height="9"
                    alt="">&nbsp;
                    Дополнительная информация об установленном модуле
                </span>
                </a>
                </div>
                <div style="padding:6px; display: none;">
                    <p class="console_std">
                    require: $installed_modules[$modinx]<br>actions: $actions_in_module
                    </p>
                </div>
            </div>
        ~;
        $MODULES_content .= qq~
            <tr style="height:20px" $bgcolor>
                <td width="10%" valign="middle" align="center">
                    $image_type</td>
                <td width="70%" class="s_t">
                    <div style="font-size:11pt;"><b>
                    <span style="color:#C0C0C0">$counter.</span> $module_descr</b>
                    </div>
                    <div class=cmstext style="padding:10; font-size:10pt; color:#A0A0A0; line-height:1.4;">
                    Версия модуля: $module_version<br>
                    $path_statuses[0]
                    $path_statuses[1]
                    $path_statuses[2]</div>
                    $EXTENDED_BLOCK</td>
                <td width="20%" class="s_t" align="center">$delete_link</td>
            </tr>
        ~;
    }
    if ( $#installed_modules == -1 ) {
        $MODULES_content .= qq~
            <tr height="20">
                <td width="100%" valign="middle" align="left" class="error">
                Установленных модулей не найдено!
                </td>
            </tr>
        ~;
    }
    $MODULES_content .= "</table>";
    return $MODULES_content;
}

#-----------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
# doModInstall ( $uploadretval, $zip_filename, $cryptlogin )
#------------------------------------------------------------------------------------------
#
# $uploadretval - возвращаемое значение функции Pheix::ToolsdoFileUpload в HTML формате.
# $zip_filename - имя файла архива модуля (*.zip).
# $cryptlogin - зашифрованный логин пользователя.
# Основная функция установки нового модуля.
# Функция возвращает массив (<результат_инсталляции>, <протокол_инсталляции_в_HTML_формате>).
#
#------------------------------------------------------------------------------------------

sub doModInstall {
    my $already_installed = -1;
    my $adminreturn       = 0;
    my $clientreturn      = 0;
    my $mainreturn        = 0;
    my $CFG_content       = '';

    # USER DIRS #

    my $user_module_dir = $USER_MODULES_PATH;

    # ADMIN DIRS #

    my $module_name  = '';
    my $module_dir   = $MODULES_PATH;
    my $extract_dir  = "$MODULES_PATH/tmp";
    my $backup_dir   = $BACKUP_PATH;
    my $zip_filename = $_[1];
    my $htmlerror    = qq~<span class="error">ошибка!</span>~;
    my $htmlok       = qq~<span class="status">ок!</span>~;
    my $time_stamp   = time;
    if ( !-e $extract_dir ) { mkdir($extract_dir); }
    my $ae = Archive::Extract->new( archive => "$module_dir/$zip_filename" );

    if ($ae) {
        $CFG_content .= "<ul>\r\n";
        my $ok = $ae->extract( to => "$extract_dir" );
        if ($ok) {
            $CFG_content .= qq~
                <li class="s_t">$_[0]</li>
                <li class="s_t">
                $zip_filename &rarr; $extract_dir ... $htmlok
                </li>
            ~;
            $ok = unlink("$module_dir/$zip_filename");
            if ($ok) {
                $CFG_content .= qq~
                    <li class="s_t">
                    Удаляем $module_dir/$zip_filename ... $htmlok
                    </li>
                ~;
                $ok = &getModXmlRecursion("$extract_dir/admin");
                if ($ok) {
                    (my $sourcemoduledir, my $module_xml_file) = &doConfigPathParse($ok);
                    $CFG_content .= qq~
                        <li class="s_t">
                        Описатель модуля &rarr; $module_xml_file ... $htmlok
                        </li>
                    ~;
                    $module_name =
                      &getConfigValue( $sourcemoduledir, $module_xml_file,
                        "name" );
                    my $module_version =
                      &getConfigValue( $sourcemoduledir, $module_xml_file,
                        "version" );
                    $CFG_content .= qq~
                        <li class="s_t">
                        Информация о модуле:&nbsp;
                        <span class="installmarkup">
                            $module_name,
                            $module_version
                        </span>
                        </li>
                    ~;
                    $ok = &isVersionCompatible($module_version);
                    if ($ok) {
                        $CFG_content .= qq~
                            <li class="s_t">
                            Совместимость версий ($ok): $htmlok
                            </li>
                        ~;
                        $ok = &isModInstalled($module_name);
                        if ($ok) {
                            $ok = 1;
                            if ( !( -e "$module_dir/$module_name" ) ) {
                                $ok = mkdir("$module_dir/$module_name");
                            }
                            if ($ok) {
                                $CFG_content .= qq~
                                    <li class="s_t">
                                    Создаем $module_dir/$module_name: $htmlok
                                    </li>
                                ~;
                                $ok = &moveFilesFromTo(
                                    "$extract_dir/admin/$module_name",
                                    "$module_dir/$module_name"
                                );
                                if ($ok) {
                                    $CFG_content .= qq~
                                        <li class="s_t">
                                        Перемещаем $extract_dir/admin/$module_name в $module_dir/$module_name: $htmlok
                                        </li>
                                    ~;
                                    $ok = rmdir("$extract_dir/admin/$module_name");
                                    if ($ok) {
                                        $CFG_content .= qq~
                                            <li class="s_t">
                                            Удаляем $extract_dir/admin: $htmlok
                                            </li>
                                        ~;
                                        $ok = &doCodeUpdateAftrInstall(
                                            "$module_dir/$module_name",
                                            $module_xml_file, 1 );
                                        if ( $ok == 1 ) {
                                            $CFG_content .= qq~
                                                <li class="s_t">
                                                Проверяем синтаксис: $htmlok
                                                </li>
                                            ~;
                                            $ok =
                                              &doBackupCopy( 'admin',
                                                $backup_dir, $time_stamp,
                                                $module_name, $_[2] );
                                            if ($ok) {
                                                $CFG_content .= qq~
                                                    <li class="s_t">
                                                    Делаем резервную копию: $htmlok
                                                    </li>
                                                ~;
                                                $ok = &doCodeUpdateAftrInstall(
                                                    "$module_dir/$module_name",
                                                    $module_xml_file,
                                                    0,
                                                    $time_stamp
                                                );
                                                if ($ok) {
                                                    $CFG_content .= qq~
                                                        <li class="s_t">
                                                        Обновляем исходный код: $htmlok
                                                        </li>
                                                    ~;
                                                    $ok = &doInstallFileUpdate(
                                                        'install',
                                                        "$module_dir/$module_name",
                                                        $module_xml_file,
                                                        $time_stamp
                                                    );
                                                    if ($ok) {
                                                        $CFG_content .= qq~
                                                            <li class="s_t">
                                                            Обновляем инсталляционный файл install.tnk: $htmlok
                                                            </li>
                                                        ~;
                                                    }
                                                    else {
                                                        $CFG_content .= qq~
                                                            <li class="s_t">
                                                            Обновляем инсталляционный файл install.tnk: $htmlerror
                                                            </li>
                                                        ~;
                                                        $mainreturn  = -1;
                                                        $adminreturn = -1;
                                                    }
                                                }
                                                else {
                                                    $CFG_content .= qq~
                                                        <li class="s_t">
                                                        Обновляем исходный код: $htmlerror
                                                        </li>
                                                    ~;
                                                    $mainreturn  = -1;
                                                    $adminreturn = -1;
                                                }
                                            }
                                            else {
                                                $CFG_content .= qq~
                                                    <li class="s_t">
                                                    Делаем резервную копию $htmlerror
                                                    </li>
                                                ~;
                                                $mainreturn  = -1;
                                                $adminreturn = -1;
                                            }
                                        }
                                        else {
                                            $CFG_content .= qq~
                                                <li class="s_t">
                                                Проверяем синтаксис ("$module_dir/$module_name"):  $htmlerror$ok
                                                </li>
                                            ~;
                                            $mainreturn  = -1;
                                            $adminreturn = -1;
                                        }
                                    }
                                    else {
                                        $CFG_content .= qq~
                                            <li class="s_t">
                                            Удаляем $extract_dir/admin: $htmlerror
                                            </li>
                                        ~;
                                        $mainreturn  = -1;
                                        $adminreturn = -1;
                                    }
                                }
                                else {
                                    $CFG_content .= qq~
                                        <li class="s_t">
                                        Перемещаем $extract_dir/admin &rarr; $module_dir/$module_name: $htmlerror
                                        </li>
                                    ~;
                                    $mainreturn  = -1;
                                    $adminreturn = -1;
                                }
                            }
                            else {
                                $CFG_content .= qq~
                                    <li class="s_t">
                                    Создаем admin-каталог: $htmlerror [$!]
                                    </li>~;
                                $mainreturn  = -1;
                                $adminreturn = -1;
                            }
                        }
                        else {
                            $CFG_content .= qq~
                                <li class="s_t">
                                <span class="installmarkup">
                                    Модуль $module_name уже установлен
                                </span>
                                </li>
                            ~;
                            $mainreturn        = -1;
                            $adminreturn       = -1;
                            $already_installed = 0;
                        }
                    }
                    else {
                        $CFG_content .= qq~
                            <li class="s_t">
                            <span class="installmarkup">
                                Несовместимая версия модуля
                            </span>
                            </li>
                        ~;
                        $mainreturn  = -1;
                        $adminreturn = -1;
                    }

               # Установка клиентской части модуля

                    if ( $adminreturn > -1 ) {
                        my $userok =
                          &getModXmlRecursion("$extract_dir/client");
                        if ($userok) {
                            (my $sourcemoduledir, my $module_xml_file) = &doConfigPathParse($userok);
                            $CFG_content .= qq~
                                <li class="s_t">
                                Описатель пользовательской части модуля &rarr; $module_xml_file ... $htmlok
                                </li>
                            ~;
                            $module_name =
                              &getConfigValue( $sourcemoduledir,
                                $module_xml_file, "name" );
                            my $module_version =
                              &getConfigValue( $sourcemoduledir,
                                $module_xml_file, "version" );
                            $CFG_content .= qq~
                                <li class="s_t">
                                Информация о пользовательской части модуля:&nbsp;
                                <span class="installmarkup">
                                    $module_name, $module_version
                                </span>
                                </li>
                            ~;
                            $userok = &isModInstalled( $module_name, 1 );
                            if ($userok) {
                                $userok = 1;
                                if ( !( -e "$user_module_dir/$module_name" ) ) {
                                    $userok =
                                      mkdir("$user_module_dir/$module_name");
                                }
                                if ($userok) {
                                    $CFG_content .= qq~
                                        <li class="s_t">
                                        Создаем каталог $user_module_dir/$module_name: $htmlok
                                        </li>
                                    ~;
                                    $userok = &moveFilesFromTo(
                                        "$extract_dir/client/$module_name",
                                        "$user_module_dir/$module_name" );
                                    if ($userok) {
                                        $CFG_content .= qq~
                                            <li class="s_t">
                                            Перемещаем $extract_dir/client/$module_name &rarr; $user_module_dir/$module_name: $htmlok
                                            </li>
                                        ~;
                                        $userok = rmdir("$extract_dir/client/$module_name");
                                        if ($userok) {
                                            $CFG_content .= qq~
                                                <li class="s_t">
                                                Удаляем $extract_dir/client: $htmlok
                                                </li>
                                            ~;
                                            $userok = &doCodeUpdateAftrInstall(
                                                "$user_module_dir/$module_name",
                                                $module_xml_file, 1
                                            );
                                            if ( $userok == 1 ) {
                                                $CFG_content .= qq~
                                                    <li class="s_t">
                                                    Проверяем синтаксис пользовательской части модуля: $htmlok
                                                    </li>
                                                ~;
                                                $userok = &doBackupCopy(
                                                    'client',    $backup_dir,
                                                    $time_stamp, $module_name,
                                                    $_[2]
                                                );
                                                if ($userok) {
                                                    $CFG_content .= qq~
                                                        <li class="s_t">
                                                        Делаем резервную копию ($time_stamp): $htmlok
                                                        </li>
                                                    ~;
                                                    $userok =
                                                      &doCodeUpdateAftrInstall(
                                                        "$user_module_dir/$module_name",
                                                        $module_xml_file,
                                                        0,
                                                        $time_stamp
                                                      );
                                                    if ($userok) {
                                                        $CFG_content .= qq~
                                                            <li class="s_t">
                                                            Обновляем исходный код пользовательской части модуля ($time_stamp): $htmlok
                                                            </li>
                                                        ~;
                                                    } else {
                                                        $CFG_content .= qq~
                                                            <li class="s_t">
                                                            Обновляем исходный код пользовательской части модуля: $htmlerror
                                                            </li>
                                                        ~;
                                                        $mainreturn   = -1;
                                                        $clientreturn = -1;
                                                    }
                                                } else {
                                                    $CFG_content .= qq~
                                                        <li class="s_t">
                                                        Делаем резервную копию $htmlerror
                                                        </li>
                                                    ~;
                                                    $mainreturn   = -1;
                                                    $clientreturn = -1;
                                                }
                                            } else {
                                                $CFG_content .= qq~
                                                    <li class="s_t">
                                                    Проверяем синтаксис пользовательской части модуля:  $htmlerror$userok
                                                    </li>
                                                ~;
                                                $mainreturn   = -1;
                                                $clientreturn = -1;
                                            }
                                        } else {
                                            $CFG_content .= qq~
                                                <li class="s_t">
                                                Удаляем $extract_dir/client: $htmlerror
                                                </li>
                                            ~;
                                            $mainreturn   = -1;
                                            $clientreturn = -1;
                                        }
                                    } else {
                                        $CFG_content .= qq~
                                            <li class="s_t">
                                            Перемещаем $extract_dir/client &rarr; $user_module_dir/$module_name: $htmlerror
                                            </li>
                                        ~;
                                        $mainreturn   = -1;
                                        $clientreturn = -1;
                                    }
                                } else {
                                    $CFG_content .= qq~
                                        <li class="s_t">
                                        Создаем каталог $user_module_dir/$module_name: $htmlerror [$!]
                                        </li>
                                    ~;
                                    $mainreturn   = -1;
                                    $clientreturn = -1;
                                }
                            } else {
                                $CFG_content .= qq~
                                    <li class="s_t">
                                    <span class="installmarkup">
                                    Модуль $module_name уже установлен
                                    </span></li>
                                ~;
                                $mainreturn        = -1;
                                $clientreturn      = -1;
                                $already_installed = 0;
                            }
                        } else {
                            $CFG_content .= qq~
                                <li class="s_t">
                                Описатель пользовательской части модуля ... $htmlerror
                                </li>
                            ~;
                            $mainreturn   = -1;
                            $clientreturn = -1;
                        }
                    } else {
                        $CFG_content .= qq~
                            <li class="s_t">
                            Установка административной части модуля ... $htmlerror
                            </li>
                        ~;
                        $clientreturn = -1;
                    }

               # Установка клиентской части модуля

                } else {
                    $CFG_content .= qq~
                        <li class="s_t">
                        Находим XML описатель модуля ... $htmlerror
                        </li>
                    ~;
                    $mainreturn = -1;
                }
            } else {
                $CFG_content .= qq~
                    <li class="s_t">
                    Удаляем $module_dir/$zip_filename ... $htmlerror
                    </li>
                ~;
                $mainreturn = -1;
            }
        } else {
            $CFG_content .= qq~
                <li class="s_t">$_[0]</li>
                <li class="s_t">
                $zip_filename &rarr; $extract_dir ... $htmlerror
                </li>
            ~;
            $mainreturn = -1;
        }
        $CFG_content .= "</ul>\r\n";
    }
    else {
        $CFG_content = qq~
            <span class="error">
            Ошибка при обработке $module_dir/$zip_filename (\$ae undefined)
            </span>
        ~;
        $mainreturn = -1;
    }
    if ( $mainreturn == 0 ) {
        open( my $outputFH, ">", "$backup_dir/$time_stamp/install.log" );
        print $outputFH $CFG_content;
        close($outputFH);
    }
    else {
        &delFiles("$extract_dir/admin/");   # для совместимости с ранними модулями (<2.1)
        &delFiles("$extract_dir/client/");  # для совместимости с ранними модулями (<2.1)
        &delFiles("$extract_dir/client/$module_name");
        &delFiles("$extract_dir/admin//$module_name");
        if ( -e "$extract_dir/client/$module_name" ) { rmdir("$extract_dir/client/$module_name"); }
        if ( -e "$extract_dir/admin/$module_name" )  { rmdir("$extract_dir/admin/$module_name"); }
        if ( $already_installed == -1 ) {
            if ( $clientreturn == -1 ) {
                &doInstallFileUpdate( 'uninstall', "$module_dir/$module_name",
                    $module_name, 0);
                if ( $module_name ne '' ) {
                    &delFiles("$user_module_dir/$module_name");
                }
                if ( $module_name ne '' ) {
                    &delFiles("$module_dir/$module_name");
                }
                if ( -e "$backup_dir/$time_stamp/user.pl" ) {
                    copy( "$backup_dir/$time_stamp/user.pl", "user.pl" );
                }
                if ( -e "$backup_dir/$time_stamp/admin.pl" ) {
                    copy( "$backup_dir/$time_stamp/admin.pl", "admin.pl" );
                }
            }
            else {
                if ( $adminreturn == -1 ) {
                    if ( $module_name ne '' ) {
                        &delFiles("$module_dir/$module_name");
                    }
                    if ( -e "$backup_dir/$time_stamp/admin.pl" ) {
                        copy( "$backup_dir/$time_stamp/admin.pl", "admin.pl" );
                    }
                }
            }
        }
    }
    return ( $mainreturn, $CFG_content );
}

#-----------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
# doCodeUpdateAftrInstall ( $module_dir, $module_xml, $check_syntax_flag, $backup_id )
#---------------------------------------------------------------------------------------------------
#
# $module_dir - каталог модуля.
# $module_xml - файл описателя модуля.
# $check_syntax_flag - флаг проверки синтаксиса (0 - только проверка синтаксиса временного
# файла _admin.pl или _user.pl; конечные изменения вносятся, если значение флага = 1).
# $backup_id - timestamp идентификатор бэкапа.
# Обновление исходного кода главного файла.
# Функция возвращает статус операции (1 - синтаксис корректный) или сообщение об ошибке в HTML формате.
#
#---------------------------------------------------------------------------------------------------

sub doCodeUpdateAftrInstall {
    my $module_dir        = $_[0];
    my $module_xml        = $_[1];
    my $check_syntax_flag = $_[2];
    my $backup_id         = $_[3];
    my @src_code          = ();
    my $workfile          = '';
    my $error             = '';

    my $error_template = qq~
                        <div class="divinstallerr">
                               <div style="background-color:#FFFFFF; padding:6px;">
                               <a
                                   href="#"
                                   style="text-decoration: none"
                                   onclick="return spoiler_js(this);"
                                   class="silverlink">
                               <span style="">
                                   <img
                                       style="margin-left:5px;"
                                       border="0"
                                       src="images/admin/cms/icon_plus.gif"
                                       width="9"
                                       height="9"
                                       alt="">
                                       &nbsp;Дополнительная информация об ошибках
                               </span>
                               <span style="display:none;">
                                   <img
                                       style="margin-left:5px"
                                       border="0"
                                       src="images/admin/cms/icon_minus.gif"
                                       width="9"
                                       height="9"
                                       alt="">
                                   &nbsp;Дополнительная информация об ошибках
                          </span>
                          </a>
                          </div>
                              <div style="padding:6px; display: none;">
                            %error%
                          </div>
                        </div>
    ~;

    if ( $module_dir =~ /^libs\/modules/ ) {
        open( my $inputFH, "<", "user.pl" );
        @src_code = <$inputFH>;
        close($inputFH);
        $workfile = "user.pl";
    } else {
        open( my $inputFH, "<", "admin.pl" );
        @src_code = <$inputFH>;
        close($inputFH);
        $workfile = "admin.pl";
    }

    # Формируем верхний require, format:
    # (%INSTALLED_MODULE%,r--,configmodule,int,%INSTALLED_XXX_PROCEDURES%)

    my $namemodule = &getConfigValue( $module_dir, $module_xml, "name" );
    my $packagepath =
      &getConfigValue( $module_dir, $module_xml, "module_package" );
    my $use_identifier =
      &getConfigValue( $module_dir, $module_xml, "use_identifier" );
    my $includepath =
      &getConfigValue( $module_dir, $module_xml, "module_include" );
    my $require_comment =
        " # (%INSTALLED_MODULE%,rwx,$namemodule,ext,%INSTALLED_"
      . uc($namemodule)
      . "_PROCEDURES%,$backup_id) #";
    if ( -e $packagepath ) {
        my $requre_string = qq~use $use_identifier;~ . $require_comment;
        if ( -e $includepath ) {

            # Формируем include код

            open( my $inputFH, "<", $includepath );
            my $incsource =
                "\n# (%INSTALLED_"
              . uc($namemodule)
              . "_PROCEDURES%) #\n"
              . join( "", <$inputFH> )
              . "\n# (%INSTALLED_"
              . uc($namemodule)
              . "_PROCEDURES%) #\n";
            close($inputFH);

            # Добавляем код - here we go!

            my $require_section_index = -1;
            my $proc_section_index    = -1;

            for ( my $srcinx = 0 ; $srcinx <= $#src_code ; $srcinx++ ) {
                if ( $src_code[$srcinx] =~ /\# DO NOT EDIT REQUIRE SECTION/ ) {
                    $require_section_index = $srcinx;
                }
                if ( $src_code[$srcinx] =~ /\# DO NOT EDIT PROCEDURE SECTION/ ){
                    $proc_section_index = $srcinx;
                }
            }

            if ( $require_section_index > -1 ) {
                my @modified_source_code = (
                    @src_code[ 0 .. $require_section_index ],
                    "\n$requre_string",
                    @src_code[ $require_section_index +
                      1 .. $proc_section_index ],
                    "\n$incsource",
                    @src_code[ $proc_section_index + 1 .. $#src_code ]
                );
                open( my $outputFH, ">", "_$workfile" );
                print $outputFH @modified_source_code;
                close($outputFH);
                my $check_syntax1 = join(
                    "",
                    &doSyscall(
                        "perl -c _$workfile", 'stderr'
                    )
                );
                my $check_syntax2 = join(
                    "",
                    &doSyscall(
                        "perl -c $packagepath", 'stderr'
                    )
                );

                if (   ( $check_syntax1 =~ /syntax OK/ )
                    && ( $check_syntax2 =~ /syntax OK/ ) )
                {
                    my $res = unlink("_$workfile");
                    if ($res) {
                        if ( $check_syntax_flag == 0 ) {
                            open( my $outputFH, ">", $workfile );
                            print $outputFH @modified_source_code;
                            close($outputFH);
                            return 1;
                        }
                        else { return 1; }
                    }
                } else {
                    $check_syntax1 =~ s/[\r\n]+/<br>/g;
                    $check_syntax2 =~ s/[\r\n]+/<br>/g;
                    $error = qq~
                               <p class="console_std">
                                   <font color="#FF0000">
                                   perl -c _$workfile</font>:<br>
                                   $check_syntax1
                               </p>
                               <p class="console_std">
                                   <font color="#FF0000">
                                   perl -c $packagepath</font>:<br>
                                   $check_syntax2</p>
                    ~;
                }
            } else {
                $error = "doCodeUpdateAftrInstall(): <b>\$require_section_index > -1</b> fails!<br>\$require_section_index = $require_section_index";
            }
        } else {
            $error = "doCodeUpdateAftrInstall(): <b>-e \$includepath</b> fails<br>\$includepath = $includepath";
        }
    } else {
        $error = "doCodeUpdateAftrInstall(): <b>-e \$packagepath</b> fails<br>\$packagepath = $packagepath";
    }
    $error_template =~ s/%error%/$error/g;
    return $error_template;
}

#-----------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------
# doModUninstall ( $module_name )
#------------------------------------------------------------------------------------------------
#
# $module_name - название модуля.
# Основная функция деинсталляции модуля.
# Функция возвращает статус операции (0 - успех, -1 - отказ) или сообщение об ошибке в HTML формате.
#
#------------------------------------------------------------------------------------------------

sub doModUninstall {
    my $backup_id         = '';
    my $mainreturn        = 0;
    my $UNINSTCFG_content = '';
    my $module_dir        = $MODULES_PATH;
    my $user_module_dir   = $USER_MODULES_PATH;
    my $module_name       = $_[0];
    my $htmlerror         = qq~<span class="error">ошибка!</span>~;
    my $htmlok            = qq~<span class="status">ок!</span>~;
    my @installed_modules = &getInstalledModsFromPl();
    $UNINSTCFG_content .= "<ul>";
    my $ok =
      &doInstallFileUpdate( 'uninstall', "$module_dir/$module_name",
        $module_name, 0 );
    if ($ok) {
        $UNINSTCFG_content .= qq~
            <li class="s_t">
            Обновляем инсталляционный файл install.tnk: $htmlok
            </li>
        ~;
        $ok =
          &doCodeUpdateAftrUninstall( "$module_dir/$module_name",
            $module_name, 1 );
        if ($ok) {
            $UNINSTCFG_content .= qq~
                <li class="s_t">
                Проверяем синтаксис: $htmlok
                </li>
            ~;
            for ( my $srcinx = 0 ; $srcinx <= $#installed_modules ; $srcinx++ )
            {
                my @tmp = split( /\,/, $installed_modules[$srcinx] );
                if ( $tmp[2] eq $module_name ) {
                    $backup_id = $tmp[$#tmp];
                    last;
                }
            }
            $ok =
              &doCodeUpdateAftrUninstall( "$module_dir/$module_name",
                $module_name, 0 );
            if ( $ok == 1 ) {
                $UNINSTCFG_content .= qq~
                    <li class="s_t">
                    Обновляем исходный код: $htmlok
                    </li>
                ~;
                $ok = &delFiles("$module_dir/$module_name");
                if ($ok) {
                    $UNINSTCFG_content .= qq~
                        <li class="s_t">
                        Удаляем системные файлы модуля: $htmlok
                        </li>
                    ~;
                    $ok = rmdir("$module_dir/$module_name");
                    if ($ok) {
                        $UNINSTCFG_content .= qq~
                            <li class="s_t">
                            Удаляем $module_dir/$module_name: $htmlok
                            </li>
                        ~;
                    } else {
                        $UNINSTCFG_content .= qq~
                            <li class="s_t">
                            Удаляем $module_dir/$module_name: $htmlerror
                            </li>
                        ~;
                        $mainreturn = -1;
                    }
                } else {
                    $UNINSTCFG_content .= qq~
                        <li class="s_t">
                        Удаляем системные файлы модуля: $htmlerror
                        </li>
                    ~;
                    $mainreturn = -1;
                }
            } else {
                $UNINSTCFG_content .= qq~
                    <li class="s_t">
                    Обновляем исходный код: $htmlerror$ok
                    </li>
                ~;
                $mainreturn = -1;
            }
        } else {
            $UNINSTCFG_content .= qq~
                <li class="s_t">
                Проверяем синтаксис:  $htmlerror
                </li>
            ~;
            $mainreturn = -1;
        }
    } else {
        $UNINSTCFG_content .= qq~
            <li class="s_t">
            Обновляем инсталляционный файл install.tnk: $htmlerror
            </li>
        ~;
        $mainreturn = -1;
    }

    # Деинсталляция клиентской части модуля

    @installed_modules = &getInstalledModsFromPl(1);

    $ok = &doCodeUpdateAftrUninstall( "$user_module_dir/$module_name",
        $module_name, 1 );

    if ($ok) {
        $UNINSTCFG_content .= qq~
            <li class="s_t">
            Проверяем синтаксис пользовательского модуля: $htmlok
            </li>
        ~;
        for ( my $srcinx = 0 ; $srcinx <= $#installed_modules ; $srcinx++ ) {
            my @tmp = split( /\,/, $installed_modules[$srcinx] );
            if ( $tmp[2] eq $module_name ) { $backup_id = $tmp[$#tmp]; last; }
        }
        $ok = &doCodeUpdateAftrUninstall( "$user_module_dir/$module_name",
            $module_name, 0 );
        if ($ok) {
            $UNINSTCFG_content .= qq~
                <li class="s_t">
                Обновляем исходный код пользовательского модуля: $htmlok
                </li>
            ~;
            $ok = &delFiles("$user_module_dir/$module_name");
            if ($ok) {
                $UNINSTCFG_content .= qq~
                    <li class="s_t">
                    Удаляем системные файлы пользовательского модуля: $htmlok
                    </li>
                ~;
                $ok = rmdir("$user_module_dir/$module_name");
                if ($ok) {
                    my $bak_path = "$BACKUP_PATH/$backup_id";
                    $UNINSTCFG_content .= qq~
                        <li class="s_t">
                        Удаляем $user_module_dir/$module_name: $htmlok</li>~;
                    $ok = &delDir($bak_path);
                    if ($ok) {
                        $UNINSTCFG_content .= qq~
                            <li class="s_t">
                            Удаляем резервную копию $bak_path: $htmlok
                            </li>
                        ~;
                    }
                    else {
                        $UNINSTCFG_content .= qq~
                            <li class="s_t">
                            Удаляем резервную копию $bak_path: $htmlerror
                            </li>
                         ~;
                        $mainreturn = -1;
                    }
                }
                else {
                    $UNINSTCFG_content .= qq~
                        <li class="s_t">
                        Удаляем $module_dir/$module_name: $htmlerror
                        </li>
                     ~;
                    $mainreturn = -1;
                }
            }
            else {
                $UNINSTCFG_content .= qq~
                    <li class="s_t">
                    Удаляем файлы пользовательского модуля: $htmlerror
                    </li>
                ~;
                $mainreturn = -1;
            }
        }
        else {
            $UNINSTCFG_content .= qq~
                <li class="s_t">
                Обновляем исходный код пользовательского модуля: $htmlerror
                </li>
            ~;
            $mainreturn = -1;
        }
    }
    else {
        $UNINSTCFG_content .= qq~
            <li class="s_t">
            Проверяем синтаксис пользовательского модуля: $htmlerror
            </li>
        ~;
        $mainreturn = -1;
    }

    # Деинсталляция клиентской части модуля

    $UNINSTCFG_content .= "</ul>";
    return ( $mainreturn, $UNINSTCFG_content );
}

#-----------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# doCodeUpdateAftrUninstall ( $module_name, $module_xml, $check_syntax_flag )
#----------------------------------------------------------------------------------------
#
# $module_dir - каталог модуля.
# $module_xml - файл описателя модуля.
# $check_syntax_flag - флаг проверки синтаксиса (0 - только проверка синтаксиса временного
# файла _admin.pl или _user.pl; конечные изменения вносятся, если значение флага = 1).
# Обновление исходного кода главного файла.
# Функция возвращает статус операции (1 - успех) или пустую строку.
#
#----------------------------------------------------------------------------------------

sub doCodeUpdateAftrUninstall {
    my $require_section   = '';
    my $proc_section      = '';
    my $module_dir        = $_[0];
    my $module_name       = $_[1];
    my $module_xml        = $_[1] . ".xml";
    my $check_syntax_flag = $_[2];
    my @src_code          = ();
    my $workfile          = '';
    my @installed_modules = ();

    if ( $module_dir =~ /^libs\/modules/ ) {
        $workfile          = "user.pl";
        open( my $inputFH, "<", $workfile );
        @src_code = <$inputFH>;
        close($inputFH);
        @installed_modules = &getInstalledModsFromPl(1);
    } else {
        $workfile          = "admin.pl";
        open( my $inputFH, "<", $workfile );
        @src_code = <$inputFH>;
        close($inputFH);
        @installed_modules = &getInstalledModsFromPl();
    }

    for (
        my $installinx = 0 ;
        $installinx <= $#installed_modules ;
        $installinx++
      )
    {
        my @tmp = split( /\,/, $installed_modules[$installinx] );
        if ( $tmp[2] eq $module_name ) {
            $require_section = $installed_modules[$installinx];
            $proc_section    = $tmp[4];
        }
    }

    # Формируем верхний require, format:
    # (%INSTALLED_MODULE%,r--,configmodule,int,%INSTALLED_XXX_PROCEDURES%)

    if ( ( $require_section ne '' ) && ( $proc_section ne '' ) ) {
        my $require_section_index = -1;
        my $proc_section_index_1  = -1;
        my $proc_section_index_2  = -1;
        for ( my $srcinx = 0 ; $srcinx <= $#src_code ; $srcinx++ ) {
            if ( $src_code[$srcinx] =~ /$require_section/ ) {
                $require_section_index = $srcinx;
            }
            if (   ( $src_code[$srcinx] =~ /\# \($proc_section\) \#/ )
                && ( $proc_section_index_1 != -1 )
                && ( $proc_section_index_2 == -1 ) )
            {
                $proc_section_index_2 = $srcinx;
            }
            if (   ( $src_code[$srcinx] =~ /\# \($proc_section\) \#/ )
                && ( $proc_section_index_1 == -1 )
                && ( $proc_section_index_2 == -1 ) )
            {
                $proc_section_index_1 = $srcinx;
            }
        }
        if (   ( $require_section_index > -1 )
            && ( $proc_section_index_1 > $require_section_index )
            && ( $proc_section_index_2 > $proc_section_index_1 ) )
        {
            my @modified_source_code = (
                @src_code[ 0 .. $require_section_index - 1 ],
                @src_code[ $require_section_index +
                  1 .. $proc_section_index_1 -
                  2 ],
                @src_code[ $proc_section_index_2 + 2 .. $#src_code ]
            );
            open( my $outputFH, ">", "_$workfile" );
            print $outputFH @modified_source_code;
            close($outputFH);
            my $check_syntax1 = join( "",
                &doSyscall( "perl -c _$workfile", 'stderr' )
            );

            if ( ( $check_syntax1 =~ /syntax OK/ ) ) {
                my $res = unlink("_$workfile");
                if ($res) {
                    if ( $check_syntax_flag == 0 ) {
                        open( my $outputFH, ">", $workfile );
                        print $outputFH @modified_source_code;
                        close($outputFH);
                        return 1;
                    } else { return 1; }
                }
            }

            open( $outputFH, ">", $workfile );
            print $outputFH @modified_source_code;
            close($outputFH);
            return 1;
        }
    }
    return;
}

#-----------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getUninstallRes ( $session_id, $crypted_login, $SHOW_MESS, $message )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_login - зашифрованный логин пользователя.
# $SHOW_MESS - флаг показа сообщения (1 - показ, 0 - пусто).
# $message - сообщение
# Вывод результатов деинсталляции модуля.
# Функция возвращает содержимое страницы в формате HTML.
#
#----------------------------------------------------------------------------------------

sub getUninstallRes {
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file, "description" );
    my $ipdatetime = '';
    if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
    my $information = qq~
        <div class=status>
            $ipdatetime Модуль успешно деинсталлирован!
        </div><br>\r\n
    ~;
    if ( $_[2] == -1 ) {
        $information = qq~
            <div class=error>
                $ipdatetime Ошибка при деинсталляции модуля!
            </div><br>\r\n
        ~;
    }
    my $CFGCNT_content = qq~
        <div class=admin_header>$module_descr</div>
        $information
        $_[3]
    ~;
    return $CFGCNT_content;
}

#-----------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getUploadRes ( $session_id, $crypted_login, $SHOW_MESS, $message )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_login - зашифрованный логин пользователя.
# $SHOW_MESS - флаг показа сообщения (1 - показ, 0 - пусто).
# $message - сообщение
# Вывод результатов инсталляции модуля.
# Функция возвращает содержимое страницы в формате HTML.
#
#----------------------------------------------------------------------------------------

sub getUploadRes {
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file, "description" );
    my $ipdatetime = '';
    if ( localtime =~ /([\d]+:[\d]+:[\d]+)/ ) { $ipdatetime = "$1:"; }
    my $information = qq~
        <div class=status>$ipdatetime Модуль успешно установлен!</div><br>\r\n
        ~;
    if ( $_[2] == -1 ) {
        $information = qq~
            <div class=error>
                $ipdatetime Ошибка при установке модуля!
            </div><br>\r\n
            ~;
        }
    my $CFGCNT_content = qq~
        <div class=admin_header>$module_descr</div>
        $information
        $_[3]
        ~;
    return $CFGCNT_content;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getInstallModForm ( $session_id )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# Вывод формы установки нового модуля.
# Функция возвращает HTML код формы установки нового модуля.
#
#----------------------------------------------------------------------------------------

sub getInstallModForm() {
    my $installModuleFormHtml = qq~
        <table cellspacing="2" cellpadding="0" width="100%">
        <tr><td align="right" valign="bottom">&nbsp;</td></tr>
        <tr><td valign="bottom">
        <form
            method="post"
            action="$ENV{SCRIPT_NAME}?id=$_[0]&amp;action=installmodule"
            enctype="multipart/form-data">
         <table
            cellspacing="2"
            cellpadding="9"
            width="100%"
            class="messagelist"
            style="background-image:url('images/social_bg.png');height:94">
         <tr>
            <td
                width="100%"
                class="s_t"
                style="font-family:Arial,Tahoma;color:#505050;font-size:12pt;">
                <table cellspacing="0" cellpadding="0" width="205">
                <tr>
                    <td width="160" class="inst_mod_head">
                        Установка&nbsp;нового&nbsp;модуля&nbsp;[</td>
                    <td width="25">
                        <div class="pheix-upload-icon">
                           <i class="fa fa-file-archive-o"
                              aria-hidden="true" title=".zip"></i>
                        </div>
                    </td>
                    <td width="20" class="inst_mod_head">]&nbsp;:</td>
                </tr></table>
            </td></tr>
            <tr><td width="100%">
            <div class="f_imulation_wrap">
                <div class="im_input">
                    <input type="text"value="Выберите файл модуля"/>
                </div>
                <input
                    type="file"
                    required
                    size=30
                    name="uploadfile"
                    id="imulated"
                    style="margin-left:-124px;margin-top:7px;height:20px"/>
            </div>
            <script type="text/javascript">
                jQuery('.im_input input').click(function(){
                    jQuery('.im_input input').css('color','#C0C0C0');
                    jQuery('.im_input input').val('Выберите файл модуля');
                    jQuery("#imulated").val('');
                    jQuery('#imulated').trigger('click');
                });
                jQuery('#imulated').change(function(){
                    jQuery('.im_input input').css('color','#505050');
                    jQuery('.im_input input').val(jQuery(this).val());
                });
            </script>
            </td></tr>
            <tr><td width="100%">
                <input
                    type="submit"
                    value="Установить новый модуль"
                    class="installbutton">
            </td></tr>
        </table>
        </form>
        </td></tr>
        </table>
    ~;
    return $installModuleFormHtml;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getMMHomePage ( $session_id )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $crypted_login - зашифрованный логин пользователя.
# $SHOW_MESS - флаг показа сообщения (1 - показ, 0 - пусто).
# $message - сообщение
# Вывод содержимого стартовой страницы модуля "Конфигурация".
# Функция возвращает HTML код стартовой страницы модуля "Конфигурация" (заголовок,
# [диагностическое сообщение], таблицу с установленными модулями).
#
#----------------------------------------------------------------------------------------

sub getMMHomePage {
    my $CFGCNT_content;
    my ($sid, $cryptlgn, $printflag, $procmess) = @_;
    my $crypted_userid = Pheix::Session::getCryptLoginBySid($sid);
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file, "description" );
    my $check_passw_hash =
      Pheix::Session::isDefaultPasswd( $crypted_userid, $sid );
    if ( $check_passw_hash == 1 ) {
        my $_hint;
        if ( $printflag == 1 ) {
            my $rc;
            if ($procmess =~ /error/) {
                $rc = 0;
            } else {
                $rc = 1;
            }
            $_hint = showUpdateHint($printflag, $rc, $procmess);
        }
        $CFGCNT_content =
          $_hint . Secure::UserAccess::getModfyPasswdForm($sid);
    } else {
        my @access_modes = ( 3, 3, 3 );
        @access_modes =
          Secure::UserAccess::getAccessMode($sid);
        if ( $access_modes[0] != 3 ) {
            $CFGCNT_content = qq~
                <div class=admin_header>$module_descr</div>
            ~. getInstalledModsTable(@_);
        } else {
            $CFGCNT_content = qq~
                <div class=admin_header>Добро пожаловать в Pheix!</div>
            ~;
        }
    }
    return $CFGCNT_content;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# checkIfSetupable ( $Modulename )
#----------------------------------------------------------------------------------------
#
# $Modulename - название модуля.
# Проверить настраиваемый ли модуль.
# Функция возвращает 1 если модуль настраиваемый, иначе 0.
#
#----------------------------------------------------------------------------------------

sub checkIfSetupable {
    my $modname = $_[0];
    my $rc = 0;
    my $modadmxml = &getModXml("$MODULES_PATH/$modname/");
    my $modusrxml = &getModXml("$MODULES_USRPATH/$modname/");
    if (($modadmxml ne '') && (-e "$MODULES_PATH/$modname/$modadmxml")) {
        my $dom = XML::LibXML->load_xml(location => "$MODULES_PATH/$modname/$modadmxml");
        foreach my $settings ($dom->findnodes('/module/configuration/settings')) {
            if ($settings->textContent() ne '' && $settings->textContent() !~ /^[\r\n\s]+$/) {
                $rc = 1;
            }
        }
    }
    if (($modusrxml ne '') && (-e "$MODULES_USRPATH/$modname/$modusrxml")) {
        my $dom = XML::LibXML->load_xml(location => "$MODULES_USRPATH/$modname/$modusrxml");
        foreach my $settings ($dom->findnodes('/module/configuration/settings/*')) {
            if ($settings->textContent() ne '' && $settings->textContent() !~ /^[\r\n\s]+$/) {
                #print "<h1>literal: \"".$settings->nodeName().": ".$settings->textContent()."\"</h1>";
                $rc = 1;
            }
        }
    }
    return $rc;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getPageSetupCntnt ( $sesion_id, $Modulename )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $Modulename - название модуля.
# Получить содержимое страницы настроек модуля.
# Функция возвращает содержимое страницы настроек модуля в формате HTML.
#
#----------------------------------------------------------------------------------------

sub getPageSetupCntnt {
    my $xml_description_file = Config::ModuleMngr::getModXml($MODULE_FOLDER);
    my $module_descr = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xml_description_file, "description" );
    my $sesion_id = $_[0];
    my $modname = $_[2];
    my $modadmxml = &getModXml("$MODULES_PATH/$modname/");
    my $modusrxml = &getModXml("$MODULES_USRPATH/$modname/");
    my $CFGCNT_content =
      qq~<div class=admin_header>$module_descr</div>~.&showUpdateHint($_[3],$_[4],$_[5]);
    if ($modname =~ /^[a-z0-9]+$/i && (&isModInstalled($modname, 0) != 1 || &isModInstalled($modname, 1) != 1)) {
        $CFGCNT_content .= &showSetupTab($sesion_id, "$MODULES_PATH/$modname/", $modadmxml);
        $CFGCNT_content .= &showSetupTab($sesion_id, "$MODULES_USRPATH/$modname/", $modusrxml);
    } else {
        $CFGCNT_content .= qq~<p class=error>Модуль с наименованием «$modname» не найден!</p>~;
    }
    return $CFGCNT_content;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# getSetupField ( $name, $value, $toggle, $tlabls, $readonly )
#----------------------------------------------------------------------------------------
#
# $name - имя поля ввода.
# $value - значение поля ввода.
# $toggle - поле ввода типа toggle (1 - да, иначе - нет).
# $tlabls - надписи для radio при toggle="multi"
# $readonly - поле только для чтения
# Получить HTML код поля ввода настроек.
#
#----------------------------------------------------------------------------------------

sub getSetupField {
    my ($name, $value, $toggle, $tlabls, $readonly) = @_;

    my $fldcntnt;
    my $ro_attr = $readonly ? 'readonly' : q{};

    if ($toggle > 0) {
        my $_radios;
        my $htmlid = $name;
        $htmlid    =~ s/\//\_/gi;
        my @_lbls  = ('Нет', 'Да');
        if ($tlabls ne '') {
            @_lbls = split /\;/, $tlabls;
        }

        for my $i (0..$#_lbls) {
            my $checked;
            if ($i == $value) { $checked = 'checked'; }
            $_radios .= qq~
                <span class="sysgal-radio-block radio-block-mr"><input id="$htmlid\_$i"
                       name="$name"
                       $checked
                       value="$i"
                       hidden="" type="radio">
                <label for="$htmlid\_$i" class="hoverable">$_lbls[$i]</label></span>
            ~;
        }
        if ($_radios) {
            $fldcntnt = qq~<div class="setup-radio-block">$_radios</div>~;
        }
    } else {
        $fldcntnt = qq~
            <input type="text"
                   required $ro_attr name="$name"
                   value="$value"
                   class="setupinput">~;
    }
    return $fldcntnt;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# showSetupTab ( $sesion_id, $filepath, $xmlfile )
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $filepath - путь до каталога с файлами модуля.
# $xmlfile - имя файла xml-описателя модуля.
# Получить таблицу настроек модуля.
# Функция возвращает таблицу настроек модуля в формате HTML.
#
#----------------------------------------------------------------------------------------

sub showSetupTab {
    my $sesion_id  = $_[0];
    my $modulename = basename($_[1]);
    my $filepath   = $_[1].$_[2];
    my $xmlfile    = $_[2];
    my $CFGCNT_tab = '';
    if (($xmlfile ne '') && (-e $filepath)) {
        my $dom = XML::LibXML->load_xml(location => $filepath);
        my $ulsetup = '';
        foreach my $settings ($dom->findnodes('/module/configuration/settings/*')) {
            if ($settings->textContent() !~ /^[\r\n\s]+$/) {
                my $_group = $settings->getAttribute('group') || 'false';
                if ( $_group eq 'false' ) {
                    my $label    = Encode::encode("utf8", $settings->getAttribute('label'));
                    my $readonly = ($settings->getAttribute('readonly') // q{} eq 'true') ? 1 : 0;
                    my $tgl      = $settings->getAttribute('toggle');
                    my $toggle   = $tgl eq 'true' ? 1 : $tgl eq 'multi' ? 2 : 0;
                    my $tlabls   = Encode::encode("utf8", $settings->getAttribute('togglelabels'));
                    my $value    = Encode::encode("utf8", $settings->textContent());
                    my $nodepath = $settings->nodePath();
                    my $field = getSetupField($nodepath, $value, $toggle, $tlabls, $readonly);
                    $value =~ s/[\r\n\t]+//gi;
                    $ulsetup .= qq~
                        <div class="setuplabeldiv">$label:</div>
                        <div class="setupinputdiv">$field</div>
                    ~;
                } else {
                    my $group_cntnt = '';
                    my $grouplabel = Encode::encode("utf8", $settings->getAttribute('label'));
                    my $groupaction = Encode::encode("utf8", $settings->getAttribute('action'));
                    my $groupnode = $settings->nodeName();
                    foreach my $group_settings ($settings->nonBlankChildNodes()) {
                            my $label = Encode::encode("utf8", $group_settings->getAttribute('label'));
                            $label =~ s/</&lt;/i;
                            $label =~ s/>/&gt;/i;
                            $label =~ s/\"/&quot;/i;
                            my $value = Encode::encode("utf8", $group_settings->textContent());
                            $value =~ s/[\r\n]+//gi;
                            $value =~ s/[\s\t]+/ /g;
                            my $nodepath = $group_settings->nodePath();
                            my $readonly = ($settings->getAttribute('readonly') // q{} eq 'true') ? 1 : 0;
                            my $tgl      = $settings->getAttribute('toggle');
                            my $toggle   = $tgl eq 'true' ? 1 : $tgl eq 'multi' ? 2 : 0;
                            my $tlabls   = Encode::encode("utf8", $settings->getAttribute('togglelabels'));
                            my $field    = getSetupField($nodepath, $value, $toggle, $tlabls, $readonly);
                            $group_cntnt .= qq~
                                <div class="setuplabeldiv">$label:</div>
                                <div class="setupinputdiv">$field</div>
                            ~;
                        }
                    $ulsetup .= qq~
                        <div class="setupgroup">
                        <p><b>$grouplabel ($groupaction)</b></p>
                        $group_cntnt
                        </div>
                    ~;
                }
            }
        }
        if ($ulsetup) {
            my $area = 1;
            if ($filepath =~ /admin/) {
                $area = 0;
            }
            $CFGCNT_tab .= qq~
            <table cellspacing="0" cellpadding="10" width="100%" class="tabselect">
            <tr>
                <td width="100%" valign="top" align="left" class="s_t">
                    <p class=cmstext><b>Настройки из хранилища $filepath:</b></p>
                    <div class="setupdiv">
                        <form method="POST" action="$ENV{SCRIPT_NAME}?id=$sesion_id&amp;action=admsetupsave&amp;name=$modulename&amp;area=$area">$ulsetup
                            <input type="submit" class="setupbutton" value="Сохранить">
                        </form>
                    </div>
                </td>
            </tr></table>
            ~;
        }
    } else {
        my $xmlfile = getModXml("$MODULES_PATH/$modulename/");
        my $sytemtype = Config::ModuleMngr::getConfigValue( $MODULE_FOLDER, $xmlfile, "systemtype" );
        if (!$sytemtype) {
            $CFGCNT_tab .= "<p class=error>Описатель модуля не найден по заданному пути: $filepath</p>";
        }
    }
    return $CFGCNT_tab;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# saveModSettings ( $sesion_id, $modulename, $area, $co)
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# $co - описатель CGI.
# Сохранить настройки модуля.
# Функция сохраняет настройки модуля в хранилище.
#
#----------------------------------------------------------------------------------------

sub saveModSettings {
    my $retcode        = 0;
    my $CFGCNT_content = '';
    my $session_id     = $_[0];
    my $modulename     = $_[1];
    my $cgi_descr      = $_[3];
    my $area = $_[2] == 1 ? 'user' : 'admin';
    if ($area =~ /user/ && &isModInstalled($modulename, 1) != 1) {
        $retcode = 1;
        my $modpath = $MODULES_USRPATH;
        my $xmlfile = getModXml("$modpath/$modulename/");
        $CFGCNT_content = qq~<p class=hintcont>Настройки сохранены в $area-хранилище модуля «$modulename»!</p>~;
        $retcode = saveXMLData($session_id, "$modpath/$modulename/", $xmlfile, $cgi_descr);
        if ($retcode == 1) {
            $CFGCNT_content = qq~<p class=hintcont>Настройки сохранены в $area-хранилище модуля «$modulename»!</p>~;
        } else {
            $CFGCNT_content = qq~<p class=hintcont>Ошибка при сохранении настроек в $area-хранилище модуля «$modulename»!</p>~;
        }
    } elsif ($area =~ /admin/ && isModInstalled($modulename, 0) != 1) {
        my $modpath = $MODULES_PATH;
        my $xmlfile = getModXml("$modpath/$modulename/");
        $retcode = saveXMLData($session_id, "$modpath/$modulename/", $xmlfile, $cgi_descr);
        if ($retcode == 1) {
            $CFGCNT_content = qq~<p class=hintcont>Настройки сохранены в $area-хранилище модуля «$modulename»!</p>~;
        } else {
            $CFGCNT_content = qq~<p class=hintcont>Ошибка при сохранении настроек в $area-хранилище модуля «$modulename»!</p>~;
        }
    } else {
        $retcode = 0;
        $CFGCNT_content = qq~<p class=hintcont>Модуль «$modulename» не установлен</p>~.isModInstalled($modulename, 1);
    }
    return ($retcode, $CFGCNT_content);
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# saveXMLData ( $sesion_id, $modulepath, $xmlfile, $cgi_descr)
#----------------------------------------------------------------------------------------
#
# $sesion_id - идентификатор сессии доступа к административной части.
# $modulepath - путь до каталога с файлами модуля.
# $xmlfile - имя файла xml-описателя модуля.
# $cgi_descr - описатель CGI.
# Сохранить настройки в XML файл.
# Функция сохраняет настройки в XML файл.
#
#----------------------------------------------------------------------------------------

sub saveXMLData {
    my $rc          = 0;
    my $sesion_id  = $_[0];
    my $modulename = basename($_[1]);
    my $filepath   = $_[1].$_[2];
    my $xmlfile    = $_[2];
    my $cgi_descr  = $_[3];
    if (($xmlfile ne '') && (-e $filepath)) {
        my $doc = XML::LibXML->load_xml(location => $filepath);
        my @all_par_names = $cgi_descr->param;
        my @results = ();
        for(my $i=0; $i<=$#all_par_names; $i++) {
            my @nodes = $doc->findnodes($all_par_names[$i]);
            if ($nodes[0]) {
                $nodes[0]->removeChildNodes();
                my $setupvalue = $cgi_descr->param($all_par_names[$i]);
                $setupvalue =~ s/</&lt;/i;
                $setupvalue =~ s/>/&gt;/i;
                $setupvalue =~ s/\"/&quot;/i;
                $setupvalue =~ s/[\r\n]+//gi;
                $setupvalue =~ s/[\s\t]+/ /g;
                $nodes[0]->appendText($setupvalue);
                push(@results, 1)
            }
        }
        if ($#results == $#all_par_names) {
            open(my $outputFH, ">", $filepath);
            print $outputFH $doc->toString;
            close($outputFH);
            $rc = 1;
        }
    } else {
        $rc = 0;
    }
    return $rc;
}

#----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------
# saveXMLDataSetting ( $sid, $modulepath, $xmlfile, $settinghash)
#----------------------------------------------------------------------------------------
#
# $sid - идентификатор сессии доступа к административной части.
# $modulepath - путь до каталога с файлами модуля.
# $xmlfile - имя файла xml-описателя модуля.
# $settinghash - хеш с настройкой.
# Сохранить заданную настройку в XML файл.
#
#----------------------------------------------------------------------------------------

sub saveXMLDataSetting {
    my ($sid, $modulepath, $xmlfile, $settinghash)  = @_;
    my $modulename = basename($modulepath);
    my $filepath   = $modulepath.$xmlfile;
    my $rc;
    if (($xmlfile ne '') && (-e $filepath)) {
        my $doc = XML::LibXML->load_xml(location => $filepath);
        my @all_par_names = ($settinghash->{setting});
        my @results;
        for(my $i=0; $i<=$#all_par_names; $i++) {
            my @nodes = $doc->findnodes($all_par_names[$i]);
            if ($nodes[0]) {
                $nodes[0]->removeChildNodes();
                my $setupvalue = $settinghash->{value};
                $setupvalue =~ s/</&lt;/i;
                $setupvalue =~ s/>/&gt;/i;
                $setupvalue =~ s/\"/&quot;/i;
                $setupvalue =~ s/[\r\n]+//gi;
                $setupvalue =~ s/[\s\t]+/ /g;
                $nodes[0]->appendText($setupvalue);
                push(@results, 1)
            }
        }
        if ($#results == $#all_par_names) {
            open(my $outputFH, ">", $filepath);
            print $outputFH $doc->toString;
            close($outputFH);
            $rc = 1;
        } else {
            $rc = 0;
        }
    } else {
        $rc = -1;
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSingleSetting ( $modulename, $area, $setting_name )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# Получить настройку из хранилища (одиночная настройка, не входящая в группу).
# Функция возвращает настройку из хранилища.
#
#------------------------------------------------------------------------------

sub getSingleSetting {
    my ( $modulename, $area, $setting_name ) = @_;
    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    my $setting;
    my $modulepath;
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }
    my $xmlfile = getModXml("$modulepath/$modulename/");
    if ( defined $xmlfile ) {
        my $xmlpath = "$modulepath/$modulename/$xmlfile";
        if ( -e $xmlpath ) {
            my $doc = XML::LibXML->load_xml( location => $xmlpath );
            my @nodes =
                $doc->findnodes("/module/configuration/settings/$setting_name");
            if ( $nodes[0] ) {
                if ( defined $nodes[0]->textContent() && $nodes[0]->textContent() !~ /^[\r\n\s]+$/sx ) {
                    $setting = $nodes[0]->textContent();
                }
            }
        }
    }
    return $setting;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSettingFromGroupByAttr ( $modulename, $area, $setting_group_name,
#                            $setting_group_attr_n, $setting_group_attr_v,
#                            $setting_name )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# $setting_group_name - название группы настроек.
# $setting_group_attr_n - название ключевого атрибута группы.
# $setting_group_attr_v - значение атрибута $setting_group_attr_n.
# $setting_name - название настройки.
# Получить настройку из хранилища (настройка из заданной группы).
# Функция возвращает настройку из хранилища.
#
#------------------------------------------------------------------------------

sub getSettingFromGroupByAttr {
    my ( $modulename, $area, $setting_group_name, $setting_group_attr_n,
        $setting_group_attr_v, $setting_name )
      = @_;
    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    my $setting;
    my $modulepath;
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }
    my $xmlfile = getModXml("$modulepath/$modulename/");
    if ( defined $xmlfile ) {
        my $xmlpath = "$modulepath/$modulename/$xmlfile";
        if ( -e $xmlpath ) {
            my $doc = XML::LibXML->load_xml( location => $xmlpath );
            foreach my $settings (
                $doc->findnodes('/module/configuration/settings/*') )
            {
                if ( $settings->textContent() !~ /^[\r\n\s]+$/sx ) {
                    my $_group = $settings->getAttribute('group') || 'false';
                    if ( $_group eq 'true'
                        && $settings->nodeName() eq $setting_group_name )
                    {
                        my $grouplabl = Encode::encode( "utf8",
                            $settings->getAttribute('label') );
                        my $groupattr = Encode::encode( "utf8",
                            $settings->getAttribute($setting_group_attr_n) );
                        if ( $groupattr eq $setting_group_attr_v ) {
                            foreach my $group_settings (
                                $settings->nonBlankChildNodes() )
                            {
                                if (
                                    $group_settings->nodeName() eq $setting_name
                                    && defined $group_settings->textContent()
                                    && $group_settings->textContent() !~
                                    /^[\r\n\s]+$/sx )
                                {
                                    $setting = $group_settings->textContent();
                                    $setting =~ s/[\r\n]+//sxgi;
                                    $setting =~ s/[\s\t]+/ /sxg;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $setting;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSettingChildNum ( $modulename, $area, $setting_name )
#------------------------------------------------------------------------------
#
# $modulename - название модуля.
# $area - тип области CMS (0 - user, 1 - admin).
# $setting_name - название настройки.
# Подсчитать количество поднастроек (дочерних элементов)  для настройки.
#
#------------------------------------------------------------------------------

sub getSettingChildNum {
    my ( $modulename, $area, $setting_name ) = @_;
    if   ( $area == 1 ) { $area = 'user'; }
    else                { $area = 'admin'; }
    my $setting;
    my $modulepath;
    my $rc = -1;
    if ( $area =~ /user/sx ) {
        $modulepath = $MODULES_USRPATH;
    } else {
        $modulepath = $MODULES_PATH;
    }
    my $xmlfile = getModXml("$modulepath/$modulename/");
    if (defined $xmlfile) {
        my $xmlpath = "$modulepath/$modulename/$xmlfile";
        if ( -e $xmlpath ) {
            my $doc = XML::LibXML->load_xml( location => $xmlpath );
            my @nodes =
                $doc->findnodes("/module/configuration/settings/$setting_name/*");
            if ( @nodes ) {
                $rc = $#nodes;
            }
        }
    }
    return $rc;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# fetchApopheozNews ( $sid )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии.
# Показать свежую новость с apopheoz.ru в SlideTabOut блоке.
#
#------------------------------------------------------------------------------

sub fetchApopheozNews {
    my ($sid) = @_;
    my $newsblock;
    my $shownewsblock = getSingleSetting($MODULE_NAME, 0, 'apopheoznews');
    my $url = 'http://apopheoz.ru/getlatestnews_json.html';
    if (defined $shownewsblock && $shownewsblock == 1) {
        my $link_title = decode("utf8", "Перейти к новости");
        my $arch_title = decode("utf8", "Все новости");
        $newsblock = qq~
        <div class="apopheoz-news-slide-out-div">
            <a class="handle" href="#">Content</a>
            <div class="apopheoz-news-date">%date%</div>
            <div class="apopheoz-news-title">
                <a href="http://apopheoz.ru/newsdetails_%id%.html" target=_blank class="apopheoz-news-link-title">%title%</a></div>
            <p>%text%</p>
            <p class="apopheoz-news-bottom-navig">  <a href="http://apopheoz.ru/newsdetails_%id%.html" target=_blank>$link_title &rarr;</a> |
                 <a href="http://apopheoz.ru/newsarchive.html" target=_blank>$arch_title</a></p>
            <!--<p>%status%</p>-->
        </div>

        <script type="text/javascript">
        jQuery(function(){
            jQuery('.apopheoz-news-slide-out-div').tabSlideOut({
                tabHandle: '.handle',
                pathToTabImage: 'images/news-panel-button.png',
                imageHeight: '129px',
                imageWidth: '38px',
                tabLocation: 'left',
                speed: 300,
                action: 'click',
                topPos: '63px',
                leftPos: '20px',
                fixedPosition: false
            });
        });
        </script>~;
        if ($url) {
            my $procstat;
            my $procmess;
            my $ua = LWP::UserAgent->new;
            $ua->agent( "PheixNewsCrawler/" . Pheix::Tools::getPheixVersion() );
            my $req = HTTP::Request->new( GET => $url );
            my $res = $ua->request($req);
            my $json_response = $res->content;
            $json_response =~ s/\s{2,}//gi;
            $json_response =~ s/\t//gi;
            $json_response =~ s/[\r\n]+//gi;

            if ( $res->is_success ) {
                my $obj = JSON::XS::decode_json($json_response);
                if ($obj) {
                    if ($obj->{status}) {
                        $newsblock =~ s/%date%/$obj->{newsdetails}->{date}/gi;
                        $newsblock =~ s/%title%/$obj->{newsdetails}->{title}/gi;
                        $newsblock =~ s/%id%/$obj->{newsdetails}->{id}/gi;
                        $newsblock =~ s/%text%/$obj->{newsdetails}->{text}/gi;
                    }
                    $procmess .= 'JSON decoded, status='.$obj->{status}.'. ';
                }
                $procstat = 1;
                $procmess .= 'Request done - downloaded '
                    . length( $res->content ). ' bytes';
            } else {
                $procstat = 0;
                $procmess = 'Error while requesting data:' . $res->status_line;
            }
            $newsblock =~ s/%status%/$procmess/gi;
        } else {
            $newsblock = '';
        }
    }
    return $newsblock;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getProtoSrvName ( )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Получить протокол + имя сервера из настроек.
#
#------------------------------------------------------------------------------

sub getProtoSrvName {
    my $_prtcl;
    my $_proto = getSingleSetting('Config', 0, 'workviaproto');
    my $_sname = getSingleSetting('Config', 0, 'servername');
    if (!$_sname || $_sname eq '' || !defined $_sname) {
        $_sname = $ENV{SERVER_NAME};
    }
    if ($_proto == 1) {
        $_prtcl = 'https://';
    } else {
        $_prtcl = 'http://';
    }
    $_sname =~ s/^https?\:\/\///gi;
    $_sname =~ s/\/{1,}$//gi;
    return $_prtcl.$_sname;
}


#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# getSitemapDetails ( )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии.
# Детализация по файлу карты сайта.
#
#------------------------------------------------------------------------------

sub getSitemapDetails {
    my ($sid) = @_;
    my @_smstat;
    my $_cntnt;
    if (-e $STATIC_SM_PATH) {
        push @_smstat, ( -s $STATIC_SM_PATH );
        push @_smstat, ( strftime '%Y-%m-%d, %T', localtime((stat($STATIC_SM_PATH))[9]) );
    }
    if (@_smstat) {
        my $_url = getProtoSrvName();
        $_cntnt = qq~
            <div id="sitemap-details-contaiter" class="pheix-margins-25px">
            <div class="statslogrec statslogrec-fullbrdr ">
              <div class="statslogrec-cont">
                <p><span>Файл: </span><a href="$_url/$STATIC_SM_PATH" target="_blank" class="statloglink">$STATIC_SM_PATH</a>
                <span class="pheix-del-sitemap-icon"><a href="javascript:doInlineDataLoad('sitemap-details-contaiter','$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsitemapdel')" class="fa-action-link">
                    <i class="fa fa-trash"
                       title="Удалить статическую карту сайта"></i></a></span>
                </p>
                <p><span>Размер: </span>$_smstat[0] B</p>
                <p><span>Модифицирован: </span>$_smstat[1]</p>
              </div>
            </div>
            </div>
        ~;
    } else {
        $_cntnt = qq~
            <div id="sitemap-details-contaiter" class="pheix-margins-25px">
            <div class="pheix-error-alert">
                Файл: $STATIC_SM_PATH не найден!
            </div>
            </div>
        ~;
    }
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# showSmSummary ( )
#------------------------------------------------------------------------------
#
# $sid - идентификатор сессии.
# Вывод информации по файлу карты сайта.
#
#------------------------------------------------------------------------------

sub showSmSummary {
    my ($sid) = @_;
    my $_cntnt = qq~
        <div class="popup_header">Генерация статической карты сайта</div>
        <div class="standart_text">
           <a href="$ENV{SCRIPT_NAME}?id=$sid&amp;action=admmain" class="simple_link">
           <b>Конфигурация</b></a>&nbsp;/&nbsp;
           Статическая карта сайта
        </div>
    ~;
    $_cntnt .= getSitemapDetails($sid);
    $_cntnt .= qq~
        <div class="frm-div-marg modal-tmargin">
        <input onclick="javascript:doInlineDataLoad('sitemap-details-contaiter','$ENV{SCRIPT_NAME}?id=$sid&amp;action=admsmdetails')"
               value="Сгенерировать карту" class="button modal-butt" type="button">
        </div>
    ~;
    return $_cntnt;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# saveStaticSitemap ( $smfname )
#------------------------------------------------------------------------------
#
# $smfname - имя файла карты сайта.
# Функция генерирует файл sitemap.xml.
#
#------------------------------------------------------------------------------

sub saveStaticSitemap {
    my ($smfname) = @_;
    if (!$smfname) {
        $smfname = $STATIC_SM_PATH;
    }
    my $_urltmpl = "\t<url><loc>%loc%</loc><lastmod>%lastmod%</lastmod></url>\n";
    my @_smap = (
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">',
        $_urltmpl,'</urlset>'
    );
    my $_root = getProtoSrvName();
    my $_rtup = strftime '%Y-%m-%d', localtime(time);
    $_smap[2] =~ s/%loc%/$_root/gi;
    $_smap[2] =~ s/%lastmod%/$_rtup/gi;
    my @_imods = getInstAppMods();
    if (@_imods) {
        foreach my $mod (@_imods) {
            my $_exec_expr = getConfigValue(
                dirname($mod),
                basename($mod),
                'use_identifier'
            );
            my $_smfunc = getConfigValue(
                dirname($mod),
                basename($mod),
                'sitemapfunction'
            );
            if ($_exec_expr && $_smfunc) {
                my @_sm;
                eval { @_sm = $_exec_expr->$_smfunc(); };
                if (@_sm) {
                    my $_smdata;
                    foreach my $ref (@_sm) {
                        my %_sm = %{$ref};
                        my $_urltmplcp = $_urltmpl;
                        $_urltmplcp =~ s/%loc%/$_sm{loc}/gi;
                        $_urltmplcp =~ s/%lastmod%/$_sm{lastmod}/gi;
                        $_smdata .= $_urltmplcp;
                    }
                    if ($_smdata) {
                        $_smap[2] .= $_smdata;
                    }
                }
            }
        }
    }
    chomp($_smap[2]);
    open my $fh, '>', $smfname;
    print $fh join "\n", @_smap;
    close $fh;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# delStaticSitemap ( $smfname )
#------------------------------------------------------------------------------
#
# У функции нет входных аргументов.
# Удалить файл статической карты сайта.
#
#------------------------------------------------------------------------------

sub delStaticSitemap {
    if (-e $STATIC_SM_PATH) {
        unlink $STATIC_SM_PATH;
    }
}

#------------------------------------------------------------------------------

END {}

1;
