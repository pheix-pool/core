<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version = "1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
    <body>
      <h2>Идентификатор клиента: <xsl:value-of select="//clientid"/></h2>
      <ul>
      		<xsl:apply-templates select="//rekvizit"/>
      </ul>
    </body>
</html>
</xsl:template>
<xsl:template match="//rekvizit"> 
     <li><xsl:value-of select="name"/>: <b><xsl:value-of select="value"/></b></li>
</xsl:template>
</xsl:stylesheet>
